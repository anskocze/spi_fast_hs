# Clocks 
create_clock -name {CLK} -period 25 -waveform {0 12.5 } [ get_ports { clk } ]
create_clock -name {USBCLK} -period 16.6667 -waveform {0 8.33333 } -add  [ get_ports { USBCLK } ]
create_clock -name {SPI_CLOCK} -period 50 -add  [ get_ports { SPI_CLOCK } ]
set_clock_groups -asynchronous -group CLK -group USBCLK -group SPI_CLOCK
# Fast Outputs
#set_clock_to_output 16.6 -clock {FTDI_A_CNT[5]} -max [ get_ports FTDI_A_D ]
#set_clock_to_output 3 -clock {FTDI_A_CNT[5]} -min [ get_ports FTDI_A_D ]

#set_output_delay 3.5 -clock {USBCLK} [ get_ports USBnWR ]
#set_output_delay 3.5 -clock {USBCLK} [ get_ports USBDataBus ]
#set_output_delay -rise -max 16.6 -clock {FTDI_A_CNT[5]} [ get_ports FTDI_A_D ]
#set_output_delay 5 -fall -max -clock {FTDI_A_CNT[5]} [ get_ports { FTDI_A_D } ]
#set_output_delay 5 -fall -min -clock {FTDI_A_CNT[5]} [ get_ports { FTDI_A_D } ]
#set_output_delay 5 -rise -min -1.0 -clock {FTDI_A_CNT[5]} [ get_ports FTDI_A_D ]
