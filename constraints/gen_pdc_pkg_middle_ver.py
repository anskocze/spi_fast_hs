# -*- coding: utf-8 -*-
"""
Created on ?????????
Description: Package to generate PDC file from Excell sheet 
    
@author: Jelena Spasic

Modified: function with two arguments; A.Skoczen (AGH-UST); 23.08.2019
Modified: geneartion of NDC file for I/O Register Combining in ECF mode; A.Skoczen (AGH-UST); 23.08.2019
Modified: second sheet for banks definition; A.Skoczen (AGH-UST); 10.03.2020
Modified: migration to openpyxl; A.Skoczen (AGH-UST); 22.07.2021

Arguments:
    libero_ver: 11 for CCF
                12 or 2021 for ECF (NDC)
    in_file: full path to Execel file
    
"""

#import xlrd
import os
from openpyxl import load_workbook

def check_xlsx(in_file):
    if not os.path.exists(in_file):
        print('Excel file', in_file, 'does not exist')
        exit()
    else:
        workbook = load_workbook(filename = in_file)
        #xlrd.open_workbook(in_file)
        sheets_name = workbook.sheetnames
        s1 = False
        s2 = False
        for names in sheets_name:
            if names == 'pinout':
                s1 = True
            if names == 'banks':
                s2 = True
        return s1 and s2

def gen_pdc(libero_ver, in_file, top_name, tmr):
    debug = False
    if libero_ver == 11:
        print('Conservative Constraint Flow')
    elif libero_ver == 12 or libero_ver == 2021:
        print('Enhanced Constraint Flow')
    else:
        print('Libero version not known')
        return False
    if not check_xlsx(in_file):
        print('Excel file', in_file, 'exists but is incorrect')
        print('It should consists of two sheet named: \"pinout\", \"banks\"')
        exit(1)
    else:
        print('Excel file', in_file, 'OK OK OK OK')
        #exit(1)
        
    generate = True
    if os.path.exists(in_file):
        pdc_input = load_workbook(filename = in_file)
        #xlrd.open_workbook(in_file)
        pdc_path = os.path.split(in_file)[0]
        pdc_file = os.path.join(pdc_path, top_name + '_io.pdc')
        ndc_file = os.path.join(pdc_path, top_name + '_io.ndc')
        if os.path.exists(pdc_file):
            if os.path.getmtime(in_file) < os.path.getmtime(pdc_file):
                generate = False
                print('PDC correct')
            else:
                print('PDC upgrade necessary')
            if libero_ver == 12 or libero_ver == 2021:
                if (not os.path.exists(ndc_file)) and (not tmr):
                    print('NDC file is missing -> regeneration is necessary')
                    generate = True
    else:
        print('Input file: ', in_file, ' does not exist')
        return False
                
    if generate :
        pdc_output = open(pdc_file, 'w')
        print('Generation:\t', in_file, '=>', pdc_file)
        if (libero_ver == 12 or libero_ver == 2021) and not tmr:
            #ndc_file = os.path.join(pdc_path, top_name + '_io.ndc')
            ndc_output = open(ndc_file, 'w')
            print('\t\tand', in_file, '=>', ndc_file)
        
        cnt_io_rc = 0
        cnt_io = 0
        cnt_b = 0
        
        bank_data = pdc_input['banks']
        #.sheet_by_name('banks')
        
        # Extract all the enabled banks
        if bank_data.cell(row=1, column=1).value == 'Bank number':
            bank_nr = [r for r in bank_data.iter_rows(min_row=2, min_col=1, max_col=1, values_only=True)]
            #print('Bank nr', bank_nr)
            #[bank_data.cell_value(r, 0) for r in range(1, bank_data.nrows)]
        else:
            print('Wrong header in column A')
            exit(1)
        if bank_data.cell(row=1, column=2).value == 'Enable/disable':
            bank_used = [r for r in bank_data.iter_rows(min_row=2, min_col=2, max_col=2, values_only=True)]
            #print('Bank enable', bank_used)
            #[bank_data.cell_value(r, 1) for r in range(1, bank_data.nrows)]
        else:
            print('Wrong header in column B')
            exit(1)
        if bank_data.cell(row=1, column=3).value == 'Suplly voltage':
            sup_vol = [r for r in bank_data.iter_rows(min_row=2, min_col=3, max_col=3, values_only=True)]
            #[bank_data.cell_value(r, 2) for r in range(1, bank_data.nrows)]
        else:
            print('Wrong header in column C')
            exit(1)
        if bank_data.cell(row=1, column=4).value == 'Fixed':
            fixed = [r for r in bank_data.iter_rows(min_row=2, min_col=4, max_col=4, values_only=True)]
            #[bank_data.cell_value(r, 3) for r in range(1, bank_data.nrows)]
        else:
            print('Wrong header in column D')
            exit(1)
        #print(len(bank_used))
        for i in range(len(bank_used)):
            #print(type(bank_used[i]))
            if bank_used[i][0] == 'enabled':
                cnt_b = cnt_b + 1
                line = 'set_iobank\t' + 'Bank' + str(int(bank_nr[i][0])) + '\t-vcci ' + str(sup_vol[i][0]) + '\t-fixed ' + fixed[i][0] + '\n'
                if debug == True: print(line)
                pdc_output.write(line)
        
        pdc_output.write('\n')
        
        pin_data = pdc_input['pinout']
        #.sheet_by_name('pinout')
        
        # Extract all the enabled pins
        signal_name = [r for r in pin_data.iter_rows(min_row=2, min_col=1, max_col=1, values_only=True)]
        #[pin_data.cell_value(r, 0) for r in range(1, pin_data.nrows)]
        pin_name = [r for r in pin_data.iter_rows(min_row=2, min_col=2, max_col=2, values_only=True)]
        #[pin_data.cell_value(r, 1) for r in range(1, pin_data.nrows)]
        pin_used = [r for r in pin_data.iter_rows(min_row=2, min_col=3, max_col=3, values_only=True)]
        #[pin_data.cell_value(r, 2) for r in range(1, pin_data.nrows)]
        IO_standard = [r for r in pin_data.iter_rows(min_row=2, min_col=4, max_col=4, values_only=True)]
        #[pin_data.cell_value(r, 3) for r in range(1, pin_data.nrows)]
        pin_direction = [r for r in pin_data.iter_rows(min_row=2, min_col=5, max_col=5, values_only=True)]
        #[pin_data.cell_value(r, 4) for r in range(1, pin_data.nrows)]
        schmitt_trigger = [r for r in pin_data.iter_rows(min_row=2, min_col=6, max_col=6, values_only=True)]
        #[pin_data.cell_value(r, 5) for r in range(1, pin_data.nrows)]
        IO_register = [r for r in pin_data.iter_rows(min_row=2, min_col=8, max_col=8, values_only=True)]
        #[pin_data.cell_value(r, 7) for r in range(1, pin_data.nrows)]
        if debug == True: print(IO_register)
        pull_up_down = [r for r in pin_data.iter_rows(min_row=2, min_col=9, max_col=9, values_only=True)]
        #[pin_data.cell_value(r, 8) for r in range(1, pin_data.nrows)]
        print(len(pin_used))
        for i in range(len(pin_used)):
            #print(pin_used[i])
            if pin_used[i][0] == 'enabled':
                cnt_io = cnt_io + 1
                sig = signal_name[i][0]
                if sig.find('[') != -1:
                    name = sig.partition('[')[0] + '\\' + sig.partition('[')[1] + sig.partition('[')[2].partition(']')[0] + '\\' + ']'
                else:
                    name = sig
                line = 'set_io\t' + name + '\t-pinname ' + pin_name[i][0] + '\t-fixed yes'
                if debug == True: print(line)
                pdc_output.write(line)
                
                if IO_standard[i][0] != '':
                    pdc_output.write('\t-iostd ' + IO_standard[i][0])
                
                if pin_direction[i][0] != '':
                    direction = pin_direction[i][0]
                else:
                    direction = 'INOUT'
                line = '\t-direction ' + direction
                if debug == True: print(line)
                pdc_output.write(line)
                
                if schmitt_trigger[i][0] != None and schmitt_trigger[i][0].upper() == 'YES':
                    pdc_output.write('\t-SCHMITT_TRIGGER On')
                if libero_ver == 11:
                    if IO_register[i][0].upper() == 'YES':
                        pdc_output.write('\t-register yes')
                        if pin_direction[i].upper() == 'INPUT':
                            pdc_output.write('\t-in_reg yes\t-out_reg no\t-en_reg no')
                        if pin_direction[i].upper() == 'OUTPUT':
                            pdc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no')
                        if pin_direction[i].upper() == 'INOUT':
                            pdc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no')
                        #pdc_output.write('\t-en_reg yes')
                    elif IO_register[i][0].upper() == 'NO':
                        pdc_output.write('\t-register no')
                elif libero_ver == 12 or libero_ver == 2021: 
                    if not tmr:
                        #print(type(IO_register[i]))
                        #print(type(IO_register[i][0]))
                        #print(IO_register[i])
                        if debug == True: print('IO_registe', IO_register[i][0])
                        if IO_register[i][0] != None and IO_register[i][0].upper() == 'YES':
                            cnt_io_rc = cnt_io_rc + 1
                            print('NDC - set_ioff ' + name)
                            ndc_output.write('set_ioff ' + name)
                            if pin_direction[i][0].upper() == 'INPUT':
                                ndc_output.write('\t-in_reg yes\t-out_reg no\t-en_reg no\n')
                            if pin_direction[i][0].upper() == 'OUTPUT':
                                ndc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no\n')
                            if pin_direction[i][0].upper() == 'INOUT':
                                ndc_output.write('\t-in_reg no\t-out_reg yes\t-en_reg no\n')
                else:
                    print('Libero version', libero_ver, ' not known')
                    return False
        
                if pull_up_down[i][0] != None:
                    pdc_output.write('\t-res_pull ' + pull_up_down[i][0])
                    
                pdc_output.write('\n')
                
        pdc_output.close()
        if (libero_ver == 12 or libero_ver == 2021) and not tmr:
            ndc_output.close()
        print('Number of I/O constrained:', cnt_io)
        if tmr and os.path.exists(ndc_file):
            os.remove(ndc_file)
        else:
            print('Number of register for combining into I/O:', cnt_io_rc)
    else:
        print('PDC and NDC not generated')
    return True

    