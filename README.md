# spi_fast_hs

30 Sep 2021
Demonstrator of SPI 20MHz transmission clock and 40MHz system clock to work with uC AT SAM E54 P20A
Handshake is used for synchronising two clock domains
To transmit one byte 12 transmission clock pulses are necessary. 

10 Feb 2023

Migration to Questa 2022.1

Instructions to run simulation:
Open Powershell or Window Terminal window.
Go to directory spi_fast_hs and create path.cfg file according to template below:

Libero=C:\Microsemi\Libero_SoC_v2021.1\Designer\bin
Synplify=C:\Synopsys\fpga_P-2019.03\bin
ModelSim=C:\MentorGraphics\Questa2022_1\win64
Git=C:\Users\anskocze\AppData\Local\Atlassian\SourceTree\git_local\bin

Customise the paths for your computer.

Create buildstamp.vhd file with command:
 python.exe .\run_uqds.py rev
 
Change to .\sim and launch simulation with command:
 python.exe .\sim_vhdl.py ..\top

After compilation the windows of Questa will be opened. 
Description of the SPI project is available in the spi_handshake.pdf file 

