#DO file for tb_top
#A.Skoczen AGH-UST, 13.2.2020
vsim  work.tb_top -lib work -t 1ps -g/tb_top/rate=374e3
# ch = 24		374kHz
# ch = 18		480kHz
# ch = 15		565kHz	
# ch = 12		680kHz	
# ch = 9		860kHz
# ch = 7		1020kHz
# ch = 5		1300kHz

quietly set sim_time {800000 ns}
#11660 ns}
#500000 ns}
#29537 ns}
#100000 ns}
quietly set NumericStdNoWarnings 1
if { $NumericStdNoWarnings == 1 } {puts "Warnings from numeric_std are suppressed"}
quietly set StdArithNoWarnings 1
if { $StdArithNoWarnings == 1 } {puts "Warnings from std_arith are suppressed"}
quietly set vVHDL 2008
switch $vVHDL {
	1987 {puts "VHDL-$vVHDL"}
	1993 {puts "VHDL-$vVHDL"}
	2002 {puts "VHDL-$vVHDL"}
	2008 {puts "VHDL-$vVHDL"}
	ams99 {puts "VHDL-AMS-1999"}
	ams07 {puts "VHDL-AMS-2007"}
}
quietly set nr_channels [examine -radix decimal /config_block_lib/CHANNELS_OUT_FR]
puts "Number of channels: $nr_channels"
quietly set size_pckt [examine -radix decimal /tb_top/PACKET_SIZE]
puts "Size of packet: $size_pckt bytes"
quietly set sampling_freq [examine -radix decimal /tb_top/rate]
quietly set sampling_period_clk [examine -radix decimal /tb_top/sample_period_cyc]
puts "Sampling period ($sampling_freq Hz) as number of system cycles: $sampling_period_clk"

quietly set fifo_ram_wwidth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/WWIDTH]
puts "FIFO RAM Write Data Width: $fifo_ram_wwidth"
quietly set fifo_ram_rwidth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/RWIDTH]
puts "FIFO RAM Read Data Width: $fifo_ram_rwidth"
quietly set fifo_ram_wdepth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/WDEPTH]
puts "FIFO RAM Write Address Width: $fifo_ram_wdepth"
quietly set fifo_ram_rdepth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/RDEPTH]
puts "FIFO RAM Raed Address Width: $fifo_ram_rdepth"

#quietly set pck_size [expr [string range $nr_channels [string first "d" $nr_channels]+1 end]*4 +8]

add wave -noupdate /tb_top/*
add wave -noupdate /tb_top/model_ftdi/*
add wave -noupdate /tb_top/model_ftdi/time_stamp_calc/*
add wave -noupdate /tb_top/uut/FIFOData
add wave -noupdate -format Analog-Step -height 200 -max 737577077.00000024 -min -737867478.0 -radix decimal /tb_top/chsin
add wave -noupdate -format Analog-Step -height 260 -max 737577077.0 -min -737867478.0 -radix decimal /tb_top/uut/fast_data/sinsig
add wave -noupdate /tb_top/uut/fast_data/sinsig

add wave -noupdate /tb_top/uut/FIFOWR
add wave -noupdate /tb_top/uut/fast_transmitt_out/byte_buffer
add wave -noupdate /tb_top/uut/fast_transmitt_out/data_buffer([expr $size_pckt - 1])
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/RE
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/WE
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/EMPTY
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FULL
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBnWR 
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBnTXE 
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBnTXEPrev
add wave -noupdate /tb_top/uut/fast_transmitt_out/FIFO_empty
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/AEMPTY
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/AFULL
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/EMPTY2 
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/FWFT
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/PREFETCH
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/EMPTY_xhdl4
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/WD   
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/RD   
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/WADDR
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/RADDR
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/WEN  
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/REN  
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/WCLK 
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/RCLK 
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/mem(0)
add wave -noupdate /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/mem(1)
add wave -noupdate /tb_top/uut/fast_transmitt_out/current_state
add wave -noupdate /tb_top/uut/fast_transmitt_out/write_byte
#add wave -noupdate /tb_top/uut/USBnTXE_debug 
#add wave -noupdate /tb_top/uut/USBnWR_debug  
#add wave -noupdate /tb_top/uut/USBCLK_debug  
#add wave -noupdate /tb_top/uut/USBnOE_debug  

add mem /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/mem -a decimal -d hexadecimal

update
WaveRestoreZoom {0 ps} $sim_time

run $sim_time
