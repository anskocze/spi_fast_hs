#DO file for tb_USB20_245FIFO_Controller
#A.Skoczen AGH-UST, 2.9.2019
vsim  work.tb_USB20_245FIFO_Controller -lib work -t 1ps

quietly set sim_time {100000 ns}
quietly set NumericStdNoWarnings 1
if { $NumericStdNoWarnings == 1 } {puts "Warnings from numeric_std are suppressed"}
quietly set StdArithNoWarnings 1
if { $StdArithNoWarnings == 1 } {puts "Warnings from std_arith are suppressed"}
quietly set vVHDL 2008
switch $vVHDL {
	1987 {puts "VHDL-$vVHDL"}
	1993 {puts "VHDL-$vVHDL"}
	2002 {puts "VHDL-$vVHDL"}
	2008 {puts "VHDL-$vVHDL"}
	ams99 {puts "VHDL-AMS-1999"}
	ams07 {puts "VHDL-AMS-2007"}
}
quietly set nr_channels [examine -radix decimal /config_block_lib/CHANNELS_OUT_FR]
puts "Number of channels: $nr_channels"
quietly set size_pckt [examine -radix decimal /tb_USB20_245FIFO_Controller/PACKET_SIZE]
puts "Size of packet: $size_pckt bytes"
add wave -noupdate /tb_USB20_245FIFO_Controller/*
add wave -noupdate /tb_USB20_245FIFO_Controller/data_generator/line_cnt
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/data_buffer
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/current_state
add wave -noupdate -radix decimal /tb_USB20_245FIFO_Controller/uut/byte_cnt
add wave -noupdate -radix decimal /tb_USB20_245FIFO_Controller/uut/sample_cnt
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/data_buffer(43)
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/st_reg
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/nx_st_reg
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/header_found
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/tstamp_finished
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/tsbuf
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/cnt
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/count
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/time_stamp/k
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/RE
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBnWR
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBnTXE
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBnTXEPrev
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/FIFO_empty
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/DATA
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/RCLOCK
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/WE
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/Q
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/WCLOCK
add wave /tb_USB20_245FIFO_Controller/uut/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/FIFOUSB_FIFOUSB_0_LSRAM_top_R0C0/MEM_1024_18 
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/EMPTY
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/AEMPTY
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/FULL
add wave -noupdate /tb_USB20_245FIFO_Controller/uut/USBFIFO/AFULL
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/dcnt
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/dcount
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/compare
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/clean
add wave -noupdate /tb_USB20_245FIFO_Controller/model_ftdi/adbus
add wave -radix unsigned /tb_USB20_245FIFO_Controller/uut/USBFIFO/FIFOUSB_0/L31/U_corefifo_async/rptr
add wave -radix unsigned /tb_USB20_245FIFO_Controller/uut/USBFIFO/FIFOUSB_0/L31/U_corefifo_async/wptr
add wave -radix unsigned /tb_USB20_245FIFO_Controller/ptr_diff
#add wave /tb_USB20_245FIFO_Controller/uut/USBFIFO/FIFOUSB_0/L31/U_corefifo_async/rptr_fwft

update
WaveRestoreZoom {0 ps} $sim_time

run $sim_time
