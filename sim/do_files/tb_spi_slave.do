quietly set ACTELLIBNAME smartfusion2
quietly set PROJECT_DIR "./sim"
#quietly 

vsim  work.tb_spi_slave -lib work -L smartfusion2 -t 1ns 

quietly set NumericStdNoWarnings 1
if { $NumericStdNoWarnings == 1 } {puts "Warnings from numeric_std are suppressed"}
quietly set StdArithNoWarnings 1
if { $StdArithNoWarnings == 1 } {puts "Warnings from std_arith are suppressed"}
quietly set VHDL93 2008
switch $VHDL93 {
	1987 {puts "VHDL-$VHDL93"}
	1993 {puts "VHDL-$VHDL93"}
	2002 {puts "VHDL-$VHDL93"}
	2008 {puts "VHDL-$VHDL93"}
	ams99 {puts "VHDL-AMS-1999"}
	ams07 {puts "VHDL-AMS-2007"}
}

quietly WaveActivateNextPane {} 0

add wave -noupdate /tb_spi_slave/clk
add wave -noupdate /tb_spi_slave/rst
add wave -noupdate /tb_spi_slave/master/clk
#add wave -noupdate /tb_spi_slave/master/sample1/samples
add wave -noupdate /tb_spi_slave/master/data
add wave -noupdate /tb_spi_slave/master/sdata
add wave -noupdate /tb_spi_slave/cs
add wave -noupdate /tb_spi_slave/spiclk
add wave -noupdate /tb_spi_slave/mosi
add wave -noupdate /tb_spi_slave/miso
add wave -noupdate /tb_spi_slave/uut/fast/state
add wave -noupdate /tb_spi_slave/uut/fast/rshreg
add wave -noupdate /tb_spi_slave/uut/fast/bitcnt
add wave -noupdate /tb_spi_slave/uut/fast/rxreq
add wave -noupdate /tb_spi_slave/uut/fast/rxack
add wave -noupdate /tb_spi_slave/uut/fast/txreq
add wave -noupdate /tb_spi_slave/uut/fast/txack
add wave -noupdate /tb_spi_slave/uut/received_dat
add wave -noupdate /tb_spi_slave/uut/rdata
add wave -noupdate /tb_spi_slave/for_transmit
add wave -noupdate /tb_spi_slave/dataclk
add wave -noupdate /tb_spi_slave/uut/fast/tshreg
add wave -noupdate /tb_spi_slave/uut/fast/txreq
add wave -noupdate /tb_spi_slave/uut/fast/txack
add wave -noupdate /tb_spi_slave/uut/nedtx
add wave -noupdate /tb_spi_slave/uut/fast/synctxreq
add wave -noupdate /tb_spi_slave/master_received
add wave -noupdate /tb_spi_slave/st
add wave -noupdate /tb_spi_slave/uut/for_transmit
add wave -noupdate /tb_spi_slave/uut/tx_valid_pulse
add wave -noupdate /tb_spi_slave/uut/rx_valid_pulse

#add wave -noupdate /tb_spi_slave/stim_gen/*
#add wave -noupdate /tb_spi_slave/uut/*
#add wave -noupdate /tb_spi_slave/uut/fast/*
#add wave -noupdate /tb_spi_slave/data_for_slave/clk
#add wave -noupdate /tb_spi_slave/data_for_slave/l
#add wave -noupdate /tb_spi_slave/data_for_slave/rawdata

add memory -addressradix decimal -dataradix hex -wordsperline 16 /tb_spi_slave/data_for_slave/sample1/samples
add memory -addressradix decimal -dataradix hex -wordsperline 16 /tb_spi_slave/master/sample1/samples
#add memory -addressradix decimal -dataradix hex -wordsperline 16 /tb_spi_slave/test_mem
TreeUpdate [SetDefaultTree]

configure wave -namecolwidth 216
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns

update

run 10us
#12 us
view -undock wave
wave zoomfull
