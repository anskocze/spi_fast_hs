#DO file for tb_top
#A.Skoczen AGH-UST, 13.2.2020
vsim  work.tb_top -lib work -t 1ps -g/tb_top/rate=374e3
# ch = 24		374kHz
# ch = 18		480kHz
# ch = 15		565kHz	
# ch = 12		680kHz	
# ch = 9		860kHz
# ch = 7		1020kHz
# ch = 5		1300kHz

quietly set sim_time {20000 ns}
#11660 ns}
#500000 ns}
#29537 ns}
#100000 ns}
quietly set NumericStdNoWarnings 1
if { $NumericStdNoWarnings == 1 } {puts "Warnings from numeric_std are suppressed"}
quietly set StdArithNoWarnings 1
if { $StdArithNoWarnings == 1 } {puts "Warnings from std_arith are suppressed"}
quietly set vVHDL 2008
switch $vVHDL {
	1987 {puts "VHDL-$vVHDL"}
	1993 {puts "VHDL-$vVHDL"}
	2002 {puts "VHDL-$vVHDL"}
	2008 {puts "VHDL-$vVHDL"}
	ams99 {puts "VHDL-AMS-1999"}
	ams07 {puts "VHDL-AMS-2007"}
}
quietly set nr_channels [examine -radix decimal /config_block_lib/CHANNELS_OUT_FR]
puts "Number of channels: $nr_channels"
quietly set size_pckt [examine -radix decimal /tb_top/PACKET_SIZE]
puts "Size of packet: $size_pckt bytes"
quietly set sampling_freq [examine -radix decimal /tb_top/rate]
quietly set sampling_period_clk [examine -radix decimal /tb_top/sample_period_cyc]
puts "Sampling period ($sampling_freq Hz) as number of system cycles: $sampling_period_clk"

quietly set fifo_ram_wwidth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/WWIDTH]
puts "FIFO RAM Write Data Width: $fifo_ram_wwidth"
quietly set fifo_ram_rwidth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/RWIDTH]
puts "FIFO RAM Read Data Width: $fifo_ram_rwidth"
quietly set fifo_ram_wdepth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/WDEPTH]
puts "FIFO RAM Write Address Width: $fifo_ram_wdepth"
quietly set fifo_ram_rdepth [examine -d /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/RDEPTH]
puts "FIFO RAM Raed Address Width: $fifo_ram_rdepth"

#quietly set pck_size [expr [string range $nr_channels [string first "d" $nr_channels]+1 end]*4 +8]

add wave -noupdate /tb_top/rst
add wave -noupdate /tb_top/clk
add wave -noupdate /tb_top/spimaster/clk
add wave -noupdate /tb_top/cs
add wave -noupdate /tb_top/spiclk
add wave -noupdate /tb_top/mosi
add wave -noupdate /tb_top/miso
add wave -noupdate /tb_top/spimaster/cs_in
add wave -noupdate /tb_top/uut/fast_data/debug
add wave -noupdate /tb_top/uut/fast_data/SPI_MISO
add wave -noupdate /tb_top/uut/fast_data/st
add wave -noupdate /tb_top/uut/fast_data/received_dat
add wave -noupdate /tb_top/uut/fast_data/rx_valid_pulse
add wave -noupdate /tb_top/uut/fast_data/tx_valid_pulse

add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/fast/rshreg
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/fast/state
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/fast/bitcnt
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/fast/rxreq
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/rxreq
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/nedrx
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/ned_rx
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/nedtx
add wave -noupdate /tb_top/uut/fast_data/fast_spi_slave/txreq

#add wave -noupdate /tb_top/uut/USBnTXE_debug 
#add wave -noupdate /tb_top/uut/USBnWR_debug  
#add wave -noupdate /tb_top/uut/USBCLK_debug  
#add wave -noupdate /tb_top/uut/USBnOE_debug  

#add mem /tb_top/uut/fast_transmitt_out/USBFIFO/FIFOUSB_0/RW1/UI_ram_wrapper_1/L1_asyncnonpipe/mem -a decimal -d hexadecimal

update
WaveRestoreZoom {0 ps} $sim_time

run $sim_time
