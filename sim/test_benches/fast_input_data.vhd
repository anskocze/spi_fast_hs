library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use IEEE.numeric_std.all;

use work.txt_util.all;

library UQDSlib;
use UQDSlib.UQDSlib.all;

entity fast_input_data is
  generic (
           fast_input_data_file	: string  := "fast_input_data_file.dat";
		   noch		: natural := 13;
		   chw		: natural := 32
          );
	port(clk, nrst	: in std_logic;
		data	: out outputChannelsArray;
		en_wr	: out std_logic
		);
end entity;

-- Ch0: U_AP2U
-- Ch1: U_AP2L
-- Ch2: U_AP1U
-- Ch3: U_AP1L
-- Ch4: CircuitCurrent
-- Ch5: Diff_AP2
-- Ch6: Diff_AP1
-- Ch7: Diff_AP12
-- Ch8: ActiveThreshold comp 0
-- Ch9: ActiveDiscriminator comp 0
-- Ch10: ActiveThreshold comp 1
-- Ch11: ActiveDiscriminator comp 1
-- Ch12: Logics

architecture test of fast_input_data is
constant nr_cycle_trans : natural := 150;

file l_file			: TEXT open read_mode is fast_input_data_file;
signal line_cnt, scnt		: natural := 0;
signal after_rst	: boolean := false;

begin

reset_finish: process(nrst)
begin
	if rising_edge(nrst) then
		after_rst <= true;
	end if;
end process;

sampling_out: process(clk)
begin
	if rising_edge(clk) then
		if nrst = '0' then
			en_wr <= '0';
			scnt <= 0;
		else
			scnt <= scnt + 1;
			if scnt = nr_cycle_trans then
				en_wr <= '1';
				scnt <= 0;
			else
				en_wr <= '0';
				scnt <= scnt + 1;
			end if;
		end if;
	end if;
end process;

read_inp_dat : process(clk)
variable data_line	: string(noch*chw/4 + noch-1 downto 1);
variable ch			: std_logic_vector(chw-1 downto 0);
--string(1 to 8);
begin
if rising_edge(clk) then
	if after_rst and en_wr = '1' then
		if endfile(l_file) then
			file_close(l_file);
			line_cnt <= 0;
			file_open(l_file, fast_input_data_file, read_mode);
		else
			str_read(l_file, data_line);
			line_cnt <= line_cnt + 1;
			for i in noch downto 1 loop
				for j in 8 downto 1 loop
					ch(j*4-1 downto (j-1)*4) := hex_to_std_logic_vector(data_line(i*9+j-9));
				end loop;		
				data(noch-i) <= to_std_logic_vector(ch); 
			end loop;
		end if;
	end if;
end if;
end process;

end architecture;
