LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;

entity sampleIn is 
	generic (samplefile : string := "signalInp.dat";
		len : integer :=8;
		nrSamples : integer := 96);
	port (clk : in std_logic;
		rawdata : out std_logic_vector(len-1 downto 0));
end sampleIn;

architecture beh of sampleIn is

type mem is array (0 to nrSamples) of std_logic_vector(len-1 downto 0);
--signal shreg : std_logic_vector(23 downto 0);
signal l : integer := 0;
signal init : std_logic := '0';
signal the_end : boolean := FALSE;
signal max	: integer := 0;

procedure Message ( str : string) is
	variable buf : LINE;
begin
	write(buf, str);
	writeline(output, buf);
end;

begin
init_mem: process
begin
	init <= '1';
	wait;
end process;

sample1: process(init, clk) 

variable inline	: LINE;
variable samples : mem;
variable indata	: std_logic_vector(len-1 downto 0);
variable k, i	: integer := 0;
file     memfile: text;
variable status	: file_open_status;

begin
--initialization at INIT_MEM 
if ( rising_edge(init) ) then
	Message(ht & ht & "Answer file: " & samplefile);
	file_open(status, memfile, samplefile, read_mode);
	if ( status=open_ok ) then
		while (( i <= nrSamples ) and ( not endfile(memfile) )) loop
			readline(memfile, inline);
			hread(inline, indata);
			samples(i) := indata;
			i := i + 1;
--			assert false report "init !" severity note;
		end loop;
		max <= i;
		assert ( false ) report " Test Memory initialized !" severity note;
	else
		assert ( samplefile'length = 0 )
		report "Failed to open memory initialization in read mode"
		severity note;
	end if;
	file_close(memfile);
end if;

if rising_edge(clk) then
	if ( max > l ) then
		rawdata <= samples(l);
		l <= l + 1;
	else
		the_end <= TRUE;
		assert ( false ) report "End of data" severity note;
	end if;
end if;

end process;

end beh;
