LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
use ieee.std_logic_unsigned.all;
USE IEEE.numeric_std.ALL;
use std.textio.all;
use ieee.std_logic_textio.all;
--
--simple behavioural master SPI for testing spi_slave
--A.Skoczen -- AGH-UST -- August 2016
-- 
entity master_spi is 
	generic (samplefile : string := "signalInp.dat";
		len : integer := 8;
		nrSamples : integer := 156;
		cs_high_gap : integer := 8;
		shift : time := 1 ns;
		sclk_idle : std_logic := '0';
		clk_start : std_logic := '0';
		long_cs : boolean := false;
		spi_half_period : time := 25 ns);
	port (cs : out std_logic := '1';
		sclk, mosi : out std_logic;
		rst, miso : in std_logic;
		receive_latch : out std_logic_vector(len - 1 downto 0);
		the_end : out boolean);
end entity;

architecture beh of master_spi is
constant nrPulses : natural := len + 4;

type mem is array (0 to nrSamples) of std_logic_vector(len-1 downto 0);
signal receive : std_logic_vector(nrPulses - 1 downto 0) := x"000";	--, receive_latch
signal l : integer := 0;
--signal k : integer := len-1;
signal init : std_logic := '0';
--signal the_end : boolean := FALSE;
signal max	: integer := 0;
signal sdata : std_logic_vector(nrPulses-1 downto 0);
signal data : std_logic_vector(len-1 downto 0);
signal long : boolean := false;

--constant ec1_ini : integer := 1;
--constant ec0_ini : integer := len+1;
--signal nec1, pec1 : integer range 0 to 9 := ec1_ini;
--signal nec0, pec0 : integer range 0 to 9 := ec0_ini;
signal clk : std_logic := clk_start;
signal cs_in : std_logic;

procedure Message ( str : string) is
	variable buf : LINE;
begin
	write(buf, str);
	writeline(output, buf);
end;

begin

--clk <= sclk_idle after shift, 
--clk <= not clk after spi_half_period;
clock: process begin
	clk <= clk_start;
	wait for shift;
	clk <= not clk;
	while True loop 
		wait for spi_half_period;
		clk <= not clk;
	end loop;
end process;

init_mem: process
begin
	init <= '1';
	wait;
end process;

sample1: process(init, cs_in) 

variable inline	: LINE;
variable samples : mem;
variable indata	: std_logic_vector(len-1 downto 0);
variable k, i	: integer := 0;
file     memfile: text;
variable status	: file_open_status;

begin
--initialization at INIT_MEM 
if ( rising_edge(init) ) then
	Message(ht & ht & "Command file: " & samplefile);
	file_open(status, memfile, samplefile, read_mode);
	if ( status=open_ok ) then
		while (( i < nrSamples ) and ( not endfile(memfile) )) loop
			readline(memfile, inline);
			hread(inline, indata);
			samples(i) := indata;
			i := i + 1;
--			assert false report "init !" severity note;
		end loop;
		max <= i;
		assert ( false ) report " Master Memory initialized !" severity note;
	else
		assert ( samplefile'length = 0 )
		report "Failed to open memory initialization in read mode"
		severity note;
	end if;
	file_close(memfile);
end if;

if falling_edge(cs_in) then
	if ( max > l ) then
		data <= samples(l);
		sdata <= "00" & samples(l) & "00";
		-- if sample(l) = x"33" then
			-- long <= false;
		-- else
			-- prlong <= long;
			-- if sample(l) = x"01" or sample(l) = x"0C" or sample(l) = x"0D" or prlong then
				-- long <= true;
			-- else
				-- long <= false;
			-- end if;
		-- end if;
		l <= l + 1;
	end if;
end if;
if rising_edge(cs_in) then
	if ( max <= l ) then
		assert ( false ) report "End of data" severity note;
		
	end if;
end if;
end process;

finish: process(clk) begin
if rising_edge(clk) then
	if max <= l then
		the_end <= TRUE;
	end if;
end if;
end process;

main_mosi: process begin
	cs_in <= '1';
	--wait until falling_edge(rst);
	for i in 2 downto 0 loop wait until falling_edge(clk); end loop;
	wait for spi_half_period/5;
	if not the_end then
		cs_in <= '0';
		mosi <= '0';
		for i in nrPulses-1 downto 0 loop
			wait for spi_half_period/5;
			wait until rising_edge(clk);
			wait until falling_edge(clk);
			--wait for spi_half_period/5;
			mosi <= sdata(i);
		end loop;
		--wait until falling_edge(clk);
		wait for 4*spi_half_period/5;
	end if;
	--if not long then 
	cs_in <= '1'; --end if;
	--mosi <= '0';
	for i in cs_high_gap downto 0 loop wait until rising_edge(clk); end loop;
end process;
main_miso: process  begin
	wait until rising_edge(clk) and cs_in = '0';
	for k in nrPulses-1 downto 0 loop
		wait until falling_edge(clk) and cs_in = '0';
		receive(k) <= miso;
	end loop;
	wait until rising_edge(clk) and cs_in = '1';
	receive <= x"000";
end process;

latch_rx:  process(cs_in) begin
if rising_edge(cs_in) then
	receive_latch <= receive(nrPulses-3 downto 2);
end if;

end process;

sclk <= clk when cs_in = '0' else sclk_idle;
cs <= '1' when rst = '1' else cs_in;

end beh;
