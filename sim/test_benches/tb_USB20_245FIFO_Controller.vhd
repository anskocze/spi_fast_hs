--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: tb_USB20_245FIFO_Controller.vhd
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	2.9.2019	:	A.Skoczen	:	creation 
--      
--! @brief Test-bench for fast readout USB20_245FIFO_Controller 
-- Description: 
--				
--! @Author: Andrzej Skoczen, AGH-UST
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.txt_util.all;

library modelsim_lib;
use modelsim_lib.util.ALL;

library UQDSlib;
use UQDSlib.UQDSlib.all;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;

entity tb_USB20_245FIFO_Controller is

end entity;

architecture test of tb_USB20_245FIFO_Controller is
constant half_period : time := 12.5 ns;
constant PACKET_SIZE : natural := 8 + CHANNELS_OUT_FR * 4;
constant WRITE_DEPTH : natural := 10;
constant READ_DEPTH : natural := 10;

signal clk : std_logic := '0';
signal nrst : std_logic := '1';
signal fast_clk	: std_logic;
signal txe, rxf, rd, wr, oe, FastReadoutDataRdy, full	: std_logic;
signal data_out : std_logic_vector(7 downto 0);
signal data_in	: data8Array(PACKET_SIZE - 9 downto 0);
signal FastReadoutData    : outputChannelsArray;
signal cmp : boolean;
signal to_compare : data8Array(PACKET_SIZE-9 downto 0);
signal cnt_dgen : natural := 0;
signal nr_sample : integer := 0;
signal wptr : std_logic_vector(WRITE_DEPTH DOWNTO 0);
signal rptr : std_logic_vector(READ_DEPTH DOWNTO 0);
signal ptr_diff : unsigned(READ_DEPTH DOWNTO 0);
--	constant CHANNELS_OUT          : integer := 13;
--	constant REGISTER_SIZE      : INTEGER := 32;
--	constant CHANNELS_OUT_FR          : integer := 9;
--	type outputChannelsArray is array (CHANNELS_OUT - 1 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);	13*32/8 = 13*4 = 52 bytes
--	type data8Array is array (natural range <>) of std_logic_vector(7 downto 0);	PACKET_SIZE = 44 bytes
component USB20_245FIFO_Controller is
	generic(
		PACKET_SIZE : natural := 11
		);
	port(
		-- USB 245FIFO lines
		USBnRXF    : in  std_logic;     -- Data available to be received (not used)
		USBnTXE    : in  std_logic;     -- Data allowed to be sent
		USBCLK     : in  std_logic;
		USBDataBus : out std_logic_vector(7 downto 0);
		USBnRD     : out std_logic;     -- Start receiving data (not used)
		USBnWR     : out std_logic;     -- Start sending data
		USBnOE     : out std_logic;     -- 0 - Enable reading / 1 - Enable writing
		-- FIFO lines
		FIFOData   : in  data8Array(PACKET_SIZE - 9 downto 0); -- Data to be added to FIFO (header, counter and CRC not included)
		FIFOWR     : in  std_logic;     -- Enable writing
		FIFOFull   : out std_logic;
		clk        : in  std_logic;
		nRST       : in  std_logic
		);
end component;

component fast_input_data is
  generic(fast_input_data_file	: string  := "fast_input_data_file.dat";
		   noch		: natural := 13;
		   chw		: natural := 32
          );
	port(clk, nrst	: in std_logic;
		data	: out outputChannelsArray;
		en_wr	: out std_logic
		);
end component;

component ftdi_ft245_synchr_write is 
	generic(
		PACKET_SIZE : natural := 11
		);
	port(wr, oe		: in std_logic;
		txe		: out std_logic;
		clkout	: out std_logic;
		adbus	: in std_logic_vector(7 downto 0)
		);	
end component;

	procedure Message ( str : string ) is
		variable buf : LINE;
	begin
		write(buf, str);
		writeline(output, buf);
	end;
	function conv_to_string(val : data8Array) return string is
	variable str_val : string(1 to 2*(val'length));
	begin
		for i in val'range loop
			--str_val(str_val'length-2*i-1 to str_val'length-2*i) := hstr(val(i));
			str_val(2*i+1 to 2*(i+1)) := hstr(val(i));
		end loop;
		return str_val;
	end;
	
begin

clk <= not clk after half_period;

power_on_reset: process begin
	nrst <= '1';
	wait until rising_edge(clk);
	wait until falling_edge(clk);
	nrst <= '0';
	for i in 0 to 4 loop wait until rising_edge(clk); end loop;
	wait for half_period;
	nrst <= '1';
	wait;
end process;

uut: USB20_245FIFO_Controller
		generic map(PACKET_SIZE => PACKET_SIZE)
		port map (USBCLK => fast_clk, USBnRXF => rxf, USBnTXE => txe, USBnRD => rd, USBnWR => wr, USBnOE => oe, --ftdi_a_cnt(0, 1, 2, 3, 5, 6)
				USBDataBus => data_out,
				clk => clk, nrst => nrst, FIFOFull => full, FIFOData => data_in, FIFOWR => FastReadoutDataRdy);

-- Each channel is split in 4 bytes for fast readout
channel_match : for i in 0 to CHANNELS_OUT_FR - 1 generate
	data_in(4*i)     <= FastReadoutData(i)(7 downto 0);
	data_in(4*i + 1) <= FastReadoutData(i)(15 downto 8);
	data_in(4*i + 2) <= FastReadoutData(i)(23 downto 16);
	data_in(4*i + 3) <= FastReadoutData(i)(31 downto 24);
end generate;

-- Ch0: U_AP2U
-- Ch1: U_AP2L
-- Ch2: U_AP1U
-- Ch3: U_AP1L
-- Ch4: CircuitCurrent
-- Ch5: Diff_AP2
-- Ch6: Diff_AP1
-- Ch7: Diff_AP12
-- Ch8: ActiveThreshold comp 0
-- Ch9: ActiveDiscriminator comp 0
-- Ch10: ActiveThreshold comp 1
-- Ch11: ActiveDiscriminator comp 1
-- Ch12: Logics

data_generator: fast_input_data
	generic map(fast_input_data_file => "./data/in_fast_dat.dat")
	port map(clk => clk, nrst => nrst, data => FastReadoutData, en_wr => FastReadoutDataRdy);

nr_sample <= <<signal .tb_USB20_245FIFO_Controller.uut.sample_cnt : integer range 0 to integer'high >>;
counter_gen_data: process(clk)
begin
	if rising_edge(clk) then
		if FastReadoutDataRdy = '1' then
			cnt_dgen <= cnt_dgen + 1;
			Message("Packet number: " & natural'image(cnt_dgen) & " (" & integer'image(nr_sample) & ")");
		end if;
	end if;
end process;
	
model_ftdi: ftdi_ft245_synchr_write
	generic map(PACKET_SIZE => PACKET_SIZE)
	port map(clkout => fast_clk, wr => wr, oe => oe,
			adbus => data_out, txe => txe);	

cmp <= <<signal .tb_USB20_245FIFO_Controller.model_ftdi.compare : boolean >>;
to_compare <= <<signal .tb_USB20_245FIFO_Controller.model_ftdi.dbuf : data8Array(PACKET_SIZE-9 downto 0) >>;
comparison_of_data: process(fast_clk)
begin
	if rising_edge(fast_clk) then
		if cmp then
			if data_in = to_compare then
				Message("Data OK");
			else
				Message("Data WRONG at " & time'image(now));
				Message("Sent:     " & conv_to_string(data_in));
				Message("Received: " & conv_to_string(to_compare));
			end if;
		end if;
	end if;
end process;

wptr <= <<signal .tb_USB20_245FIFO_Controller.uut.USBFIFO.FIFOUSB_0.L31.U_corefifo_async.wptr : std_logic_vector(WRITE_DEPTH DOWNTO 0)>>;   
rptr <= <<SIGNAL .tb_USB20_245FIFO_Controller.uut.USBFIFO.FIFOUSB_0.L31.U_corefifo_async.rptr : std_logic_vector(READ_DEPTH DOWNTO 0)>>; 
ptr_diff <= unsigned(wptr) - unsigned(rptr);
			
end architecture;
