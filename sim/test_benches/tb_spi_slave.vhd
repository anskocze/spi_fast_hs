library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use IEEE.NUMERIC_STD.ALL;
library UQDSlib;
use UQDSlib.UQDSlib.all;
--
--simple testbench for unit spi_slave
--A.Skoczen -- AGH-UST -- August 2016
--A.Skoczen -- AGH-UST -- July 2021
--
entity tb_spi_slave is

end entity;

architecture test of tb_spi_slave is
constant half_period : time := 12.5 ns;
constant shift : time := 1 ns;
constant len : natural := 8;
constant nrSamples : natural := 98;
constant answer_length : natural := 5;

signal clk, rst, nrst, dataclk : std_logic := '0';
signal cs, spiclk, mosi, miso, rx_valid_pulse, tx_valid_pulse : std_logic;
signal te : boolean;
signal for_transmit_gated, for_transmit, received_dat : std_logic_vector(len-1 downto 0);
--signal trans_cnt : unsigned(2 downto 0);
type states is (idle, A, B, C, TR, TR_DATA);
signal st, nst : states;
signal master_received : std_logic_vector(len - 1 downto 0);
signal trcnt : unsigned(2 downto 0);

component spi_slave is 				
generic(trans_length : natural := 8);
port (clk, nrst : in std_logic;
    --spi hw lines
    spi_csn			:in std_logic;
	spi_sclk		:in std_logic;
	spi_sdin		:in std_logic;
	spi_sdout		:out std_logic;
	--internal signals
	rdata			:out std_logic_vector(trans_length-1 downto 0);
	wdata			:in std_logic_vector(trans_length-1 downto 0);
	rx_valid_pulse	:out std_logic;
	tx_valid_pulse	:out std_logic
);
end component;

component master_spi is 
	generic (samplefile : string := "signalInp.dat";
		len : integer := 8;
		nrSamples : integer := 156;
		cs_high_gap : integer := 8;
		shift : time := 1 ns;
		sclk_idle : std_logic := '0';
		long_cs : boolean := false;
		spi_half_period : time := 25 ns);
	port (cs : out std_logic := '1';
		sclk, mosi : out std_logic;
		rst, miso : in std_logic;
		receive_latch : out std_logic_vector(len - 1 downto 0);
		the_end : out boolean);
end component;

component sampleIn is 
	generic (samplefile : string := "signalInp.dat";
		len : integer :=8;
		nrSamples : integer := 96);
	port (clk : in std_logic;
		rawdata : out std_logic_vector(len-1 downto 0));
end component;

begin
clk <= not clk after half_period;
rst <= '1' after 1 ns, '0' after 3*half_period;
nrst <= not rst;

data_for_slave : sampleIn
		generic map (samplefile => "./data/slave_send.dat", len => len)
		port map(clk => dataclk, rawdata => for_transmit);
--dataclk <= clk when st = TR_DATA else '0';
clock_for_data_for_slave: process(clk)
begin	
	if rising_edge(clk) then
		if rst = '1' then
			dataclk <= '0';
		else
			if st = TR_DATA then
				dataclk <= '1';
			else
				dataclk <= '0';
			end if;	
		end if;
	end if;
end process;
		
--for_transmit_gated <= for_transmit when st = TR else x"00";

uut : spi_slave
		generic map(trans_length => len)
		port map(clk => clk, nrst => nrst, spi_csn => cs, spi_sclk => spiclk, spi_sdin => mosi, spi_sdout => miso, 
				rdata => received_dat, wdata => for_transmit, rx_valid_pulse => rx_valid_pulse, tx_valid_pulse => tx_valid_pulse);

master : master_spi
		generic map(samplefile => "./data/master_send.dat", len => len, spi_half_period => 2*half_period, shift => shift)
		port map(rst => rst, cs => cs, sclk => spiclk, mosi => mosi, miso => miso, 
				receive_latch => master_received, the_end => te); 

state_reg_pattern24: process(clk)
begin	
	if rising_edge(clk) then
		if rst = '1' then
			st <= idle;
		else
			st <= nst;
		end if;
	end if;
end process;

next_st_log: process(all)
begin
		case st is
			when idle => if rx_valid_pulse = '0' then
							nst <= idle;
						else
							if received_dat = x"04" then
								nst <= A;
							else
								nst <= idle;
							end if;
						end if;
			when A => if rx_valid_pulse = '0' then
							nst <= A;
						else
							if received_dat = x"06" then
								nst <= B;
							elsif received_dat = x"04" then
								nst <= A;
							else
								nst <= idle;
							end if;
						end if;
			when B => if rx_valid_pulse = '0' then
							nst <= B;
						else
							if received_dat = x"02" then
								nst <= C;
							else
								nst <= idle;
							end if;
						end if;
			when C => if tx_valid_pulse = '1' then
							nst <= TR_DATA;
						else
							nst <= C;
						end if;
			when TR_DATA => nst <= TR;
			when TR => if tx_valid_pulse = '1' then
							nst <= idle when trcnt = answer_length else TR_DATA;
						else
							nst <= TR;
						end if;
		end case;
end process;

transmittion_counter: process(clk)
begin	
	if rising_edge(clk) then
		if rst = '1' then
			trcnt <= (others => '0');
		else
			if st = TR_DATA then
				trcnt <= trcnt + 1;
			elsif st = idle then
				trcnt <= (others => '0');
			end if;
		end if;
	end if;
end process;
		
end architecture;
