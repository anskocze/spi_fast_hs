--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: ftdi_ft245_synchr_write.vhd
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	4.9.2019	:	A.Skoczen	:	creation 
--      
--! @brief Model of the FTDI as FT245 synchronous FIFO mode 
-- Description: 
--				
--! @Author: Andrzej Skoczen, AGH-UST
--
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;
use work.txt_util.all;

entity ftdi_ft245_synchr_write is 
	generic(
		PACKET_SIZE : natural := 11;		--packet size sholud be unknown for this unit
		sample_period_cyc_int : natural := 40	--value to compare with step
		);
	port(wr, oe		: in std_logic;
		txe		: out std_logic;
		clkout : out std_logic := '0';
		adbus	: in std_logic_vector(7 downto 0)
		);	
end entity;

architecture beh of ftdi_ft245_synchr_write is
--type membuf is array (natural range <>) of std_logic_vector(7 downto 0);
type data8Array is array (natural range <>) of std_logic_vector(7 downto 0);
constant usb_half_period : time := 8350 ps;		--10.0 ns;
--constant sys_half_period : time := 12500 ps;
constant sys_period : time := 25.0 ns;
constant header : data8Array(2 downto 0) := ((x"CC"), (x"BB"), (x"AA"));
constant h_ts_cs : natural := 9;

signal tsbuf : data8Array(3 downto 0);
signal dbuf : data8Array(PACKET_SIZE-h_ts_cs downto 0);		-- -1
--signal i, k : natural := 0;
signal header_found, tstamp_finished : boolean := false;
signal cnt, dcnt : natural := 0;
signal dcount, count, compare, clean : boolean := false;
signal temp, clean_ts : boolean := false;
signal time_error_cnt : natural := 0;

type header_states is (idle, rec_aa, rec_bb, rec_cc, rec_ts, transmit);
signal st_reg, nx_st_reg : header_states := idle; 

	procedure Message ( str : string ) is
		variable buf : LINE;
	begin
		write(buf, str);
		writeline(output, buf);
	end;

	function conv_to_natural(val : data8Array) return natural is
	type int_vect is array (3 downto 0) of natural;
	variable int_val : natural := 0;
	constant dec_place : int_vect := (1, 256, 65536, 16777216);
	begin
		for i in val'range loop
			int_val := int_val + dec_place(i)*to_integer(unsigned(val(i)));
		end loop;
		return int_val;
	end;

	function conv_to_string(val : data8Array) return string is
	variable str_val : string(1 to 2*(val'length));
	begin
		for i in val'range loop
			--str_val(str_val'length-2*i-1 to str_val'length-2*i) := hstr(val(i));
			str_val(2*i+1 to 2*(i+1)) := hstr(val(i));
		end loop;
		return str_val;
	end;

begin
clkout <= not clkout after usb_half_period;

-- ??????????????????
--full FIFO in FTDI
fifo_full: process begin
	txe <= '0';
	for i in 0 to sample_period_cyc_int*10 loop wait until rising_edge(wr); end loop;
	txe <= '1';
	for i in 0 to sample_period_cyc_int loop wait until rising_edge(clkout); end loop;
	txe <= '0';
end process;

-- recognize and report of header AABBCC
fsm_header_reg: process(clkout)
begin
	if rising_edge(clkout) then
		if wr = '0' then
			st_reg <= nx_st_reg;
		else
			if clean then
				st_reg <= idle;
			end if;
		end if;
	end if;
end process;
--
fsm_header_comb: process(all)
begin		
	header_found <= false;
	count <= false;
	nx_st_reg <= idle;
	case st_reg is
		when idle => if adbus = x"AA" and wr = '0' then
						nx_st_reg <= rec_aa;
					else
						nx_st_reg <= idle;
					end if;
		when rec_aa => if adbus = x"BB" and wr = '0' then
						nx_st_reg <= rec_bb;
					else
						nx_st_reg <= idle;
					end if;
		when rec_bb => if adbus = x"CC" and wr = '0' then
						nx_st_reg <= rec_cc;
					else
						nx_st_reg <= idle;
					end if;
		when rec_cc => if wr = '0' then
						nx_st_reg <= rec_ts;
						header_found <= true;
						count <= true;
					else
						nx_st_reg <= rec_cc;
					end if;
		when rec_ts => if cnt <= 3 then --wr = '0' and 
						nx_st_reg <= rec_ts;
						count <= true;
					else
						nx_st_reg <= transmit;
					end if;
		when transmit => if clean then
						nx_st_reg <= idle;
					else
						nx_st_reg <= transmit;
					end if;
	end case;
end process;

heade_message: process(all)
begin
	if header_found then
		Message("Header " & time'image(now));
	end if;
end process;

-- read out and report time stamp
time_stamp_counter:  process(clkout)
begin
	if rising_edge(clkout) then
		if (count and wr = '0') or clean_ts then
			cnt <= cnt + 1;
		end if;
		if clean then
			cnt <= 0;
		end if;
	end if;
end process;

--tstamp_finished <= count and (cnt = 2) and (wr = '0');
time_stamp_collect: process(clkout)
begin
	if rising_edge(clkout) then
		if count and wr = '0' then
			tsbuf(cnt) <= adbus;
		end if;
	end if;
end process;

time_stamp_calc: process(clkout)
variable int_tsbuf, int_tsbuf_prev, step : natural := 0;
variable time_step : time;
variable ts_micros : real;
begin
	if falling_edge(clkout) then
		if cnt = 4 then
			int_tsbuf_prev := int_tsbuf;
			int_tsbuf := conv_to_natural(tsbuf);
			step := int_tsbuf - int_tsbuf_prev;
			time_step := step*sys_period;
			ts_micros := real(time_step/(1 ps))/1.0e6;
			Message("Time stamp: " & natural'image(int_tsbuf) & "(hex:" & conv_to_string(tsbuf) &") at " & time'image(now) & " Step: " & natural'image(step) & " => " & time'image(time_step) & " => " & real'image(ts_micros) & " us");
			if step /= sample_period_cyc_int then
				Message("ERROR: Step is incorrect ! Should be " & natural'image(sample_period_cyc_int) & " but there is " & natural'image(step));
				time_error_cnt <= time_error_cnt + 1;
			end if;
			clean_ts <= true;
		else
			clean_ts <= false;
		end if;
	end if;
end process;

-- data receiving
data_counter_gate:  process(clkout)
begin
	if falling_edge(clkout) then --and wr = '0' 
		if cnt = 4 then
			dcount <= true;
		else
			if dcnt = PACKET_SIZE - h_ts_cs + 1 then
				dcount <= false;
			end if;
		end if;
	end if;
end process;

data_counter:  process(clkout)
begin
	if rising_edge(clkout) then
		if dcount then
			if wr = '0' then
				dcnt <= dcnt + 1;
			end if;
		else
			dcnt <= 0;
		end if;
	end if;
end process;

compare <= temp and not dcount;
compare_pulse: process(clkout)
begin
	if rising_edge(clkout) then
		temp <= dcount;
		clean <= compare;
	end if;
end process;
--(not dcount) 
receive_data: process(clkout)
begin
	if rising_edge(clkout) then
		if dcount and (wr = '0') then
			dbuf(dcnt) <= adbus;
		else
			if clean then
				for i in 0 to PACKET_SIZE - h_ts_cs loop
					dbuf(i) <= x"XX";
				end loop;
			end if;
		end if;
	end if;
end process;
		
-- CRC or parity check

end architecture;
