--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: tb_top.vhd for comm_fast small project (USB20_245FIFO_Controller)
-- File pre-history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	2.9.2019	:	A.Skoczen	:	creation 
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	12.2.2020	:	A.Skoczen	:	copu & modification 
--      
--! @brief Test-bench for fast readout top 
-- Description: 
--	necessary modification to avoid using RTL pulse FastReadoutDataRdy (tb_top.uut.fast_transmitt_out.FIFOWR) !!!
--	and to avoid hazard related to sequence of cmp and FastReadoutDataRdy pulses
--	wrong functionality when rate is to low !
--				
--! @Author: Andrzej Skoczen, AGH-UST
--
--------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use std.textio.all;

library work;
use work.txt_util.all;

library modelsim_lib;
use modelsim_lib.util.ALL;

library UQDSlib;
use UQDSlib.UQDSlib.all;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;

entity tb_top is
	generic(rate : real := 700.0e3);
end entity;

architecture test of tb_top is
constant half_period : time := 12500 ps;
constant half_period_real : real := (2.0e-12)*real(half_period/(1 ps));
--constant rate : real := 700.0e3;
constant rate_period : real := 1.0/rate;
constant sample_period_cyc : real := rate_period/real(half_period_real);
constant PACKET_SIZE : natural := 8 + CHANNELS_OUT_FR * 4;
constant WRITE_DEPTH : natural := 10;
constant READ_DEPTH : natural := 10;
constant nsch : natural := CHANNELS_OUT_FR/2;		-- channel with sinus
constant len : natural := 8;
constant shift : time := 21 ns;
constant clk_start : std_logic := '1';

signal clk : std_logic := '0';
signal nrst : std_logic := '1';
signal rst : std_logic := '0';
signal fast_clk	: std_logic;
signal txe, rxf, rd, wr, oe, FastReadoutDataRdy	: std_logic;
signal data_out : std_logic_vector(7 downto 0);
signal data_in	: data8Array(PACKET_SIZE - 9 downto 0);
signal FastReadoutData    : outputChannelsArray;
signal cmp : boolean;
signal to_compare : data8Array(PACKET_SIZE-9 downto 0);
signal cnt_dgen : natural := 0;
signal nr_sample : integer := 0;
signal wptr : std_logic_vector(WRITE_DEPTH DOWNTO 0);
signal rptr : std_logic_vector(READ_DEPTH DOWNTO 0);
signal ptr_diff : unsigned(READ_DEPTH DOWNTO 0);
signal fifofull : std_logic;
signal chsin : std_logic_vector(31 DOWNTO 0);
signal data_error_cnt : natural := 0;
signal cs, spiclk, mosi, miso : std_logic;
signal te : boolean;
signal master_received : std_logic_vector(len - 1 downto 0);
signal slave_received : std_logic_vector(len-1 downto 0);

--	constant CHANNELS_OUT          : integer := 13;
--	constant REGISTER_SIZE      : INTEGER := 32;
--	constant CHANNELS_OUT_FR          : integer := 9;
--	type outputChannelsArray is array (CHANNELS_OUT - 1 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);	13*32/8 = 13*4 = 52 bytes
--	type data8Array is array (natural range <>) of std_logic_vector(7 downto 0);	PACKET_SIZE = 44 bytes
component top is
	generic(
		PACKET_SIZE : natural := 8 + CHANNELS_OUT_FR * 4;
		nsch : natural := 5;
		sample_period_cyc_int : natural := 100
	);
	port(
		-- slow clock for data generation
		clk        : in  std_logic;
		nRST       : in  std_logic;
		-- USB 245FIFO lines
		--USBnRXF    : in  std_logic;     -- Data available to be received (not used)
		USBnTXE    : in  std_logic;     -- Data allowed to be sent
		USBnRD     : out std_logic;     -- Start receiving data (not used)
		USBnWR     : out std_logic;     -- Start sending data
		USBCLK     : in  std_logic;
		USBnOE     : out std_logic;     -- 0 - Enable reading / 1 - Enable writing
		USBDataBus : out std_logic_vector(7 downto 0);
		-- debug
		-- USBnTXE_debug    : out  std_logic;		-- Data allowed to be sent - from FTDI
		-- USBnWR_debug     : out std_logic;		-- Start sending data
		-- USBCLK_debug     : out  std_logic;		-- Fast clock - from FTDI
		-- USBnOE_debug     : out std_logic		-- 0 - Enable reading / 1 - Enable writing		
		USBnTXE_debug    : out  std_logic;		-- Data allowed to be sent - from FTDI
		USBnWR_debug     : out std_logic;		-- Start sending data
		USBCLK_debug     : out  std_logic;		-- Fast clock - from FTDI
		-- USBnOE_debug     : out std_logic;		-- 0 - Enable reading / 1 - Enable writing	
		FIFOWR_debug		: out std_logic;		-- sampling pulse
		SPI_MOSI		: in  std_logic;
		SPI_MISO		: out  std_logic;
		SPI_CLOCK		: in  std_logic;
		SPI_CS			: in  std_logic);
end component;

component ftdi_ft245_synchr_write is 
	generic(
		PACKET_SIZE : natural := 11;
		sample_period_cyc_int : natural := 40
		);
	port(wr, oe		: in std_logic;
		txe		: out std_logic;
		clkout	: out std_logic;
		adbus	: in std_logic_vector(7 downto 0)
		);	
end component;

component master_spi is 
	generic (samplefile : string := "signalInp.dat";
		len : integer := 8;
		nrSamples : integer := 156;
		cs_high_gap : integer := 8;
		shift : time := 11 ns;
		sclk_idle : std_logic := '0';
		clk_start : std_logic := '0';
		long_cs : boolean := false;
		spi_half_period : time := 25 ns);
	port (cs : out std_logic := '1';
		sclk, mosi : out std_logic;
		rst, miso : in std_logic;
		receive_latch : out std_logic_vector(len - 1 downto 0);
		the_end : out boolean);
end component;

	procedure Message ( str : string ) is
		variable buf : LINE;
	begin
		write(buf, str);
		writeline(output, buf);
	end;
	function conv_to_string(val : data8Array) return string is
	variable str_val : string(1 to 2*(val'length));
	begin
		for i in val'range loop
			--str_val(str_val'length-2*i-1 to str_val'length-2*i) := hstr(val(i));
			str_val(2*i+1 to 2*(i+1)) := hstr(val(i));
		end loop;
		return str_val;
	end;
	
begin

clk <= not clk after half_period;

power_on_reset: process begin
	nrst <= '1';
	wait until rising_edge(clk);
	wait until falling_edge(clk);
	nrst <= '0';
	for i in 0 to 3 loop wait until rising_edge(clk); end loop;
	wait for half_period;
	nrst <= '1';
	wait;
end process;

rst <= not nrst;

spimaster : master_spi
		generic map(samplefile => "./data/master_send.dat", len => len, spi_half_period => 2*half_period, shift => shift, clk_start => clk_start)
		port map(rst => rst, cs => cs, sclk => spiclk, mosi => mosi, miso => miso, 
				receive_latch => master_received, the_end => te); 
slave_received <= <<signal .tb_top.uut.fast_data.received_dat : std_logic_vector(len-1 downto 0)>>;				
uut: top
		generic map(PACKET_SIZE => PACKET_SIZE, nsch => nsch, sample_period_cyc_int => integer(sample_period_cyc))
		port map (USBCLK => fast_clk, USBnTXE => txe, USBnRD => rd, USBnWR => wr, USBnOE => oe, --ftdi_a_cnt(0, 1, 2, 3, 5, 6)	USBnRXF => rxf, 
				USBDataBus => data_out,
				clk => clk, nrst => nrst, 
				SPI_MOSI => mosi, SPI_MISO => miso, SPI_CLOCK => spiclk, SPI_CS => cs);

-- Ch0: U_AP2U
-- Ch1: U_AP2L
-- Ch2: U_AP1U
-- Ch3: U_AP1L
-- Ch4: CircuitCurrent
-- Ch5: Diff_AP2
-- Ch6: Diff_AP1
-- Ch7: Diff_AP12
-- Ch8: ActiveThreshold comp 0
-- Ch9: ActiveDiscriminator comp 0
-- Ch10: ActiveThreshold comp 1
-- Ch11: ActiveDiscriminator comp 1
-- Ch12: Logics

nr_sample <= <<signal .tb_top.uut.fast_transmitt_out.sample_cnt : integer range 0 to integer'high >>;
FastReadoutDataRdy <= <<signal .tb_top.uut.fast_transmitt_out.FIFOWR : std_logic >>;
counter_gen_data: process(clk)
begin
	if rising_edge(clk) then
		if FastReadoutDataRdy = '1' then
			cnt_dgen <= cnt_dgen + 1;
			Message("Packet number: " & natural'image(cnt_dgen) & " (" & integer'image(nr_sample) & ")");
		end if;
	end if;
end process;
	
model_ftdi: ftdi_ft245_synchr_write
	generic map(PACKET_SIZE => PACKET_SIZE, sample_period_cyc_int => integer(sample_period_cyc))
	port map(clkout => fast_clk, wr => wr, oe => oe,
			adbus => data_out, txe => txe);	

cmp <= <<signal .tb_top.model_ftdi.compare : boolean >>;
input_data_for_comparison : process(FastReadoutDataRdy)
begin
	if rising_edge(FastReadoutDataRdy) then		--take value updated on falling edge of previous pulse
		data_in <= <<signal .tb_top.uut.FIFOData : data8Array(PACKET_SIZE-9 downto 0) >>;
	end if;
end process;
to_compare <= <<signal .tb_top.model_ftdi.dbuf : data8Array(PACKET_SIZE-9 downto 0) >>;

check_sinus : process(cmp) begin
	if cmp then
		chsin <= to_compare(nsch*4) & to_compare(nsch*4+1) & to_compare(nsch*4+2) & to_compare(nsch*4+3);
	end if;
end process;

comparison_of_data: process(fast_clk)
begin
	if rising_edge(fast_clk) then
		if cmp then
			if data_in = to_compare then
				Message("Data OK");
				Message("Received: " & conv_to_string(to_compare));
			else
				Message("Data WRONG at " & time'image(now));
				Message("Sent:     " & conv_to_string(data_in));
				Message("Received: " & conv_to_string(to_compare));
				data_error_cnt <= data_error_cnt + 1;
			end if;
		end if;
	end if;
end process;

fifofull <= <<signal .tb_top.uut.fast_transmitt_out.FIFOFull : std_logic>>;
fifo_full_monitor : process(fifofull) begin
	if rising_edge(fifofull) then
		Message("FIFO Full - FIFO Full - FIFO Full - FIFO Full - FIFO Full - FIFO Full"); 
	end if;
end process;

wptr <= <<signal .tb_top.uut.fast_transmitt_out.USBFIFO.FIFOUSB_0.L31.U_corefifo_async.wptr : std_logic_vector(WRITE_DEPTH DOWNTO 0)>>;   
rptr <= <<SIGNAL .tb_top.uut.fast_transmitt_out.USBFIFO.FIFOUSB_0.L31.U_corefifo_async.rptr : std_logic_vector(READ_DEPTH DOWNTO 0)>>; 
ptr_diff <= unsigned(wptr) - unsigned(rptr);


			
end architecture;
