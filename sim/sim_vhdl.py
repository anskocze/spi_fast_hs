# -*- coding: utf-8 -*-
"""
Created on Fri Jul  5 15:49:36 2019
Description: Control of compilation and simulation flow for uQDS project
    
@author: anskocze
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - July 2019
"""
import os
import re
import subprocess
import glob
import shutil
import sys

if os.path.exists('..\\'):
    sys.path.append('..\\')
    import igloo2 as ig2 
else:
    print('No access to igloo2 package')
    


#def is_tool(name):
    #"""Check whether `name` is on PATH and marked as executable """
    # from whichcraft import which
    #from shutil import which
#    return shutil.which(name) is not None

def one_vhdl(vhdlfile, lib, log):
    new = []
    if os.path.exists(vhdlfile):
        tool = 'vcom.exe'
        if ig2.is_tool(tool):
            args = [tool]
#            args.extend(['-suppress=12110'])
            args.extend(['-work'])
            args.extend([lib])
            args.extend(['-2008'])
            args.extend(['-explicit'])
            args.extend(['+acc=nrv'])

            args.extend([vhdlfile])
            #print("CMD: ", args)
            #print("wait for output ...")
            pout = subprocess.run(args, capture_output=True, universal_newlines=True)
            tx = pout.stdout.split('\n')
            #l = len(tx)
            line = tx[len(tx)-2]
            new.append(line[line.find('Errors: ')+8:line.rfind(',')])
            new.append(line[line.find('Warnings: ')+10:])
            print(vhdlfile, '\t=>', line)
            log.write(pout.stdout)
            #print(pout.stdout)
        else:
            print(tool, "is not reachable - check your path.cfg and varaiable PATH")
            return new
    else:
        print("Lack of vhdl file:", vhdlfile)
        return new
    log.write('=============================================================================================\n')
    return new

def add_lib(tb_lib, tlibs):
    if(not os.path.exists(tlibs) or not os.path.isdir(tlibs)):
        os.makedirs(tlibs)

    dir_tb_lib = os.path.join(tlibs, tb_lib)
    tool = 'vlib.exe'
    if ig2.is_tool(tool):
        args = [tool]
        #args.extend(['-type directory'])
        args.extend([dir_tb_lib])
        #print("CMD: ", args)
        #print("wait for output ...")
        subprocess.run(args)
    #exit()
    tool = 'vmap.exe'
    if ig2.is_tool(tool):
        args = [tool]
        args.extend([tb_lib])
        args.extend([dir_tb_lib])
        subprocess.run(args)
    print('=============================================================================================')
    return True

def removed(items, original_list, only_duplicates=False, inplace=False):
    """By default removes given items from original_list and returns
    a new list. Optionally only removes duplicates of `items` or modifies
    given list in place.
    """
    if not hasattr(items, '__iter__') or isinstance(items, str):
        items = [items]

    if only_duplicates:
        result = []
        for item in original_list:
            if item not in items or item not in result:
                result.append(item)
    else:
        result = [item for item in original_list if item not in items]

    if inplace:
        original_list[:] = result
    else:
        return result
    
def generate_vhdl_units_list(file):
    if os.path.isfile(file):
        text = open(file).readlines()
    else:
        print(file, 'does not exists')
        ig2.script_help('cfg')
        exit()
    cfg_blk_length = len(text)
    #print(cfg_blk_length)
    list_units = []
    for ll in range(1,cfg_blk_length):
        #print(ll)
#Requiremets for VHDL syntax: 
#instance_name : unit_name
#must be placed in separate line befor 'generic map' or 'port map''
#The name of file containing unit must be the same as entity name: <unit_name>.vhd
        if( re.search('generic map', text[ll], re.IGNORECASE) or re.search('port map', text[ll], re.IGNORECASE) ):
            unit = text[ll-1]
            if ( unit.find('--') < 0 and  unit.find(':') > 0 ):
                #print(unit)
                list_units.append(unit.split(':')[1].strip() + '.vhd')
    #print('List of TB units: ', list_units)
    cleaned_list_units = removed(list_units, list_units, only_duplicates=True)
    return cleaned_list_units

def generate_vhdl_packages_list(file):
#it must be modified to use also with RTL
    pckg_path = os.path.dirname(file)
    text = open(file).readlines()
    list_pkgs = []
    for ll in range(1,len(text)):
        if( re.search('use work.', text[ll], re.IGNORECASE)):
            use = text[ll]
            #print(use)
            list_pkgs.append(use.split('work.')[1].split('.')[0] + '.vhd')
    cleaned_list_units = removed(list_pkgs, list_pkgs, only_duplicates=True)
    cleaned_list_units_path = []
    for x in cleaned_list_units:
        cleaned_list_units_path.append(os.path.join(pckg_path, x))
    #print('Packages for TB list: ', cleaned_list_units_path)
    return cleaned_list_units_path

def search_lib(lib, unit):
    print('Looking into', lib, 'library for unit', unit, end = '')
    tool = 'vdir.exe'
    if ig2.is_tool(tool):
        args = [tool]
        args.extend(['-lib'])
        args.extend([lib])
        args.extend(['-l'])
        args.extend([unit])
        #print("CMD: ", args)
        pout = subprocess.run(args, capture_output=True, universal_newlines=True)
        tx = pout.stdout.split('\n')
        if len(tx) > 1:
            print('\t=> OK')
            return True
        else:
            print(tx)
            return False
    else:
        print(tool, "is not reachable - check your path.cfg and varaiable PATH")
        return False

def compilation(tb_list, list_units):
    """ Launch compilation with modelsim """
    sf2 = 'smartfusion2';
    path_sf2 = 'C://Microsemi//Libero_SoC_v12.0//Designer//lib//modelsim//precompiled//vlog';
    add_lib(sf2, path_sf2);

    src_lib = '..\\hdl\\lib'
    src_src = '..\\hdl\\src'
    tlibs = '.\\lib'
    prjlib = 'UQDSlib'
    add_lib(prjlib, tlibs)

    tb_lib = 'work'
    add_lib(tb_lib, tlibs)

    n_err = 0
    n_war = 0
    complog = '.\\vcom.log'
    log = open(complog, 'w')
    #lf = glob.glob(src_lib + '\\*.vhd')
    lf = [src_lib+'\\util.vhd', src_lib+'\\config_block_lib.vhd', src_lib+'\\UQDSLib.vhd', src_lib+'\\buildstamp.vhd', src_lib+'\\reg_map_pkg.vhd', src_lib+'\\fifo_pkg.vhd']
    for fv in lf:
        new = one_vhdl(fv, prjlib, log)
        n_err = n_err + int(new[0])
        n_war = n_war + int(new[1])
    #lf = glob.glob(src_src + '\\*.vhd')
    for fv in list_units:
        if fv == 'config_block.vhd':
            pattern = '@brief'
            fname = os.path.join(src_src, fv)
            print('======================================== Config Block ========================================')
            ig2.purpose_of_config_block(fname, pattern)
        prj_file = os.path.join(src_src, fv)
        if os.path.exists(prj_file):
            new = one_vhdl(prj_file, prjlib, log)
            n_err = n_err + int(new[0])
            n_war = n_war + int(new[1])
        else:
            print('No', fv, 'file in design library', prjlib)
            search_lib(sf2, fv.split('.')[0])
    #top_file = os.path.join('..\\hdl', 'Top.vhd')
    #new = one_vhdl(top_file, prjlib, log)
    #n_err = n_err + int(new[0])
    #n_war = n_war + int(new[1])
    #lt = ['tb_top2.vhd', 'LTC2378_model.vhd']
    for tbf in tb_list:
        #tb_file = os.path.join('.\\test_benches', tbf)
        new = one_vhdl(tbf, tb_lib, log)
        #print(new)
        n_err = n_err + int(new[0])
        n_war = n_war + int(new[1])
    log.close()
    print('Total number of errors: ', n_err)
    print('Total number of warnings: ', n_war)
    print('Check full compilation log:', complog)
    return n_err

def simulation(dofile):
    """ Launch simulation with modelsim """
    #print('Still empty simulation procedure !!!')
    if os.path.exists(dofile):
        #tool = 'modelsim.exe'
        tool = 'vsim.exe'
        if ig2.is_tool(tool):
            args = [tool]
            args.extend(['-do'])
            args.extend([dofile])
            print("CMD: ", args)
            print("wait for output ...")
            subprocess.run(args)
        else:
            print(tool, "is not reachable - check your path.cfg and varaiable PATH")
            return False
    else:
        print("Lack of .do file:", dofile)
        return False
    return False

def show_globals():
    print("source directory = ",src_dir)
    print("project name = ",prj_name)
    print("library name = ",lib_name)
    print("top name = ",top_name)
    print("path to Git = ",gitp)
    print("path to Libero = ",liberop)
    print("path to ModelSim = ",modelsimp)
    print("path to Synplify = ",synplifyp)

def clean():
    dirlist = ['.\\lib']
    for direc in dirlist:
        if os.path.exists(direc):
            shutil.rmtree(direc)
            print(direc," removed")
    return True

""" load project config """
fname = '..\config.cfg'
with open(fname, 'r') as fin:
    lines = fin.readlines()
for line in lines:
    one_entry = line.splitlines()[0].split('=',2)
    #print(one_entry)
    if (one_entry[0] == 'source_dir'):
        src_dir = one_entry[1]
    if (one_entry[0] == 'proj_name'):
        prj_name = one_entry[1]
    if (one_entry[0] == 'lib_name'):
        lib_name = one_entry[1]
    if (one_entry[0] == 'top_name'):
        top_name = one_entry[1]

""" load PATH config """
fname = '..\path.cfg'
with open(fname, 'r') as fin:
    lines = fin.readlines()
for line in lines:
    one_entry = line.splitlines()[0].split('=',2)
    if (one_entry[0] == 'Libero'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            liberop = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
            lver = ig2.find_between(liberop, '_v', '.')
        else:
            liberop = "Error: no Libero path"
    if (one_entry[0] == 'Synplify'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            synplifyp = one_entry[1]
            #check_add_path(one_entry[0],one_entry[1])
            ig2.check_add_path('Synopsys',one_entry[1])
        else:
            synplifyp = "Error: no Synplify path"
    if (one_entry[0] == 'ModelSim'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            modelsimp = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
        else:
            modelsimp = "Error: no ModelSim path"
    if (one_entry[0] == 'Git'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            gitp = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
        else:
            gitp = "Error: no Git path"
            
""" manage arguments """
if len(sys.argv) == 2:
    if (sys.argv[1] == 'clean'):
        print('Cleaning ...')
        clean()
        print('Done')
        exit()
    else:
        #vhdllist_config_block = generate_vhdl_units_list('..\\hdl\\src\\config_block.vhd')
        Top_file = '..\\hdl\\src\\'+sys.argv[1]+'.vhd'
        print('Top: ', Top_file)
        vhdllist_Top = generate_vhdl_units_list(Top_file)
        #vhdllist = vhdllist_Top + vhdllist_config_block
        vhdllist_l1 = []
        print('===============================================================')
        for vhdl in vhdllist_Top:
            print('Level 0:', vhdl)
            vhdllist_l1 = vhdllist_l1 + generate_vhdl_units_list(os.path.join('..\\hdl\\src\\', vhdl))
        #print(vhdllist)
        vhdllist_l2 = []
        for vhdl in vhdllist_l1:
            print('Level 1:', vhdl)
            vhdllist_l2 = vhdllist_l2 + generate_vhdl_units_list(os.path.join('..\\hdl\\src\\', vhdl))
        vhdllist_l3 = []
        for vhdl in vhdllist_l2:
            print('Level 2:', vhdl)
            vhdllist_l3 = vhdllist_l3 + generate_vhdl_units_list(os.path.join('..\\hdl\\src\\', vhdl))
        vhdllist_l4 = []
        for vhdl in vhdllist_l3:
            print('Level 3:', vhdl)
            vhdllist_l4 = vhdllist_l4 + generate_vhdl_units_list(os.path.join('..\\hdl\\src\\', vhdl))
        for vhdl in vhdllist_l4:
            print('Level 4:', vhdl)
        list_units = vhdllist_Top + vhdllist_l1 + vhdllist_l2 + vhdllist_l3 + vhdllist_l4 + [sys.argv[1]+'.vhd']
        #print(list_units)
        #list_units.extend([Top_file])
        cleaned_list_units = removed(list_units, list_units, only_duplicates=True)
        #for ff in cleaned_list_units:
            #print(ff)
        #exit()
        tf = os.path.basename(sys.argv[1])
        tb_file = '.\\test_benches\\tb_'+tf+'.vhd'
        print('TB file: ', tb_file)
        if not os.path.isfile(tb_file):
            print('There is not testbench for ', sys.argv[1])
            print('List of possible arguments:')
            ltf = glob.glob('.\\test_benches\\tb_*.vhd')
            for ftb in ltf:
                #print(ftb)
                print('\t', ftb.split('tb_')[1].strip('.vhd'))
            with open('.\\test_benches\\read.me', 'r') as fin:
                print(fin.read())
                ig2.script_help('cmd')
            exit()
        else:
            vhdllist_tb = generate_vhdl_units_list(tb_file)
            #print(vhdllist_tb)
            tb_list = [tb_file]
            tb_list_pkg = []
            print('Initial tb_list: ', tb_list)
            for vhdl in vhdllist_tb:
                if ( vhdl.lower() != tf.lower()+'.vhd' ):
                    print('Level TB:', vhdl, ' ', tf)
                    tb_list_pkg = tb_list_pkg + generate_vhdl_packages_list(os.path.join('.\\test_benches\\', vhdl))
                    tb_list = tb_list + generate_vhdl_units_list(os.path.join('.\\test_benches\\', vhdl)) + [os.path.join('.\\test_benches\\', vhdl)]
                    #tb_list = tb_list + generate_vhdl_packages_list(os.path.join('.\\test_benches\\', vhdl))
            cleaned_tb_list_pkg = removed(tb_list_pkg, tb_list_pkg, only_duplicates=True)
            tb_list = cleaned_tb_list_pkg + tb_list
            print('Final tb_list: ', tb_list)
            if ( compilation(tb_list, cleaned_list_units) == 0 ):
                #for vhdl in vhdllist + vhdllist_l1:
                #    print('Full:', vhdl)
                simulation(os.path.join('.\\do_files', 'tb_'+tf+'.do'))
            else:
                print('Simultaion not started due to errors')
if ( len(sys.argv) > 2 ):
    #print(len(sys.argv))
    if (sys.argv[2] == 'sim'):
        tf = os.path.basename(sys.argv[1])
        simulation(os.path.join('.\\do_files', 'tb_'+tf+'.do'))


