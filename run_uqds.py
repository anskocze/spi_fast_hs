# -*- coding: utf-8 -*-
"""
Created on Fri Mar 15 18:01:42 2019
Description: Control of design flow for uQDS project

@author: anskocze
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - March 2019
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - July - September 2019: wide development 
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - January-February 2020 - further development
A.Skoczen, AGH-UST, FP&ACS; CERN, TE-MPE-EP - March 2020 - new arguments, deep rearrangement
"""

import os  
import time
import sys
import shutil
import datetime
import re
import glob
import igloo2 as ig2

print(sys.version)
if sys.version_info.major != 3:
    print("Wrong python version")
    exit()
    
if os.path.exists('.\\DOC'):
    sys.path.append('.\\DOC')
    import reg_map_pkg as rmp
else:
    print('No access to reg_map_pkg package')
    exit()

if os.path.exists('.\\constraints'):
    sys.path.append('.\\constraints')
    import gen_pdc_pkg as pdc
else:
    print('No access to gen_pdc_pkg package')
    exit()

""" load project config """
fname = '.\config.cfg'
if not os.path.exists(fname):
    print("\nERROR")
    print("Lack of ", fname, "file -> create it:")
    ig2.script_help('cfg')
    exit()
    
with open(fname, 'r') as fin:
    lines = fin.readlines()
for line in lines:
    one_entry = line.splitlines()[0].split('=',2)
    #print(one_entry)
    if one_entry[0] == 'source_dir':
        src_dir = one_entry[1]
    if one_entry[0] == 'proj_name':
        prj_name = one_entry[1]
    if one_entry[0] == 'lib_name':
        lib_name = one_entry[1]
    if one_entry[0] == 'top_name':
        top_name = one_entry[1]
    if one_entry[0] == 'tech':
        fpga = one_entry[1]
        if not fpga in ig2.fpga_nb_sle:
            print('ERROR: Wrong value of \'tech\' in config.cfg')
            print('Available values: ',  end='')
            for key in ig2.fpga_nb_sle :
                print(key, '', end='')
            ig2.script_help('cmd')
            exit()
    if one_entry[0] == 'lic_check':
        if one_entry[1] == 'yes':
            ig2.lic_check_allowed = True
    if one_entry[0] == 'xls_ext':
        ig2.xls_ext = one_entry[1]
    if one_entry[0] == 'debug_impl':
        if one_entry[1] == 'yes':
            ig2.debug_impl = True
    if one_entry[0] == 'backanno':
        if one_entry[1] == 'yes':
            ig2.backanno = True

""" load PATH config """
fname = '.\path.cfg'
if not os.path.exists(fname):
    print("\nERROR")
    print("Lack of ", fname, "file -> create it:")
    ig2.script_help('cfg')
    exit()
    
modelsimp = ""
gitp = ""
with open(fname, 'r') as fin:
    lines = fin.readlines()
for line in lines:
    one_entry = line.splitlines()[0].split('=',2)
    if (one_entry[0] == 'Libero'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            liberop = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
            lver = ig2.find_between(liberop, '_v', '.')
        else:
            liberop = "Error: no Libero path"
    if (one_entry[0] == 'Flashpro'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            flashp = one_entry[1]
            #check_add_path(one_entry[0],one_entry[1])
            ig2.fp_path = one_entry[1]
        else:
            flashp = "Error: no Flashpro path"
    if (one_entry[0] == 'Synplify'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            synplifyp = one_entry[1]
            #check_add_path(one_entry[0],one_entry[1])
            ig2.check_add_path('Synopsys',one_entry[1])
        else:
            synplifyp = "Error: no Synplify path"
    if (one_entry[0] == 'ModelSim'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            modelsimp = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
        else:
            modelsimp = "Error: no ModelSim path"
    if (one_entry[0] == 'Git'):
        if(os.path.exists(one_entry[1]) and os.path.isdir(one_entry[1])):
            gitp = one_entry[1]
            ig2.check_add_path(one_entry[0],one_entry[1])
        else:
            gitp = "Error: no Git path"

if os.path.isdir(os.path.join(src_dir,'config_blocks')):
    if len(sys.argv) == 1 or sys.argv[1] != "reg_map":
        config_block_file = os.path.join(src_dir, 'src\\config_block.vhd')
        if not os.path.exists(config_block_file):
            print('Lack of config block file', config_block_file)
            print('Assign a Config Block to your design: python run_uqds reg_map config_block_number')
            ig2.script_help('cmd')
            exit()
        cnt, cb_purpose = ig2.purpose_of_config_block(config_block_file, '@brief')
        if cnt != 1:
            print('Edit your Config Block VHDL file and add or modifie @brief line')
            ig2.script_help('cmd')
            exit()
else:
    cb_purpose = 'No config block in current design'

#variables global to run_uqds
#files
sdc_resfile  = os.path.join('.\\synplify\\synthesis', os.path.basename(prj_name)+'_sdc.sdc')
#ig2.find_first_file_sub('.\\synplify', 'sdc')
synth_resfile  = os.path.join('.\\synplify\\synthesis', os.path.basename(prj_name)+'.edn')
#ig2.find_first_file_sub('.\\synplify', 'edn')
srr_file = os.path.join('.\\synplify\\synthesis', os.path.basename(prj_name)+'.srr')
xlsx_file = os.path.join('.\\constraints\\', os.path.basename(prj_name)+ig2.xls_ext+'.xlsx')
#strings
vhdl = os.path.join('.\\hdl\\lib', os.path.basename(lib_name)+'.vhd')
tmr_line = '	constant rad_control        : string  := \"tmr\";'
none_line = '	constant rad_control        : string  := \"none\";'

def show_globals():
    path_tab = os.environ['PATH'].split(';')
    print('=====================================================')
    print('PATH:')
    for pp in path_tab:
        print(pp)
    print('=====================================================')
    print('Project configuration parameters:')
    print('\tsource directory = ', src_dir)
    print('\tproject name = ', prj_name)
    print("\tlibrary name = ", lib_name)
    print("\ttop name = ", top_name)
    print("\tFPGA = ", fpga)
    print("\txls_ext = ", ig2.xls_ext, '=> Pinout config file:', xlsx_file)
    print("\tlic_check = ", ig2.lic_check_allowed)
    print("\tbackanno = ", ig2.backanno)
    print("\tdebug_impl = ", ig2.debug_impl)
    print('=====================================================')
    print('Configuration of paths to toolss:')
    if gitp and gitp.strip:
        print("\tpath to Git = ", gitp)
    else: 
        print("ERROR: Git is not configured")
    if liberop and liberop.strip():
        print("\tpath to Libero = ", liberop)
    else: 
        print("ERROR: Libero is not configured")
    if modelsimp and modelsimp.strip(): 
        print('\tpath to ModelSim = ', modelsimp)
    else: 
        print("ERROR: ModelSim is not configured")
    if synplifyp and synplifyp.strip():
        print("\tpath to Synplify = ", synplifyp)
    else: 
        print("ERROR: Synplify is not configured")
    return True

def run_synth():
    if ig2.buildstamp(src_dir, 'buildstamp.vhd'):
        print("Built stamp")
    else:
        return False
    if len(sys.argv) == 3:
        argtmr = sys.argv[2]
    else:
        argtmr = 'none'

    if argtmr == 'tmr':
        ig2.replace_in_file(vhdl, none_line, tmr_line)
    elif argtmr == 'none':
        ig2.replace_in_file(vhdl, tmr_line, none_line)
    else:
        print('Wrong second optional argument for synthesize')
        print('Possible values: tmr, none')
        print('Missing of this argument is equivalent to none')
        return False
    if ig2.tmr_check(vhdl, tmr_line):
        print('Design in mode TMR')
    else:
        print('Design in mode NONE TMR')
    #if not ig2.tmr_fdc(argtmr,lib_name,top_name):
    #    exit()
    if(ig2.workDir(src_dir) == 0):
        print("Warning: !!!")
    #synth_resfile  = ig2.find_first_file_sub('.\\synplify', 'edn')
    t0 = datetime.datetime.now()
    print("Start time:", t0.strftime('%H:%M:%S'))
    print(cb_purpose)
    if not ig2.synthesize(synth_resfile, src_dir):
        return False
    if os.path.exists(synth_resfile):
        print("EDN ",synth_resfile," created")
    else:
        print("no EDN output from synthesis")
    if os.path.exists(sdc_resfile):
        dst = os.path.join('.\\constraints',os.path.basename(sdc_resfile))
        shutil.copyfile(sdc_resfile,dst)
        print("SDC copied from synthesis directory to",dst)
    else:
        print("no SDC output from synthesis")
    dt = str(datetime.datetime.now()-t0)
    print("Total Run Time:",dt," of synthesis phase")
    if os.path.exists(srr_file):
        ig2.clocks(srr_file)
        ig2.resources(srr_file, fpga)
    else:
        print("no SRR logfile from synthesis")
    print('Design with Config Block:', cb_purpose)
    return True

def run_impl():
    if(ig2.workDir(src_dir) == 0):
        print("Warning: !!!")
        return False
    ndcf = os.path.join('.\\constraints', os.path.basename(top_name)+'_io.ndc')
    tmr = ig2.tmr_check(vhdl, tmr_line)
    if tmr:
        print('Design in mode TMR')
        if os.path.exists(ndcf):
            os.remove(ndcf)
    else:
        print('Design in mode NONE TMR')
    #add check TMR in SRR file and report a conflict
    t0 = datetime.datetime.now()
    print("Start time:", t0.strftime('%H:%M:%S'))
    print('Design with Config Block:', cb_purpose)
    print('Input for Libero: ', synth_resfile)
    ig2.implement(synth_resfile, lver, src_dir, top_name, prj_name, tmr)
    dt = str(datetime.datetime.now()-t0)
    print("Total Run Time:",dt," of implemetation phase")
    tv_path = '.\\actel\\designer\\' + top_name
    if os.path.exists(tv_path):
        tv_files = [f for f in os.listdir(tv_path) if re.search(r'.timing_violations.', f)]
        #print(tv_files)
        ig2.time_violations(tv_files, tv_path)
        io_bank_file = os.path.join(tv_path, top_name + '_bankrpt.rpt')
        #print(io_bank_file)
        if ig2.io_func_rep(io_bank_file):
            iocomb_rep = os.path.join(tv_path, top_name + '_ioff.rpt')
            if os.path.exists(iocomb_rep): 
                print('I/O Register Combining Summary is available in file ', iocomb_rep)
        resources_log = os.path.join(tv_path, top_name + '_layout_log.log')
        if os.path.exists(resources_log):
            ig2.resources_impl(resources_log)
    else:
        print('Debug mode - implemntation not done')
    print('Design with Config Block:', cb_purpose)
    return True
    
""" manage arguments """
if len(sys.argv) > 1:
    #############################################
    #synthesis
    if sys.argv[1] == "synth":
        run_synth()
        exit()
        #############################################
        #conver PPD to STAPL
    elif sys.argv[1] == "stapl":
        #synth_resfile  = ig2.find_first_file_sub('.\\synplify', 'edn')
        ig2.export_stapl(synth_resfile, lver, top_name, prj_name)
        #############################################
		# generate PDC (and NDC in case at ver 12)
        # default is NONE TMR
    elif sys.argv[1] == "pdc":
        if len(sys.argv) > 2:
            in_file = int(sys.argv[2])
        else:
            in_file = xlsx_file
        print("Libero version:",lver)
        if pdc.gen_pdc(int(lver), in_file, top_name, False):
            print('Done')
        else:
            print('Failed')
            exit()
        #############################################
		# generate reg map package for specified config block from excel file and copy to lib folder
		# copies specified config block to main source dir
		# copies specified config block library to lib dir
    elif sys.argv[1] == "reg_map":
        if len(sys.argv) > 2:
            config_block_select = int(sys.argv[2])
        else:
            print('Lack of second argument')
            print('Usage: python run_uqds reg_map index_to_data_sheet_in_Excel_file')
            exit()
        gen = rmp.generate(config_block_select, '.\\DOC', '.\\hdl\\lib')
        if gen == 1:
            print('Package generated')
        else:
            print('Error: Package not generated')
            exit()
        cb_path = '.\\hdl\\config_blocks'
        config_block_file = os.path.join(cb_path, 'config_block_'+sys.argv[2]+'.vhd')
        config_block_lib_file = os.path.join(cb_path, 'config_block_'+sys.argv[2]+'_lib.vhd')
        if not os.path.exists(config_block_file):
            print('File', config_block_file, 'does not exist: reg_map_pkg is not synchronized with config_block !')
            cb_files = [f for f in os.listdir(cb_path) if os.path.isfile(os.path.join(cb_path, f))]
            print('Available files in', cb_path)
            for f in cb_files:
                print(f)
        elif not os.path.exists(config_block_lib_file):
            print('File', config_block_lib_file, 'does not exist: reg_map_pkg is not synchronized with config_block !')
            cb_files = [f for f in os.listdir(cb_path) if f.endswith("_lib.vhd") if os.path.isfile(os.path.join(cb_path, f))]
            print('Available lib files in', cb_path)
            for f in cb_files:
                print(f)
        else:
            fname = os.path.join('.\\hdl\\src', 'config_block.vhd')
            fname_lib = os.path.join('.\\hdl\\lib', 'config_block_lib.vhd')
            print('Synchronisation with config block:')
            # metadata preserved
            shutil.copy2(config_block_file, fname)
            print(config_block_file, '=>', fname)
            # metadata preserved
            shutil.copy2(config_block_lib_file, fname_lib)
            print(config_block_lib_file, '=>', fname_lib)
            pattern = '@brief'
            ig2.purpose_of_config_block(fname, pattern)
        exit()
        #############################################
        #displey revision number and built stamp
    elif sys.argv[1] == "rev":
        if ig2.buildstamp(src_dir, 'buildstamp.vhd'):
            print("Built stamp")
        else:
            exit()
        #############################################
        #deisplay config block number
    elif sys.argv[1] == "cb_nr":
        print('Current config block number is:', ig2.cfg_blk_nr('.\\hdl'))
        #############################################
        # Implement with Libero
    elif sys.argv[1] == "impl":
        run_impl()
        exit()
        #############################################
        #check TMR mode of design 
    elif sys.argv[1] == "tmr":
        cnttmr = -1
        #srr_file = os.path.join('.\\synplify\\synthesis', os.path.basename(prj_name)+'.srr')
        if os.path.exists(srr_file):
            cnttmr = open(srr_file, 'r').read().count('TMR')
            print('Number of instances synthesized with TMR:', cnttmr)
        else:
            print('No synthesis result')
        #vhdl = os.path.join('.\\hdl\\lib', os.path.basename(lib_name)+'.vhd')
        #tmr_line = '	constant rad_control        : string  := \"tmr\";'
        src_tmr = ig2.tmr_check(vhdl,tmr_line)
        if src_tmr:
            print('Source code: rad_control set to TMR')
        else:
            print('Source code: rad_control set to NONE TMR')
        if cnttmr > 0:
            if not src_tmr:
                print('Conflict: rad_contr; is \"none\" but synthesis is with TMR')
        elif cnttmr == 0:
            if src_tmr:
                print('Conflict: rad_contr; is \"tmr\" but synthesis is without TMR')
        #############################################
        #clean design 
    elif sys.argv[1] == "all":      
        if run_synth():
            print("EDIF from synplify: ", synth_resfile)
            run_impl()
        else:
            print('Synthesis failed')
        #############################################
        #programm FPGA device 
    elif sys.argv[1] == "prg":
        to_find = '.\\bitstream\\' + prj_name + '_' + ig2.cfg_blk_nr('.\\hdl') + '_*' + '.stp'
        dir_archive = '.\\bitstream_backup\\'
        #print('Looking for:', to_find)
        l = glob.glob(to_find)
        if len(l) == 1:
            prg_file = l[0]
            print('Using default STAPL file:', prg_file)
            repeat_conf = True
            while repeat_conf:
                conf = input('Confirm [y] or change to one of the archive files [n] or cancel [c] : ')
                if conf == 'n':
                    i = 0
                    print('Choose from the list of files in directory', dir_archive, ':')
                    lstp = glob.glob(dir_archive + '*.stp')
                    #os.listdir(dir_archive)
                    for f in lstp:
                        i += 1
                        print(i, ". ", f, '\t', time.ctime(os.path.getctime(f)))
                    prompt = 'Choose 1 to ' + str(i) + ': '
                    inp_iff = input(prompt)
                    if inp_iff.isdigit():
                        iff = int(inp_iff)
                        if iff <= i:
                            prg_file =  lstp[iff-1]
                            #prg_file = os.path.join(dir_archive, lstp[iff-1])
                            print('File', prg_file, 'was choosen for programming FPGA')
                            repeat_conf = False
                        else:
                            print('Wrong answer')
                    else:
                        print('Wrong answer')
                elif conf == 'c':
                    repeat_conf = False
                    print('Programming cancelled')
                    exit()
                elif conf == 'y':
                    repeat_conf = False
                else:
                    print('Wrong answer')
        else:
            if len(l) != 0:
                print('More than one STAPL file:')
                for f in l:
                    print('\t', f)
            else:
                print('Lack of STAPL file:', to_find)
                exit()
        if ig2.program_fpga(prj_name, prg_file):
            print('Done OK')
        else:
            print('Programming of  FPGA device -> Failed')
        #############################################
        #clean design 
    elif sys.argv[1] == "clean":
        ig2.clean()
        #############################################
        #clean only implementation and leave synthesis 
    elif sys.argv[1] == "clean_impl":
        ig2.clean_impl()
        #############################################
        #show congiguration parameters and paths
    elif sys.argv[1] == "show_cfg":
        show_globals()
        #############################################
        #display help
    elif sys.argv[1] == "cfg_help":
        ig2.script_help('cfg')
        #############################################
        #display help
    elif sys.argv[1] == "help":
        ig2.script_help('cmd')
        #############################################
        #display help
    else: ig2.script_help('cmd')
else: ig2.script_help('cmd')
