--------------------------------------------------------------------------------
--! Company: CERN
--!
--! @File: top.vhd for comm_fast small project (USB20_245FIFO_Controller)
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--				v1.0	:	27.1.2020	:	A.Skoczen	:	creation 
--				v2.0	:	12.2.2020	:	A.Skoczen	:	data generator added
--      
--! @brief Top unit for fast readout USB20_245FIFO_Controller 
-- Description: 
--		Values for sample_period_cyc_int:
--			ch=24	min=107  (374kHz)
--			ch=18	min=83  (480kHz)	
--			ch=15	min=71  (565kHz)	
--			ch=12	min=59	(680kHz)
--			ch=9	min=47  (860kHz)	
--			ch=7	min=29  (1020kHz)
--			ch=5	min=31  (1300kHz)	
--
--! @Author: Andrzej Skoczen, AGH-UST, CERN-TE-MPE-EP
--
--------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

library UQDSlib;
use UQDSlib.UQDSlib.all;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;

entity top is
	generic(
		PACKET_SIZE : natural := 8 + CHANNELS_OUT_FR * 4;
		nsch : natural := 5;
		sample_period_cyc_int : natural := 110
	);
	port(
		-- slow clock for data generation
		clk        : in  std_logic;
		nRST       : in  std_logic;
		LED			: OUT   std_logic_vector(2 downto 0);
		-- USB 245FIFO lines
		--USBnRXF    : in  std_logic;     -- Data available to be received (not used)
		USBnTXE    : in  std_logic;     -- Data allowed to be sent
		USBnRD     : out std_logic;     -- Start receiving data (not used)
		USBnWR     : out std_logic;     -- Start sending data
		USBCLK     : in  std_logic;
		USBnOE     : out std_logic;     -- 0 - Enable reading / 1 - Enable writing
		USBDataBus : out std_logic_vector(7 downto 0);
		-- debug
		USBnTXE_debug    : out  std_logic;		-- Data allowed to be sent - from FTDI
		USBnWR_debug     : out std_logic;		-- Start sending data
		USBCLK_debug     : out  std_logic;		-- Fast clock - from FTDI
		-- USBnOE_debug     : out std_logic;		-- 0 - Enable reading / 1 - Enable writing	
		FIFOWR_debug		: out std_logic;		-- sampling pulse
		SPI_MOSI		: in  std_logic;
		SPI_MISO		: out  std_logic;
		SPI_CLOCK		: in  std_logic;
		SPI_CS			: in  std_logic);
end entity;

architecture test of top is
signal FIFOfull, FIFOWR	: std_logic;
signal FIFOData	: data8Array(PACKET_SIZE - 9 downto 0);

component USB20_245FIFO_Controller is
	generic(
		PACKET_SIZE : natural := 11
		);
	port(
		-- USB 245FIFO lines
		--USBnRXF    : in  std_logic;     -- Data available to be received (not used)
		USBnTXE		: in  std_logic;     -- Data allowed to be sent
		USBCLK		: in  std_logic;
		USBDataBus	: out std_logic_vector(7 downto 0);
		USBnRD		: out std_logic;     -- Start receiving data (not used)
		USBnWR		: out std_logic;     -- Start sending data
		USBnOE		: out std_logic;     -- 0 - Enable reading / 1 - Enable writing
		-- FIFO lines
		FIFOData	: in  data8Array(PACKET_SIZE - 9 downto 0); -- Data to be added to FIFO (header, counter and CRC not included)
		FIFOWR		: in  std_logic;     -- Enable writing
		FIFOFull	: out std_logic;
		clk			: in  std_logic;
		nRST		: in  std_logic
		);
end component;

component data_gen is
  generic(noch		: natural := 13;
			PACKET_SIZE : natural := 8 + noch * 4;
		   chw		: natural := 32;
		   nsch : natural := 5;
		   nr_cycle_trans : natural := 100
          );
	port(clk, nrst	: in std_logic;
		data	: out data8Array(PACKET_SIZE - 9 downto 0);
		en_wr, debug	: out std_logic;
		SPI_MOSI		: in  std_logic;
		SPI_MISO		: out  std_logic;
		SPI_CLOCK		: in  std_logic;
		SPI_CS			: in  std_logic);
end component;

signal blink_led_counter	: unsigned(23 downto 0);

begin

	-- Leds
	LED(0) <= nrst;
	LED(1) <= blink_led_counter(22);

	-- Blink Led Counter 							
	BlinkLed : process(CLK)
	begin
		if rising_edge(CLK) then
			if nrst = '0' then
				blink_led_counter <= (others => '1');
			else
				if blink_led_counter > 0 then
					blink_led_counter <= blink_led_counter - 1;
				else
					blink_led_counter <= (others => '1');
				end if;
			end if;
		end if;
	end process;
	
USBnTXE_debug <= USBnTXE;
USBnWR_debug <= USBnWR;
USBCLK_debug <= USBCLK;
-- USBnOE_debug <= USBnOE;
FIFOWR_debug <= FIFOWR;

fast_data: data_gen
		generic map(noch => CHANNELS_OUT_FR, PACKET_SIZE => PACKET_SIZE, nsch => nsch, nr_cycle_trans => sample_period_cyc_int)
		port map (clk => clk, nrst => nrst, data => FIFOData, en_wr => FIFOWR, debug => LED(2), 
		SPI_MOSI => SPI_MOSI, SPI_MISO => SPI_MISO, SPI_CLOCK => SPI_CLOCK, SPI_CS => SPI_CS);
--SPI_MISO <= SPI_CLOCK;		--LED(0)
fast_transmitt_out: USB20_245FIFO_Controller
		generic map(PACKET_SIZE => PACKET_SIZE)
		port map (USBCLK => USBCLK, USBnTXE => USBnTXE, USBnRD => USBnRD, USBnWR => USBnWR, USBnOE => USBnOE, --ftdi_a_cnt(0, 1, 2, 3, 5, 6)
				USBDataBus => USBDataBus,
				clk => clk, nrst => nrst, FIFOFull => FIFOFull, FIFOData => FIFOData, FIFOWR => FIFOWR);	
	
end architecture;
