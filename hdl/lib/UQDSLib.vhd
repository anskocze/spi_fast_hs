--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: ucom_v2_0.vhd
-- File history:
--      <Revision number>: <Date>: <Comments>
--		  V A1.0 			 :	10.11.2016 : modified version for the altera FPGA
--		  V m1.0 			 :	29.11.2017 : Modified for the Microsemi IGLOO2
--
-- Description: 
-- Register control block used for a configuration of the device via RS485 interface, 
-- it decodes address, and command from the packet and propagates thete data to upper 
-- entity together with read/write strobe and register address
--
-- 
--
-- Targeted device: originally (<Family::IGLOO2> <Die::M2GL060> <Package::484 FBGA>)
-- 
--	
-- Author: Jens Steckert (Josef Kopal)
-- 
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
USE IEEE.fixed_pkg.all;

library UQDSLib;
use UQDSLib.util.all;
use UQDSLib.config_block_Lib.all;


package UQDSLib is
	---------------
	-- Constants --
	---------------
	constant rad_control        : string  := "none";
	constant REGISTER_SIZE      : INTEGER := 32; --register size 32 bit should be sufficient for every thing it is generic but I would prefer not to change it;
	
	constant CHANNELS_IN_MAX       : integer := 16;
	constant CHANNELS_OUT_MAX      : integer := 32;
	constant TRIGGER_LINES_MAX     : integer := 18;
	constant register_adr_width 	: integer := 8;
	
	
	
	--============================constants for recycled DQQDL code===========================================
	--mostly decimating buffer and cy_sram_interface  
	--ADC settings
	constant nb_ch              : integer                := 6;
	constant adc_data_width     : integer range 15 to 25 := 16;
	--type vect_array is array (natural range <>) of std_logic_vector(31 downto 0);
	--SRAM settings
	
	
	--=========================================================================================================
	
	constant data_path_width    : integer                := 32;

	-----------
	---Types---
	-----------
	type channelsArray is array (natural range <>) of std_logic_vector(31 downto 0);
	type channelsArray16B is array (natural range <>) of std_logic_vector(15 downto 0);
	type regDataArray is array (natural range <>) of std_logic_vector(REGISTER_SIZE - 1 downto 0);
	type regChannelsArray is array (31 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);
	type outputChannelsArray is array (CHANNELS_OUT - 1 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);
	type customRegArray is array (127 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);
	type channelConfigArray is array (31 downto 0) of std_logic_vector(REGISTER_SIZE - 1 downto 0);
	type regArrayCurDepSettings is array (IDepSettingsSteps -1 downto 0) of std_logic_vector(REGISTER_SIZE -1 downto 0);
	
	type regArray is array (0 to 2**REGISTER_ADR_WIDTH - 1) of std_logic_vector(REGISTER_SIZE - 1 downto 0);

	type data8Array is array (natural range <>) of std_logic_vector(7 downto 0);
	--TYPE fifoArray IS ARRAY (NATURAL RANGE <>) OF STD_LOGIC_VECTOR;
	type currentlevelArray is array (natural range <>) of integer range 1 to 31;

	Type flash_inputs is record
		cmd         : std_logic_vector(3 downto 0);
		transparent : std_logic;
		address     : std_logic_vector(23 downto 0);
		wdata       : std_logic_vector(7 downto 0);
		wdata2      : std_logic_vector(7 downto 0);
		wdata3      : std_logic_vector(7 downto 0);
		wdata4      : std_logic_vector(7 downto 0);

	end record;

	Type flash_outputs is record
		busy       : std_logic;
		write_ok   : std_logic;
		read_valid : std_logic;
		last_adr   : std_logic_vector(23 downto 0);
		status     : std_logic_vector(7 downto 0);
		rdata      : std_logic_vector(7 downto 0); --holds byte in one byte mode
		rdata2     : std_logic_vector(15 downto 0); --holds two bytes in two byte mode
		rdata4     : std_logic_vector(31 downto 0);
	end record;

	Type AT_flash_outputs is record
		sck  : std_logic;
		si   : std_logic;
		cs   : std_logic;
		wp   : std_logic;
		hold : std_logic;

	end record;

	Type channel_generic_settings is record
		mux_data               : std_logic_vector(7 downto 0);
		mux_daq                : std_logic_vector(1 downto 0);
		flt_mov_avg_depth_bits : integer range 0 to 12;
		flt_median_depth       : integer range 2 to 126;
		flt_median_middle      : integer range 1 to 62;
		cor_gain               : std_logic_vector(cor_c_width - 1 downto 0);
		cor_offset             : std_logic_vector(data_path_width - 1 downto 0);
	end record;

	--define default fixed notations for various signal types here
	--Voltage: +/- 128V, 60nV/LSB
	subtype sf_voltage is sfixed(7 downto -24);

	--Current: +/- 32.768 kA, 15.3uA/LSB
	subtype sf_current is sfixed(15 downto -16);

	--Current derivative: +/- 32.768 kA/s, 15.3uA/s * LSB
	subtype sf_didt is sfixed(15 downto -16);

	----------------
	---Components---
	----------------
	COMPONENT ComReg IS
		GENERIC(
			dwidth : integer range 8 to 64 := 32
			--reset default for register
		);
		port(clk, rst     : in  std_logic;
		     resetDefault :     std_logic_vector(dwidth - 1 downto 0);
		     registerMask : in  std_logic_vector(dwidth - 1 downto 0);
		     din          : in  std_logic_vector(dwidth - 1 downto 0);
		     --write enable enables writing to the registers (only relevant for the writable registers)
		     wEN          : in  std_logic;
		     ReadOnly     : in  std_logic;
		     --freezes register update (global signal for all registers)
		     freeze       : in  std_logic;
		     dout         : out std_logic_vector(dwidth - 1 downto 0)
		    );
	end COMPONENT;

	--	COMPONENT ucom_v2_0 IS
	--		PORT(
	--			-- RS485
	--			uartComTX_FTDI  : OUT STD_LOGIC;
	--			uartComRX_FTDI  : IN  STD_LOGIC;
	--			uartComTX_RS485 : OUT STD_LOGIC;
	--			uartComRX_RS485 : IN  STD_LOGIC;
	--			--SPI
	--			spi_csn         : in  std_logic;
	--			spi_sclk        : in  std_logic;
	--			spi_sdin        : in  std_logic;
	--			spi_sdout       : out std_logic;
	--			--Registes
	--			regAddress      : OUT STD_LOGIC_VECTOR(REGISTER_ADR_WIDTH - 1 downto 0);
	--			dataIn          : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	--			--regWR                 :   OUT registerVectorArray;
	--			regWRsig        : OUT STD_LOGIC_VECTOR(2**REGISTER_ADR_WIDTH - 1 downto 0);
	--			--output of selected register going back into communication module						
	--			dataOut         : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
	--			--regRD                 :   OUT	registerVectorArray;
	--			regRDSig        : OUT STD_LOGIC_VECTOR(2**REGISTER_ADR_WIDTH - 1 downto 0);
	--			-- standard
	--			clk             : IN  STD_LOGIC;
	--			nrst            : IN  STD_LOGIC;
	--			rstOut          : OUT STD_LOGIC
	--		);
	--	END COMPONENT;
	component reg_com is
		port(
			clk             : in  std_logic;
			rst             : in  std_logic;
			-- RS485 and FTDI
			uartComTX_FTDI  : out std_logic;
			uartComRX_FTDI  : in  std_logic;
			uartComTX_RS485 : out std_logic;
			uartComRX_RS485 : in  std_logic;
			--SPI
			spi_csn         : in  std_logic;
			spi_sclk        : in  std_logic;
			spi_sdin        : in  std_logic;
			spi_sdout       : out std_logic;
			-- Write register
			dataIn          : out std_logic_vector(31 downto 0);
			regWRsig        : out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
			-- Read register
			regAddress      : out std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);
			dataOut         : in  std_logic_vector(31 downto 0)
		);
	end component;

	component serialStream is
		generic(
			channels : integer range 1 to 16 := 1
		);
		port(
			clk      : in  std_logic;
			reset    : in  std_logic;
			uart_rx  : in  std_logic;
			uart_tx  : out std_logic;
			hartBeat : in  std_logic;
			DataIn   : in  channelsArray(channels - 1 downto 0);
			debugSig : out std_logic_vector(7 downto 0)
		);
	end component;
	component adc_LTC2378_20 is
	port(
		-- FPGA side
		clk        		: in  std_logic;
		reset      		: in  std_logic;
		start_conv 		: in  std_logic;
		data_rdy   		: out std_logic;
		data_out   		: out std_logic_vector(19 downto 0);
		adc_constant 	: out std_logic;
		adc_stuck		: out std_logic;
		-- ADC side
		sdi        		: in  std_logic;
		sck        		: out std_logic;
		busy       		: in  std_logic;
		conv       		: out std_logic
	);
	end component;
	component flash_at25f512b is
		port(
			clk           : in  std_logic;
			rst           : in  std_logic;
			sck           : out std_logic;
			so            : in  std_logic;
			si            : out std_logic;
			cs            : out std_logic;
			wp            : out std_logic;
			hold          : out std_logic;
			cmd           : in  std_logic_vector(3 downto 0);
			busy          : out std_logic;
			pstatus       : out std_logic_vector(7 downto 0);
			paddress      : in  std_logic_vector(23 downto 0);
			pwdata        : in  std_logic_vector(7 downto 0);
			pwdata2       : in  std_logic_vector(7 downto 0);
			pwdata3       : in  std_logic_vector(7 downto 0);
			pwdata4       : in  std_logic_vector(7 downto 0);
			prdata2       : out std_logic_vector(15 downto 0);
			prdata2_valid : out std_logic;
			prdata        : out std_logic_vector(7 downto 0);
			prdata4       : out std_logic_vector(31 downto 0)
		);
	end component;

	component flash_wrapper is
		port(
			clk           : in  std_logic;
			rst           : in  std_logic;
			--low level signals
			pso           : in  std_logic;
			pAT_flash_out : out AT_flash_outputs;
			--high(er) level signals
			pinputs       : in  flash_inputs;
			poutputs      : out flash_outputs
		);
	end component;

	component FlashConfig is
		port(
			clk               : in  std_logic;
			rst               : in  std_logic;
			--command of the flash config entity, drives the main fsm                    
			cmd               : in  std_logic_vector(3 downto 0);
			flash_block       : in  std_logic_vector(3 downto 0);
			flash_data_w      : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			flash_data_r      : out std_logic_vector(REGISTER_SIZE - 1 downto 0);
			--write enable for com reg (recall from flash)                               
			reg_WEN           : out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
			--multplexer signal to divert data to control comRegIn by the flash          
			mux_flash         : out std_logic;
			--com reg address currently read or written                                  
			reg_adr           : out std_logic_vector(REGISTER_ADR_WIDTH - 1 downto 0);
			--low level flash memory signals routed to the top level ports               
			Flash_AT25_SO     : in  std_logic;
			Flash_AT25_inputs : out AT_flash_outputs;
			debug             : out std_logic_vector(7 downto 0)
		);
	end component;

	component decimating_buffer is
		generic(nb_channels          : integer range 1 to 32      := 7;
		        address_width        : integer range 2 to 22      := 17;
		        --decimation 				: integer range 0 to 31 := 4; --number of samples to be added up (must be 2^n)
		        decimation_bits      : integer range 0 to 64       := 2; --number of bits (n) the sum vector needs to be xtended
		        words_max            : integer range 3 to 1048576 := 1048574; --number of words saved in memory
		        post_trigger_samples : integer range 3 to 1048576 := 524228); --number of samples recorded after trigger
		port(
			clk, rst       : in  std_logic;
			--cypress ram interface signals
			cy_adr_in      : out std_logic_vector(address_width - 1 downto 0);
			cy_rdata       : in  std_logic_vector(adc_data_width - 1 downto 0);
			cy_wdata       : out std_logic_vector(adc_data_width - 1 downto 0);
			cy_r_done      : in  std_logic;
			cy_w_done      : in  std_logic;
			cy_cmd         : out std_logic_vector(1 downto 0);
			--Input data
			input_array    : in  channelsArray(nb_channels - 1 downto 0);
			logic_channels : in  std_logic_vector(nb_channels - 1 downto 0);
			--Command pulses 						Sensitive state
			dready         : in  std_logic; --IDLE
			buffer_trigger : in  std_logic; --IDLE

			--read_pulse_all		:in std_logic;		--READY_TO_READ
			--read_pulse_dec		:in std_logic;		--READY_TO_READ

			--rearm_pulse 		:in std_logic;		--READT_TO_READ
			cmd_vector     : in  std_logic_vector(7 downto 0);
			output_array   : out channelsArray(nb_channels - 1 downto 0);
			--output_ready 		:out std_logic;
			status         : out std_logic_vector(7 downto 0);
			data_r_debug   : out std_logic_vector(adc_data_width - 1 downto 0);
			data_w_debug   : out std_logic_vector(adc_data_width - 1 downto 0);
			debug_vector   : out std_logic_vector(7 downto 0)
		);
	end component;

	component spi_slave is
		generic(trans_length : natural := 8);
		port(clk, rst       : in  std_logic;
		     --spi hw lines
		     spi_csn        : in  std_logic;
		     spi_sclk       : in  std_logic;
		     spi_sdin       : in  std_logic;
		     spi_sdout      : out std_logic;
		     --internal signals
		     rdata          : out std_logic_vector(7 downto 0);
		     wdata          : in  std_logic_vector(7 downto 0);
		     -- spi_active		:out std_logic;
		     -- rx_valid		:out std_logic;
		     -- tx_valid		:out std_logic;
		     rx_valid_pulse : out std_logic;
		     tx_valid_pulse : out std_logic
		     --tx_req			:out std_logic
		    );
	end component;

	component cy_sram_interface is
		generic(
			dw  : natural := 16;        -- data width
			aw  : natural := 17;        -- address width
			dbg : boolean := false
		);
		port(
			clk, rst : in    std_logic;
			--HW signals of sram
			ram_oe   : out   std_logic;
			ram_bhe  : out   std_logic;
			ram_ble  : out   std_logic;
			ram_ce   : out   std_logic;
			ram_we   : out   std_logic;
			ram_adr  : out   std_logic_vector(aw - 1 downto 0);
			ram_io   : inout std_logic_vector(dw - 1 downto 0);
			--data/controls
			adr_in   : in    std_logic_vector(aw - 1 downto 0);
			rdata    : out   std_logic_vector(dw - 1 downto 0);
			wdata    : in    std_logic_vector(dw - 1 downto 0);
			r_done   : out   std_logic;
			w_done   : out   std_logic;
			cmd      : in    std_logic_vector(1 downto 0);
			debug    : out   std_logic_vector(3 downto 0)
		);
	end component;
	
	component adc_spi is
    generic(trans_length : natural := 8);
    port (clk, rst 		:in std_logic;
        --spi hw lines
        spi_csn			:in std_logic;
        spi_sclk		:in std_logic;
        spi_sdin		:in std_logic;
        spi_sdout		:out std_logic;
        --internal signals
        rdata			:out std_logic_vector(7 downto 0);
        wdata			:in std_logic_vector(7 downto 0);
        -- spi_active		:out std_logic;
        -- rx_valid		:out std_logic;
        -- tx_valid		:out std_logic;
        rx_valid_pulse	:out std_logic;
        tx_valid_pulse	:out std_logic
        --tx_req			:out std_logic
    );
    end component;
	
	component ups_mon is 
	generic (self_corr : boolean := true);
	port(
		clk, rst 			:in std_logic;
		--spi hw lines
		csn			:in std_logic;
		sclk		:in std_logic;
		sdin		:in std_logic;
		sdout		:out std_logic;
		threshold_5V	:in unsigned(15 downto 0);
		threshold_15V	:in unsigned(15 downto 0);
		data_ready		:out std_logic;
		data_5V			:out std_logic_vector(15 downto 0);
		data_15V        :out std_logic_vector(15 downto 0);
		status_out		:out std_logic_vector(3 downto 0)
	);
	end component;

	component channel_generic is
		generic(
			generate_median	   	: boolean := TRUE;
			d_width        : integer range 3 to 32   := 16;
			b_width        : integer range 3 to 32   := 16;
			--		median_depth   : integer range 2 to 126  := 2;
			--		median_middle  : integer range 1 to 62   := 1;
			mavg_ram_depth : integer range 3 to 1024 := 1024;
			cor_c_width    : integer range 8 to 24   := 16
		);
		port(clk             : in  std_logic;
		     rst             : in  std_logic;
		     sampleclock     : in  std_logic;
		     Data_in         : in  std_logic_vector(d_width - 1 downto 0);
		     autozero_start  : in  std_logic;
		     --Multiplexer
		     mux_data        : in  std_logic_vector(7 downto 0);
		     mux_daq         : in  std_logic_vector(1 downto 0);
		     --functional block parameters
		     mavg_depth_bits : in  integer range 0 to 12;
		     real_div : in std_logic_vector(d_width - 1 downto 0);
	    	 real_invdiv : in std_logic_vector(d_width - 1 downto 0);
		     median_depth    : in  integer range 2 to 126 := 2;
		     median_middle   : in  integer range 1 to 62  := 1;
		     cor_gain        : in  std_logic_vector(cor_c_width - 1 downto 0);
		     cor_offset      : in  std_logic_vector(d_width - 1 downto 0);
		     --outputs
		     cor_offset_auto : out std_logic_vector(d_width - 1 downto 0);
		     Dmux_data_out   : out std_logic_vector(d_width - 1 downto 0);
	      	 Bmux_data_out   : out std_logic_vector(b_width - 1 downto 0);
	      	 d_ready_out     : out std_logic;
		     buf_ready_out	 : out std_logic;
		     debug           : out std_logic_vector(1 downto 0));
	end component;

	component multS is
		generic(W1, W2 : natural; omode : std_logic);
		port(clk, rst, start : in  std_logic;
		     multiplier      : in  std_logic_vector(W1 - 1 downto 0);
		     multiplicand    : in  std_logic_vector(W2 - 1 downto 0);
		     done            : out std_logic;
		     product         : out std_logic_vector(W1 + W2 - 2 downto 0));
	end component;

	component scaler is
		generic(d_width_in     : integer range 15 to 32 := 20;
				d_width_out    : integer range 15 to 32 := 32;
				d_width_factor : integer range 2 to 32  := 21);
		port(clk, rst, str     : in  std_logic;
		     gainsel           : in  std_logic_vector(3 downto 0);
		     transducer_factor : in  std_logic_vector(3 downto 0);
		     resistor_divider  : in  std_logic_vector(0 downto 0);
		     scaler_value      : in  std_logic_vector(d_width_factor - 2 downto 0);
		     direct_mode       : in  std_logic;
		     din               : in  std_logic_vector(d_width_in - 1 downto 0);
		     dout              : out std_logic_vector(d_width_out - 1 downto 0);
		      input_overrange   : out std_logic;
	    	 output_overrange  : out std_logic;
		     ready             : out std_logic
		    );
	end component;

	component fastbuffer is
		generic(d_width : integer range 3 to 32 := 16);
		port(
			clk         : in  std_logic;
			rst         : in  std_logic;
			sampleclock : in  std_logic;
			rclock      : in  std_logic;
			Data_in     : in  std_logic_vector(d_width - 1 downto 0);
			spulse      : in  std_logic;
			restart     : in  std_logic;
			skipsamples : in  std_logic_vector(4 downto 0);
			Data_out    : out std_logic_vector(d_width - 1 downto 0);
			debug       : out std_logic_vector(1 downto 0));
	end component;

	component channel_controller is
		port(
			clk      : in  std_logic;
			slowclock : in std_logic;
			reset    : in  std_logic;
			data_in  : in  std_logic_vector(7 downto 0);
			enable   : in  std_logic;
			data_out : out std_logic;
			clk_out  : out std_logic;
			strobe   : out std_logic
		);
	end component;

	component USB20_245FIFO_Controller
		generic(PACKET_SIZE : natural);
		port(
			USBnRXF    : in  std_logic;
			USBnTXE    : in  std_logic;
			USBCLK     : in  std_logic;
			USBDataBus : out std_logic_vector(7 downto 0);
			USBnRD     : out std_logic;
			USBnWR     : out std_logic;
			USBnOE     : out std_logic;
			FIFOData   : in  data8Array(PACKET_SIZE - 9 downto 0);
			FIFOWR     : in  std_logic;
			FIFOFull   : out std_logic;
			clk        : in  std_logic;
			nRST       : in  std_logic
		);
	end component USB20_245FIFO_Controller;

	component circular_buffer IS
		generic(
			memory_size : integer := 256
		);
		port(
			-- RS485
			rst      : in  std_logic;
			data_in  : in  std_logic_vector(7 downto 0);
			wclock   : in  std_logic;
			we       : in  std_logic;
			data_out : out std_logic_vector(7 downto 0);
			rclock   : in  std_logic;
			re       : in  std_logic;
			aempty   : out std_logic;
			afull    : out std_logic
		);
	end component;

	component FIFOUSB is
		port(
			-- Inputs
			DATA   : in  std_logic_vector(7 downto 0);
			RCLOCK : in  std_logic;
			RE     : in  std_logic;
			RESET  : in  std_logic;
			WCLOCK : in  std_logic;
			WE     : in  std_logic;
			-- Outputs
			AEMPTY : out std_logic;
			AFULL  : out std_logic;
			EMPTY  : out std_logic;
			FULL   : out std_logic;
			Q      : out std_logic_vector(7 downto 0)
		);
	end component;

	component detection_block_mag_twolead is
		generic(d_width : integer range 15 to 33 := 32);
		port(
			clk         : in  std_logic;
			rst         : in  std_logic;
			magA        : in  std_logic_vector(d_width - 1 downto 0);
			magB        : in  std_logic_vector(d_width - 1 downto 0);
			leadA       : in  std_logic_vector(d_width - 1 downto 0);
			leadB       : in  std_logic_vector(d_width - 1 downto 0);
			thres_mag   : in  std_logic_vector(d_width - 1 downto 0);
			disc_mag    : in  std_logic_vector(15 downto 0);
			thres_leads : in  std_logic_vector(d_width - 1 downto 0);
			disc_leads  : in  std_logic_vector(15 downto 0);
			magABsum    : out std_logic_vector(d_width - 1 downto 0);
			logic_out   : out std_logic_vector(7 downto 0)
		);
	end component;

	component mul_L_didt is
		generic(d_width   : integer range 15 to 32 := 32;
		        d_width_L : integer range 7 to 32  := 16
		       );
		port(
			clk, rst   : in  std_logic;
			didt       : in  std_logic_vector(d_width - 1 downto 0); --s(15,16)
			L          : in  std_logic_vector(d_width_L - 1 downto 0); --u(2,14)
			start_mult : in  std_logic;
			dout       : out std_logic_vector(d_width - 1 downto 0) --s(7,24)
		);
	end component;
	--component detection_block_mag_simetric is
	--	generic(d_width : integer range 15 to 33 := 32);
	--	port(
	--		clk : in std_logic;
	--		rst : in std_logic;
	--		
	--		magA 			: in std_logic_vector(d_width-1 downto 0);
	--		magB 			: in std_logic_vector(d_width-1 downto 0);
	--		leadA 			: in std_logic_vector(d_width-1 downto 0);
	--		leadB 			: in std_logic_vector(d_width-1 downto 0);
	--		thres_mag		: in std_logic_vector(d_width-1 downto 0);
	--		disc_mag		: in std_logic_vector(15 downto 0);
	--		thres_leads		: in std_logic_vector(d_width-1 downto 0);
	--		disc_leads		: in std_logic_vector(15 downto 0);
	--		magABsum		: out std_logic_vector(d_width-1 downto 0);
	--		logic_out		: out std_logic_vector(7 downto 0)
	--		
	--	);
	--end component;

	component detection_block_comp_L_didt is
		generic(d_width                  : integer range 15 to 32 := 32;
		        d_width_L                : integer range 7 to 32  := 16;
		        differentiator_arr_depth : integer range 7 to 65536
		       );
		port(
			clk              : in  std_logic;
			rst              : in  std_logic;
			dready           : in  std_logic;
			chgenUind_params : in  channel_generic_settings;
			chgenUres_params : in  channel_generic_settings;
			diff_dt          : in  std_logic_vector(d_width_L downto 0);
			diff_nms         : in  std_logic_vector(7 downto 0);
			Udiff            : in  std_logic_vector(d_width - 1 downto 0); --voltage across magnet
			I_mag            : in  std_logic_vector(d_width - 1 downto 0);
			Inductance       : in  std_logic_vector(d_width_L - 1 downto 0);
			threshold        : in  std_logic_vector(d_width - 1 downto 0);
			discrimination   : in  std_logic_vector(15 downto 0);
			Uind             : out std_logic_vector(d_width - 1 downto 0);
			Uind_diag        : out std_logic_vector(d_width - 1 downto 0);
			Ures             : out std_logic_vector(d_width - 1 downto 0);
			Ures_diag        : out std_logic_vector(d_width - 1 downto 0);
			Logic_out        : out std_logic_vector(3 downto 0)
		);
	end component;

	component detection_block_L_didt_sensor is
		generic(d_width   : integer range 15 to 32 := 32;
		        d_width_L : integer range 7 to 32  := 16
		        --differentiator_arr_depth : integer range 7 to 65536 
		       );
		port(
			clk              : in  std_logic;
			rst              : in  std_logic;
			dready           : in  std_logic;
			--chgenUind_params: in channel_generic_settings;
			chgenUres_params : in  channel_generic_settings;
			--diff_dt			: in std_logic_vector(d_width_L downto 0);
			--diff_nms		: in std_logic_vector(7 downto 0);
			Udiff            : in  std_logic_vector(d_width - 1 downto 0); --voltage across magnet
			didt             : in  std_logic_vector(d_width - 1 downto 0);
			Inductance       : in  std_logic_vector(d_width_L - 1 downto 0);
			threshold        : in  std_logic_vector(d_width - 1 downto 0);
			discrimination   : in  std_logic_vector(15 downto 0);
			--Uind			: out std_logic_vector(d_width -1 downto 0);
			--Uind_diag		: out std_logic_vector(d_width -1 downto 0);
			Ures             : out std_logic_vector(d_width - 1 downto 0);
			Ures_diag        : out std_logic_vector(d_width - 1 downto 0);
			Logic_out        : out std_logic_vector(3 downto 0)
		);
	end component;

	component differentiator_synram is
		generic(arr_depth : integer range 7 to 65536;
		        d_width   : integer range 15 to 32);
		port(
			clk, rst           : in  std_logic;
			din                : in  std_logic_vector(d_width - 1 downto 0); --input current s(15,16)
			dready             : in  std_logic;
			dt                 : in  std_logic_vector((log2c(arr_depth)) downto 0); --# of samples forming the dt window
			normalizing_shifts : in  std_logic_vector(7 downto 0); --# of bits to shift for normalization
			dout               : out std_logic_vector(d_width - 1 downto 0); --s(15,16) (keep the same as current)
			done               : out std_logic
		);
	end component;

	component qlogic_comp_simple is
		generic(d_width : integer range 15 to 33 := 32);
		port(
			clk   : in  std_logic;
			rst   : in  std_logic;
			U_in  : in  std_logic_vector(d_width - 1 downto 0);
			T_in  : in  std_logic_vector(d_width - 1 downto 0);
			disc  : in  std_logic_vector(15 downto 0);
			L_out : out std_logic_vector(3 downto 0)
		);
	end component;
	
	component inverted_qlogic_comp_simple is
	generic(d_width : integer range 15 to 33 := 32);
	port (
		clk : 		in std_logic;
		rst : 		in std_logic;
		U_in : 		in std_logic_vector(d_width-1 downto 0);
		T_in : 		in std_logic_vector(d_width -1 downto 0);
		disc	: 	in std_logic_vector(15 downto 0);
		L_out : 	out std_logic_vector(3 downto 0)
	);
	end component;

	component qlogic_bridge is
		generic(d_width : natural := 32);
		port(
			clk        : in  std_logic;
			rst        : in  std_logic;
			qthreshold : in  std_logic_vector(d_width - 1 downto 0);
			time_disc  : in  std_logic_vector(15 downto 0);
			inA        : in  std_logic_vector(d_width - 1 downto 0);
			inB        : in  std_logic_vector(d_width - 1 downto 0);
			outAplusB  : out std_logic_vector(d_width - 1 downto 0);
			log_out    : out std_logic_vector(3 downto 0)
		);
	end component;
	
	component qlogic_bridge_flex is
		generic(	d_width : natural := 32;		--data width
			subtract: boolean := FALSE;				--subtract inputs or add 
			num_comps: natural range 1 to 2 := 1	--number of comparators instantiated (max 2)
			);
		port(
		clk 		: in std_logic;
		rst 		: in std_logic;
		qthreshold	: in channelsArray(num_comps-1 downto 0);
		time_disc	: in channelsArray(num_comps-1 downto 0);
		inA			: in std_logic_vector(d_width-1 downto 0);
		inB			: in std_logic_vector(d_width-1 downto 0);
		outAB 		: out std_logic_vector(d_width-1 downto 0);
		log_out 	: out std_logic_vector(3 downto 0)
	);
	end component;
	

	component dynamic_threshold is
	generic(d_width      : integer range 15 to 32 := 32;
	        table_length : integer range 1 to 8   := 4
	       );
	port(
		clk, rst             : in  std_logic;
		threshold_tab        : in  regArrayCurDepSettings;
		discriminator_tab    : in  regArrayCurDepSettings;
		current_level_tab    : in  regArrayCurDepSettings;
		current              : in  std_logic_vector(d_width - 1 downto 0);
		force_last_threshold : in  std_logic;
		threshold            : out std_logic_vector(31 downto 0);
		discriminator        : out std_logic_vector(31 downto 0)
	);
    end component;

	component channel_malfunction_alerter is
		generic(d_width     : integer range 15 to 32 := 32;
		        max_samples : integer range 8 to 255 := 16
		       );
		port(
			clk, rst    : in  std_logic;
			data_in     : in  std_logic_vector(d_width - 1 downto 0);
			data_rdy    : in  std_logic;
			malfunction : out std_logic
		);
	end component;

	component sram_tester is
		generic(
			addr_width : natural := 20;
			data_width : natural := 16
		);
		port(
			clk       : in  std_logic;
			rst       : in  std_logic;
			start     : in  std_logic;
			flags     : out std_logic_vector(2 downto 0);
			cy_adr_in : out std_logic_vector(addr_width - 1 downto 0);
			cy_rdata  : in  std_logic_vector(data_width - 1 downto 0);
			cy_wdata  : out std_logic_vector(data_width - 1 downto 0);
			cy_r_done : in  std_logic;
			cy_w_done : in  std_logic;
			cy_cmd    : out std_logic_vector(1 downto 0)
		);
	end component;

	component config_block is
		port(
			CLK                      : in  std_logic;
			rst                      : in  std_logic;
			--Data from ADC, scaled & dready pulses
			ChannelsScaledData       : in  channelsArray(CHANNELS_IN_MAX - 1 downto 0);
			ChannelsScaledDataRdy    : in  std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
			
			--Flags gnerated by scaler to indicate that signal left linear range of UQDS channel
			--Flags to show if ADC gives constant values
			ADC_Health			 : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			ADC_Health_Mask		 : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			ADC_overRange			: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			ADC_overRange_Mask		: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			--Registers configuring this block (127 registers in total)		
			ConfigBlockReg           : in  customRegArray;
			
			--Signals from top level command interpreter to initiate auto offset correction
			AutozeroStart            : in  std_logic_vector(31 downto 0);
			
			--Configuration of Readout (mux settings)
			ReadoutConfig            : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			
			AdcStartConv             : in  std_logic;
			
			--Mask to select Post mortem trigger from local trigger sources
			PMBufferMask             : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
			
			--Sync signals from top level (actual action is defined on top)
			SyncSignals              : in  std_logic_vector(5 downto 0);
			
			--Data to PM buffer & associated dready signals
			BufferMUXData            : out outputChannelsArray;
			BufferMUXDataRdy         : out std_logic;
			
			--Mask to mark non-decimable PM buffer channels (non-analogue values)
			BufferNoDecimateChannels : out std_logic_vector(CHANNELS_OUT_MAX - 1 downto 0);
			
			--Data to normal register space
			RegMUXData               : out regChannelsArray;
			
			--Data to Fast Readout module & associated dready
			FReadoutMUXData          : out outputChannelsArray;
			FReadoutMUXDataRdy       : out std_logic;
			
			--Trigger lines generated locally, mapped on TOP to pysical lines
			TriggerLines             : out std_logic_vector(TRIGGER_LINES_MAX - 1 downto 0);
			
			--PM trigger signal generated locally
			TriggerPM                : out std_logic;
			
			--ID of this config block
			ConfigBlockNum           : out std_logic_vector(REGISTER_SIZE - 1 downto 0)
		);
	end component;
	
	component discriminator is
		generic (freq_factor : unsigned(14 downto 0);
			disc_time_width : natural:=16);
		port (clk, rst, enable, lsig : in std_logic;
			maxval : in unsigned(disc_time_width-1 downto 0);
			gate : out std_logic);
	end component;
	
	component decimateX is
		generic(	d_width : integer range 15 to 32 := 24;
			X : natural := 4; 
			omode: boolean := false
		); 
		port (clk, reset, dready	: in std_logic;
			data_in		: in std_logic_vector(d_width -1 downto 0);
			slow_dready	: out std_logic;
			slow_data	: out std_logic_vector(d_width -1 downto 0)
		);
	end component;
	
	component median_filter is
		generic(
			d_width : integer range 15 to 32 := 24;
			max_depth   : integer range 2 to 126 := 6
			--middle  : integer range 1 to 62  := 3
		);
		port(
			clk          : in  std_logic;
			rst          : in  std_logic;
			sample_clock : in  std_logic;
			val_in       : in  std_logic_vector(d_width-1 downto 0);
			out_valid    : out std_logic;
			median_out   : out std_logic_vector(d_width-1 downto 0);
			depth : in integer range 2 to 126 := 6;
			middle : in integer range 1 to 62  := 3
		);
	end component;
	
	component meanram_fsm is
		generic(d_width : integer range 15 to 32 := 16;
				ram_depth : integer range 0 to 1024 := 128;
				--init_ram_load_counter : natural := 4000000;
				--type fxp_signaltype; --(voltage,current, didt ) (use
				realmul : boolean := false
				);
		port (
			clk			:in std_logic;
			sampleclock :in std_logic;
			rst		 	:in std_logic;
			depth_bits  :in integer range 0 to 12;
			Data_in		:in std_logic_vector(d_width -1 downto 0);
			real_div	:in std_logic_vector(d_width -1 downto 0);
			real_invdiv	:in std_logic_vector(d_width -1 downto 0);
			Qout		:out std_logic_vector(d_width -1 downto 0);
			dready_out : out std_logic
			);
	end component;
	
	component meanram_fsm_fast is
	generic(d_width : integer range 15 to 32 := 16;
			ram_depth : integer range 0 to 1024 := 128;
			init_ram_load_counter : natural := 4000000;
			--type fxp_signaltype; --(voltage,current, didt ) (use
			real_mul : boolean := false
			);
	port (
		clk			:in std_logic;
		sampleclock :in std_logic;
		rst		 	:in std_logic;
		depth_bits  :in integer range 0 to 12;
		Data_in		:in std_logic_vector(d_width -1 downto 0);
		real_div	:in std_logic_vector(d_width -1 downto 0);
		real_invdiv	:in std_logic_vector(d_width -1 downto 0);
		Qout		:out std_logic_vector(d_width -1 downto 0);
		dready_out : out std_logic
		);
	end component;

	
	component corr_fixed is 
		generic(d_width : integer range 15 to 32 := 20;
				o_width : integer range 15 to 32 := 16;
				c_width : integer range 4 to 24 := 24;
				outreg : boolean := TRUE);

		port (clk, rst : in std_logic;
			din	 : in std_logic_vector(d_width -1 downto 0);
			cfactor	: in std_logic_vector(c_width -1 downto 0);
			offset	:in std_logic_vector(o_width -1 downto 0);
			dout : out std_logic_vector(d_width -1 downto 0) );
	end component;
	
	component autozero is
		generic(d_width : integer range 15 to 32);
		port(
			clk    : in  std_logic;
			rst    : in  std_logic;
			start  : in  std_logic;
			done   : out std_logic;
			din    : in  std_logic_vector(d_width - 1 downto 0);
			dready : in  std_logic;
			offcor : out std_logic_vector(d_width - 1 downto 0)
		);
	end component;
	
	function conv_chgen_params(regA, regB, regC, regD : std_logic_vector(REGISTER_SIZE -1 downto 0)) return channel_generic_settings;
end UQDSLib;

package body UQDSLib is
	--assign up to four registers to generic data processing channel
	function conv_chgen_params(regA, regB, regC, regD : std_logic_vector(REGISTER_SIZE -1 downto 0)) return channel_generic_settings is
		variable rec_settings : channel_generic_settings;
	begin
		rec_settings.mux_data               := regA(15 downto 8);
		rec_settings.mux_daq                := regA(7 downto 6);
		rec_settings.flt_mov_avg_depth_bits := to_integer(unsigned(regB(7 downto 0)));
		rec_settings.flt_median_depth       := to_integer(unsigned(regB(15 downto 8)));
		rec_settings.flt_median_middle      := to_integer(unsigned('0' & regB(15 downto 9)));
		rec_settings.cor_gain               := regB(31 downto 16);
		rec_settings.cor_offset             := regC;

		return rec_settings;
	end conv_chgen_params;

end UQDSLib;