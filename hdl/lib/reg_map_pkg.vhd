library IEEE;
use IEEE.STD_LOGIC_1164.all;

library UQDSLib;
use UQDSLib.UQDSLib.all;
use UQDSLib.buildstamp.all;

package reg_map_pkg is
	--inputs
	signal comRegDataIn_MUX		: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal comRegOut			: regArray;
	--outputs
	signal comRegIn, comRegRD, comRegMk	: regArray;
	signal comRegRO						: std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
	signal reg_CRC	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal config_block_num	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal board_id	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal trigger_status	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal system_status	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal readout_config	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal pm_buffer_mask	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal GlobalHeartBeatReg	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal adc_health	:std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal adc_health_mask	:std_logic_vector(REGISTER_SIZE- 1 downto 0);
	signal scaler_overrange	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal scaler_overrange_mask	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal ChannelConfig	:regChannelsArray;
	signal ps5_volt	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal ps15_volt	: std_logic_vector(REGISTER_SIZE - 1 downto 0);
	signal ChannelData	:regChannelsArray;
	signal ChannelBuffer	:regChannelsArray;
	signal ConfigCustomRegister	:customRegArray;

	signal ChannelGenericA	:regChannelsArray;
	signal ChannelGenericB	:regChannelsArray;
	signal ChannelGenericC	:regChannelsArray;
	signal ChannelGenericD	:regChannelsArray;
	signal CurrLevelTable	:regArrayCurDepSettings;
	signal DynThreshTableComp0	:regArrayCurDepSettings;
	signal DynDiscrTableComp0	:regArrayCurDepSettings;
	signal DynThreshTableComp1	:regArrayCurDepSettings;
	signal DynDiscrTableComp1	:regArrayCurDepSettings;

	procedure RegisterMap (
		signal comRegDataIn_MUX	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal comRegOut	: in regArray;
		signal comRegIn, comRegRD, comRegMk	: out regArray;
		signal comRegRO	: out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
		signal reg_CRC	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal config_block_num	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal board_id	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal trigger_status	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal system_status	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal readout_config	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal pm_buffer_mask	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal GlobalHeartBeatReg	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal adc_health	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal adc_health_mask	: out std_logic_vector(REGISTER_SIZE- 1 downto 0);
		signal scaler_overrange	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal scaler_overrange_mask	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal ChannelConfig	: out regChannelsArray;
		signal ps5_volt	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal ps15_volt	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal ChannelData	: in regChannelsArray;
		signal ChannelBuffer	: in regChannelsArray;
		signal ConfigCustomRegister	: out customRegArray
	);

	procedure RegisterMapConfigBlock (
		signal ConfigCustomRegister	: in customRegArray;
		signal ChannelGenericA	:out regChannelsArray;
		signal ChannelGenericB	:out regChannelsArray;
		signal ChannelGenericC	:out regChannelsArray;
		signal ChannelGenericD	:out regChannelsArray;
		signal CurrLevelTable	:out regArrayCurDepSettings;
		signal DynThreshTableComp0	:out regArrayCurDepSettings;
		signal DynDiscrTableComp0	:out regArrayCurDepSettings;
		signal DynThreshTableComp1	:out regArrayCurDepSettings;
		signal DynDiscrTableComp1	:out regArrayCurDepSettings
	);

	end reg_map_pkg;

package body reg_map_pkg is

	procedure RegisterMap (
		signal comRegDataIn_MUX   : in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal comRegOut	: in regArray;
		signal comRegIn, comRegRD, comRegMk	: out regArray;
		signal comRegRO : out std_logic_vector(2**REGISTER_ADR_WIDTH - 1 downto 0);
		signal reg_CRC	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal config_block_num	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal board_id	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal trigger_status	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal system_status	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal readout_config	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal pm_buffer_mask	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal GlobalHeartBeatReg	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal adc_health	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal adc_health_mask	: out std_logic_vector(REGISTER_SIZE- 1 downto 0);
		signal scaler_overrange	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal scaler_overrange_mask	: out std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal ChannelConfig	: out regChannelsArray;
		signal ps5_volt	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal ps15_volt	: in std_logic_vector(REGISTER_SIZE - 1 downto 0);
		signal ChannelData	: in regChannelsArray;
		signal ChannelBuffer	: in regChannelsArray;
		signal ConfigCustomRegister	: out customRegArray
	) is 
 	begin 
		comRegRO(0) <= '1';
		comRegIn(0) <= build_rev;
		comRegRD(0) <= x"00000000";
		comRegMk(0) <= x"FFFFFFFF";

		comRegRO(1) <= '1';
		comRegIn(1) <= reg_CRC;
		comRegRD(1) <= x"00000000";
		comRegMk(1) <= x"FFFFFFFF";

		comRegRO(2) <= '1';
		comRegIn(2) <= config_block_num;
		comRegRD(2) <= x"00000000";
		comRegMk(2) <= x"FFFFFFFF";

		comRegRO(3) <= '1';
		comRegIn(3) <= board_id;
		comRegRD(3) <= x"0000AFFE";
		comRegMk(3) <= x"FFFFFFFF";

		comRegRO(5) <= '1';
		comRegIn(5) <= trigger_status;
		comRegRD(5) <= x"00000000";
		comRegMk(5) <= x"FFFFFFFF";

		comRegRO(6) <= '1';
		comRegIn(6) <= system_status;
		comRegRD(6) <= x"00000000";
		comRegMk(6) <= x"FFFFFFFF";

		comRegRO(10) <= '0';
		comRegIn(10) <= comRegDataIn_MUX;
		comRegRD(10) <= x"6C000000";
		comRegMk(10) <= x"FFFFFFFF";
		readout_config <= comRegOut(10);

		comRegRO(11) <= '0';
		comRegIn(11) <= comRegDataIn_MUX;
		comRegRD(11) <= x"00010555";
		comRegMk(11) <= x"FFFFFFFF";
		pm_buffer_mask <= comRegOut(11);

		comRegRO(12) <= '0';
		comRegIn(12) <= comRegDataIn_MUX;
		comRegRD(12) <= x"000000C3";
		comRegMk(12) <= x"FFFFFFFF";
		GlobalHeartBeatReg <= comRegOut(12);

		comRegRO(13) <= '1';
		comRegIn(13) <= adc_health;
		comRegRD(13) <= x"00000000";
		comRegMk(13) <= x"FFFFFFFF";

		comRegRO(14) <= '0';
		comRegIn(14) <= comRegDataIn_MUX;
		comRegRD(14) <= x"FFFFFFFF";
		comRegMk(14) <= x"FFFFFFFF";
		adc_health_mask <= comRegOut(14);

		comRegRO(15) <= '1';
		comRegIn(15) <= scaler_overrange;
		comRegRD(15) <= x"00000000";
		comRegMk(15) <= x"FFFFFFFF";

		comRegRO(16) <= '0';
		comRegIn(16) <= comRegDataIn_MUX;
		comRegRD(16) <= x"FFFFFFFF";
		comRegMk(16) <= x"FFFFFFFF";
		scaler_overrange_mask <= comRegOut(16);

		comRegRO(19) <= '0';
		comRegIn(19) <= comRegDataIn_MUX;
		comRegRD(19) <= x"00000000";
		comRegMk(19) <= x"FFFFFFFF";
		ChannelConfig(0) <= comRegOut(19);

		comRegRO(20) <= '0';
		comRegIn(20) <= comRegDataIn_MUX;
		comRegRD(20) <= x"00000000";
		comRegMk(20) <= x"FFFFFFFF";
		ChannelConfig(1) <= comRegOut(20);

		comRegRO(21) <= '0';
		comRegIn(21) <= comRegDataIn_MUX;
		comRegRD(21) <= x"00000000";
		comRegMk(21) <= x"FFFFFFFF";
		ChannelConfig(2) <= comRegOut(21);

		comRegRO(22) <= '0';
		comRegIn(22) <= comRegDataIn_MUX;
		comRegRD(22) <= x"00000000";
		comRegMk(22) <= x"FFFFFFFF";
		ChannelConfig(3) <= comRegOut(22);

		comRegRO(23) <= '0';
		comRegIn(23) <= comRegDataIn_MUX;
		comRegRD(23) <= x"00000004";
		comRegMk(23) <= x"FFFFFFFF";
		ChannelConfig(4) <= comRegOut(23);

		comRegRO(24) <= '0';
		comRegIn(24) <= comRegDataIn_MUX;
		comRegRD(24) <= x"00000000";
		comRegMk(24) <= x"FFFFFFFF";
		ChannelConfig(5) <= comRegOut(24);

		comRegRO(25) <= '0';
		comRegIn(25) <= comRegDataIn_MUX;
		comRegRD(25) <= x"00000000";
		comRegMk(25) <= x"FFFFFFFF";
		ChannelConfig(6) <= comRegOut(25);

		comRegRO(26) <= '0';
		comRegIn(26) <= comRegDataIn_MUX;
		comRegRD(26) <= x"00000000";
		comRegMk(26) <= x"FFFFFFFF";
		ChannelConfig(7) <= comRegOut(26);

		comRegRO(27) <= '0';
		comRegIn(27) <= comRegDataIn_MUX;
		comRegRD(27) <= x"00000000";
		comRegMk(27) <= x"FFFFFFFF";
		ChannelConfig(8) <= comRegOut(27);

		comRegRO(28) <= '0';
		comRegIn(28) <= comRegDataIn_MUX;
		comRegRD(28) <= x"00000000";
		comRegMk(28) <= x"FFFFFFFF";
		ChannelConfig(9) <= comRegOut(28);

		comRegRO(29) <= '0';
		comRegIn(29) <= comRegDataIn_MUX;
		comRegRD(29) <= x"00000000";
		comRegMk(29) <= x"FFFFFFFF";
		ChannelConfig(10) <= comRegOut(29);

		comRegRO(30) <= '0';
		comRegIn(30) <= comRegDataIn_MUX;
		comRegRD(30) <= x"00000000";
		comRegMk(30) <= x"FFFFFFFF";
		ChannelConfig(11) <= comRegOut(30);

		comRegRO(31) <= '0';
		comRegIn(31) <= comRegDataIn_MUX;
		comRegRD(31) <= x"00000000";
		comRegMk(31) <= x"FFFFFFFF";
		ChannelConfig(12) <= comRegOut(31);

		comRegRO(32) <= '0';
		comRegIn(32) <= comRegDataIn_MUX;
		comRegRD(32) <= x"00000000";
		comRegMk(32) <= x"FFFFFFFF";
		ChannelConfig(13) <= comRegOut(32);

		comRegRO(33) <= '0';
		comRegIn(33) <= comRegDataIn_MUX;
		comRegRD(33) <= x"00000000";
		comRegMk(33) <= x"FFFFFFFF";
		ChannelConfig(14) <= comRegOut(33);

		comRegRO(34) <= '0';
		comRegIn(34) <= comRegDataIn_MUX;
		comRegRD(34) <= x"00000000";
		comRegMk(34) <= x"FFFFFFFF";
		ChannelConfig(15) <= comRegOut(34);

		comRegRO(51) <= '1';
		comRegIn(51) <= ps5_volt;
		comRegRD(51) <= x"00000000";
		comRegMk(51) <= x"FFFFFFFF";

		comRegRO(52) <= '1';
		comRegIn(52) <= ps15_volt;
		comRegRD(52) <= x"00000000";
		comRegMk(52) <= x"FFFFFFFF";

		comRegRO(64) <= '1';
		comRegIn(64) <= ChannelData(0);
		comRegRD(64) <= x"00000000";
		comRegMk(64) <= x"FFFFFFFF";

		comRegRO(65) <= '1';
		comRegIn(65) <= ChannelData(1);
		comRegRD(65) <= x"00000000";
		comRegMk(65) <= x"FFFFFFFF";

		comRegRO(66) <= '1';
		comRegIn(66) <= ChannelData(2);
		comRegRD(66) <= x"00000000";
		comRegMk(66) <= x"FFFFFFFF";

		comRegRO(67) <= '1';
		comRegIn(67) <= ChannelData(3);
		comRegRD(67) <= x"00000000";
		comRegMk(67) <= x"FFFFFFFF";

		comRegRO(68) <= '1';
		comRegIn(68) <= ChannelData(4);
		comRegRD(68) <= x"00000000";
		comRegMk(68) <= x"FFFFFFFF";

		comRegRO(69) <= '1';
		comRegIn(69) <= ChannelData(5);
		comRegRD(69) <= x"00000000";
		comRegMk(69) <= x"FFFFFFFF";

		comRegRO(70) <= '1';
		comRegIn(70) <= ChannelData(6);
		comRegRD(70) <= x"00000000";
		comRegMk(70) <= x"FFFFFFFF";

		comRegRO(71) <= '1';
		comRegIn(71) <= ChannelData(7);
		comRegRD(71) <= x"00000000";
		comRegMk(71) <= x"FFFFFFFF";

		comRegRO(72) <= '1';
		comRegIn(72) <= ChannelData(8);
		comRegRD(72) <= x"00000000";
		comRegMk(72) <= x"FFFFFFFF";

		comRegRO(73) <= '1';
		comRegIn(73) <= ChannelData(9);
		comRegRD(73) <= x"00000000";
		comRegMk(73) <= x"FFFFFFFF";

		comRegRO(74) <= '1';
		comRegIn(74) <= ChannelData(10);
		comRegRD(74) <= x"00000000";
		comRegMk(74) <= x"FFFFFFFF";

		comRegRO(75) <= '1';
		comRegIn(75) <= ChannelData(11);
		comRegRD(75) <= x"00000000";
		comRegMk(75) <= x"FFFFFFFF";

		comRegRO(76) <= '1';
		comRegIn(76) <= ChannelData(12);
		comRegRD(76) <= x"00000000";
		comRegMk(76) <= x"FFFFFFFF";

		comRegRO(77) <= '1';
		comRegIn(77) <= ChannelData(13);
		comRegRD(77) <= x"00000000";
		comRegMk(77) <= x"FFFFFFFF";

		comRegRO(78) <= '1';
		comRegIn(78) <= ChannelData(14);
		comRegRD(78) <= x"00000000";
		comRegMk(78) <= x"FFFFFFFF";

		comRegRO(79) <= '1';
		comRegIn(79) <= ChannelData(15);
		comRegRD(79) <= x"00000000";
		comRegMk(79) <= x"FFFFFFFF";

		comRegRO(80) <= '1';
		comRegIn(80) <= ChannelData(16);
		comRegRD(80) <= x"00000000";
		comRegMk(80) <= x"FFFFFFFF";

		comRegRO(81) <= '1';
		comRegIn(81) <= ChannelData(17);
		comRegRD(81) <= x"00000000";
		comRegMk(81) <= x"FFFFFFFF";

		comRegRO(82) <= '1';
		comRegIn(82) <= ChannelData(18);
		comRegRD(82) <= x"00000000";
		comRegMk(82) <= x"FFFFFFFF";

		comRegRO(83) <= '1';
		comRegIn(83) <= ChannelData(19);
		comRegRD(83) <= x"00000000";
		comRegMk(83) <= x"FFFFFFFF";

		comRegRO(84) <= '1';
		comRegIn(84) <= ChannelData(20);
		comRegRD(84) <= x"00000000";
		comRegMk(84) <= x"FFFFFFFF";

		comRegRO(85) <= '1';
		comRegIn(85) <= ChannelData(21);
		comRegRD(85) <= x"00000000";
		comRegMk(85) <= x"FFFFFFFF";

		comRegRO(86) <= '1';
		comRegIn(86) <= ChannelData(22);
		comRegRD(86) <= x"00000000";
		comRegMk(86) <= x"FFFFFFFF";

		comRegRO(87) <= '1';
		comRegIn(87) <= ChannelData(23);
		comRegRD(87) <= x"00000000";
		comRegMk(87) <= x"FFFFFFFF";

		comRegRO(88) <= '1';
		comRegIn(88) <= ChannelData(24);
		comRegRD(88) <= x"00000000";
		comRegMk(88) <= x"FFFFFFFF";

		comRegRO(89) <= '1';
		comRegIn(89) <= ChannelData(25);
		comRegRD(89) <= x"00000000";
		comRegMk(89) <= x"FFFFFFFF";

		comRegRO(90) <= '1';
		comRegIn(90) <= ChannelData(26);
		comRegRD(90) <= x"00000000";
		comRegMk(90) <= x"FFFFFFFF";

		comRegRO(91) <= '1';
		comRegIn(91) <= ChannelData(27);
		comRegRD(91) <= x"00000000";
		comRegMk(91) <= x"FFFFFFFF";

		comRegRO(92) <= '1';
		comRegIn(92) <= ChannelData(28);
		comRegRD(92) <= x"00000000";
		comRegMk(92) <= x"FFFFFFFF";

		comRegRO(93) <= '1';
		comRegIn(93) <= ChannelData(29);
		comRegRD(93) <= x"00000000";
		comRegMk(93) <= x"FFFFFFFF";

		comRegRO(94) <= '1';
		comRegIn(94) <= ChannelData(30);
		comRegRD(94) <= x"00000000";
		comRegMk(94) <= x"FFFFFFFF";

		comRegRO(95) <= '1';
		comRegIn(95) <= ChannelData(31);
		comRegRD(95) <= x"00000000";
		comRegMk(95) <= x"FFFFFFFF";

		comRegRO(96) <= '1';
		comRegIn(96) <= ChannelBuffer(0);
		comRegRD(96) <= x"00000000";
		comRegMk(96) <= x"FFFFFFFF";

		comRegRO(97) <= '1';
		comRegIn(97) <= ChannelBuffer(1);
		comRegRD(97) <= x"00000000";
		comRegMk(97) <= x"FFFFFFFF";

		comRegRO(98) <= '1';
		comRegIn(98) <= ChannelBuffer(2);
		comRegRD(98) <= x"00000000";
		comRegMk(98) <= x"FFFFFFFF";

		comRegRO(99) <= '1';
		comRegIn(99) <= ChannelBuffer(3);
		comRegRD(99) <= x"00000000";
		comRegMk(99) <= x"FFFFFFFF";

		comRegRO(100) <= '1';
		comRegIn(100) <= ChannelBuffer(4);
		comRegRD(100) <= x"00000000";
		comRegMk(100) <= x"FFFFFFFF";

		comRegRO(101) <= '1';
		comRegIn(101) <= ChannelBuffer(5);
		comRegRD(101) <= x"00000000";
		comRegMk(101) <= x"FFFFFFFF";

		comRegRO(102) <= '1';
		comRegIn(102) <= ChannelBuffer(6);
		comRegRD(102) <= x"00000000";
		comRegMk(102) <= x"FFFFFFFF";

		comRegRO(103) <= '1';
		comRegIn(103) <= ChannelBuffer(7);
		comRegRD(103) <= x"00000000";
		comRegMk(103) <= x"FFFFFFFF";

		comRegRO(104) <= '1';
		comRegIn(104) <= ChannelBuffer(8);
		comRegRD(104) <= x"00000000";
		comRegMk(104) <= x"FFFFFFFF";

		comRegRO(105) <= '1';
		comRegIn(105) <= ChannelBuffer(9);
		comRegRD(105) <= x"00000000";
		comRegMk(105) <= x"FFFFFFFF";

		comRegRO(106) <= '1';
		comRegIn(106) <= ChannelBuffer(10);
		comRegRD(106) <= x"00000000";
		comRegMk(106) <= x"FFFFFFFF";

		comRegRO(107) <= '1';
		comRegIn(107) <= ChannelBuffer(11);
		comRegRD(107) <= x"00000000";
		comRegMk(107) <= x"FFFFFFFF";

		comRegRO(108) <= '1';
		comRegIn(108) <= ChannelBuffer(12);
		comRegRD(108) <= x"00000000";
		comRegMk(108) <= x"FFFFFFFF";

		comRegRO(109) <= '1';
		comRegIn(109) <= ChannelBuffer(13);
		comRegRD(109) <= x"00000000";
		comRegMk(109) <= x"FFFFFFFF";

		comRegRO(110) <= '1';
		comRegIn(110) <= ChannelBuffer(14);
		comRegRD(110) <= x"00000000";
		comRegMk(110) <= x"FFFFFFFF";

		comRegRO(111) <= '1';
		comRegIn(111) <= ChannelBuffer(15);
		comRegRD(111) <= x"00000000";
		comRegMk(111) <= x"FFFFFFFF";

		comRegRO(112) <= '1';
		comRegIn(112) <= ChannelBuffer(16);
		comRegRD(112) <= x"00000000";
		comRegMk(112) <= x"FFFFFFFF";

		comRegRO(113) <= '1';
		comRegIn(113) <= ChannelBuffer(17);
		comRegRD(113) <= x"00000000";
		comRegMk(113) <= x"FFFFFFFF";

		comRegRO(114) <= '1';
		comRegIn(114) <= ChannelBuffer(18);
		comRegRD(114) <= x"00000000";
		comRegMk(114) <= x"FFFFFFFF";

		comRegRO(115) <= '1';
		comRegIn(115) <= ChannelBuffer(19);
		comRegRD(115) <= x"00000000";
		comRegMk(115) <= x"FFFFFFFF";

		comRegRO(116) <= '1';
		comRegIn(116) <= ChannelBuffer(20);
		comRegRD(116) <= x"00000000";
		comRegMk(116) <= x"FFFFFFFF";

		comRegRO(117) <= '1';
		comRegIn(117) <= ChannelBuffer(21);
		comRegRD(117) <= x"00000000";
		comRegMk(117) <= x"FFFFFFFF";

		comRegRO(118) <= '1';
		comRegIn(118) <= ChannelBuffer(22);
		comRegRD(118) <= x"00000000";
		comRegMk(118) <= x"FFFFFFFF";

		comRegRO(119) <= '1';
		comRegIn(119) <= ChannelBuffer(23);
		comRegRD(119) <= x"00000000";
		comRegMk(119) <= x"FFFFFFFF";

		comRegRO(120) <= '1';
		comRegIn(120) <= ChannelBuffer(24);
		comRegRD(120) <= x"00000000";
		comRegMk(120) <= x"FFFFFFFF";

		comRegRO(121) <= '1';
		comRegIn(121) <= ChannelBuffer(25);
		comRegRD(121) <= x"00000000";
		comRegMk(121) <= x"FFFFFFFF";

		comRegRO(122) <= '1';
		comRegIn(122) <= ChannelBuffer(26);
		comRegRD(122) <= x"00000000";
		comRegMk(122) <= x"FFFFFFFF";

		comRegRO(123) <= '1';
		comRegIn(123) <= ChannelBuffer(27);
		comRegRD(123) <= x"00000000";
		comRegMk(123) <= x"FFFFFFFF";

		comRegRO(124) <= '1';
		comRegIn(124) <= ChannelBuffer(28);
		comRegRD(124) <= x"00000000";
		comRegMk(124) <= x"FFFFFFFF";

		comRegRO(125) <= '1';
		comRegIn(125) <= ChannelBuffer(29);
		comRegRD(125) <= x"00000000";
		comRegMk(125) <= x"FFFFFFFF";

		comRegRO(126) <= '1';
		comRegIn(126) <= ChannelBuffer(30);
		comRegRD(126) <= x"00000000";
		comRegMk(126) <= x"FFFFFFFF";

		comRegRO(127) <= '1';
		comRegIn(127) <= ChannelBuffer(31);
		comRegRD(127) <= x"00000000";
		comRegMk(127) <= x"FFFFFFFF";

		comRegRO(128) <= '0';
		comRegIn(128) <= comRegDataIn_MUX;
		comRegRD(128) <= x"00000304";
		comRegMk(128) <= x"FFFFFFBF";
		ConfigCustomRegister(0) <= comRegOut(128);

		comRegRO(129) <= '0';
		comRegIn(129) <= comRegDataIn_MUX;
		comRegRD(129) <= x"00000000";
		comRegMk(129) <= x"FFFFFFFF";
		ConfigCustomRegister(1) <= comRegOut(129);

		comRegRO(130) <= '0';
		comRegIn(130) <= comRegDataIn_MUX;
		comRegRD(130) <= x"00000000";
		comRegMk(130) <= x"FFFFFFFF";
		ConfigCustomRegister(2) <= comRegOut(130);

		comRegRO(131) <= '0';
		comRegIn(131) <= comRegDataIn_MUX;
		comRegRD(131) <= x"00000000";
		comRegMk(131) <= x"FFFFFFFF";
		ConfigCustomRegister(3) <= comRegOut(131);

		comRegRO(132) <= '0';
		comRegIn(132) <= comRegDataIn_MUX;
		comRegRD(132) <= x"00000304";
		comRegMk(132) <= x"FFFFFFBF";
		ConfigCustomRegister(4) <= comRegOut(132);

		comRegRO(133) <= '0';
		comRegIn(133) <= comRegDataIn_MUX;
		comRegRD(133) <= x"00000000";
		comRegMk(133) <= x"FFFFFFFF";
		ConfigCustomRegister(5) <= comRegOut(133);

		comRegRO(134) <= '0';
		comRegIn(134) <= comRegDataIn_MUX;
		comRegRD(134) <= x"00000000";
		comRegMk(134) <= x"FFFFFFFF";
		ConfigCustomRegister(6) <= comRegOut(134);

		comRegRO(135) <= '0';
		comRegIn(135) <= comRegDataIn_MUX;
		comRegRD(135) <= x"00000000";
		comRegMk(135) <= x"FFFFFFFF";
		ConfigCustomRegister(7) <= comRegOut(135);

		comRegRO(136) <= '0';
		comRegIn(136) <= comRegDataIn_MUX;
		comRegRD(136) <= x"00000304";
		comRegMk(136) <= x"FFFFFFBF";
		ConfigCustomRegister(8) <= comRegOut(136);

		comRegRO(137) <= '0';
		comRegIn(137) <= comRegDataIn_MUX;
		comRegRD(137) <= x"00000000";
		comRegMk(137) <= x"FFFFFFFF";
		ConfigCustomRegister(9) <= comRegOut(137);

		comRegRO(138) <= '0';
		comRegIn(138) <= comRegDataIn_MUX;
		comRegRD(138) <= x"00000000";
		comRegMk(138) <= x"FFFFFFFF";
		ConfigCustomRegister(10) <= comRegOut(138);

		comRegRO(139) <= '0';
		comRegIn(139) <= comRegDataIn_MUX;
		comRegRD(139) <= x"00000000";
		comRegMk(139) <= x"FFFFFFFF";
		ConfigCustomRegister(11) <= comRegOut(139);

		comRegRO(140) <= '0';
		comRegIn(140) <= comRegDataIn_MUX;
		comRegRD(140) <= x"00000304";
		comRegMk(140) <= x"FFFFFFBF";
		ConfigCustomRegister(12) <= comRegOut(140);

		comRegRO(141) <= '0';
		comRegIn(141) <= comRegDataIn_MUX;
		comRegRD(141) <= x"00000000";
		comRegMk(141) <= x"FFFFFFFF";
		ConfigCustomRegister(13) <= comRegOut(141);

		comRegRO(142) <= '0';
		comRegIn(142) <= comRegDataIn_MUX;
		comRegRD(142) <= x"00000000";
		comRegMk(142) <= x"FFFFFFFF";
		ConfigCustomRegister(14) <= comRegOut(142);

		comRegRO(143) <= '0';
		comRegIn(143) <= comRegDataIn_MUX;
		comRegRD(143) <= x"00000000";
		comRegMk(143) <= x"FFFFFFFF";
		ConfigCustomRegister(15) <= comRegOut(143);

		comRegRO(144) <= '0';
		comRegIn(144) <= comRegDataIn_MUX;
		comRegRD(144) <= x"00090324";
		comRegMk(144) <= x"FFFFFFBF";
		ConfigCustomRegister(16) <= comRegOut(144);

		comRegRO(145) <= '0';
		comRegIn(145) <= comRegDataIn_MUX;
		comRegRD(145) <= x"00000000";
		comRegMk(145) <= x"FFFFFFFF";
		ConfigCustomRegister(17) <= comRegOut(145);

		comRegRO(146) <= '0';
		comRegIn(146) <= comRegDataIn_MUX;
		comRegRD(146) <= x"00000000";
		comRegMk(146) <= x"FFFFFFFF";
		ConfigCustomRegister(18) <= comRegOut(146);

		comRegRO(147) <= '0';
		comRegIn(147) <= comRegDataIn_MUX;
		comRegRD(147) <= x"00000000";
		comRegMk(147) <= x"FFFFFFFF";
		ConfigCustomRegister(19) <= comRegOut(147);

		comRegRO(148) <= '0';
		comRegIn(148) <= comRegDataIn_MUX;
		comRegRD(148) <= x"00000000";
		comRegMk(148) <= x"FFFFFFFF";
		ConfigCustomRegister(20) <= comRegOut(148);

		comRegRO(149) <= '0';
		comRegIn(149) <= comRegDataIn_MUX;
		comRegRD(149) <= x"00000000";
		comRegMk(149) <= x"FFFFFFFF";
		ConfigCustomRegister(21) <= comRegOut(149);

		comRegRO(150) <= '0';
		comRegIn(150) <= comRegDataIn_MUX;
		comRegRD(150) <= x"00000000";
		comRegMk(150) <= x"FFFFFFFF";
		ConfigCustomRegister(22) <= comRegOut(150);

		comRegRO(151) <= '0';
		comRegIn(151) <= comRegDataIn_MUX;
		comRegRD(151) <= x"00000000";
		comRegMk(151) <= x"FFFFFFFF";
		ConfigCustomRegister(23) <= comRegOut(151);

		comRegRO(152) <= '0';
		comRegIn(152) <= comRegDataIn_MUX;
		comRegRD(152) <= x"00000000";
		comRegMk(152) <= x"FFFFFFFF";
		ConfigCustomRegister(24) <= comRegOut(152);

		comRegRO(153) <= '0';
		comRegIn(153) <= comRegDataIn_MUX;
		comRegRD(153) <= x"00000000";
		comRegMk(153) <= x"FFFFFFFF";
		ConfigCustomRegister(25) <= comRegOut(153);

		comRegRO(154) <= '0';
		comRegIn(154) <= comRegDataIn_MUX;
		comRegRD(154) <= x"00000000";
		comRegMk(154) <= x"FFFFFFFF";
		ConfigCustomRegister(26) <= comRegOut(154);

		comRegRO(155) <= '0';
		comRegIn(155) <= comRegDataIn_MUX;
		comRegRD(155) <= x"00000000";
		comRegMk(155) <= x"FFFFFFFF";
		ConfigCustomRegister(27) <= comRegOut(155);

		comRegRO(156) <= '0';
		comRegIn(156) <= comRegDataIn_MUX;
		comRegRD(156) <= x"00000000";
		comRegMk(156) <= x"FFFFFFFF";
		ConfigCustomRegister(28) <= comRegOut(156);

		comRegRO(157) <= '0';
		comRegIn(157) <= comRegDataIn_MUX;
		comRegRD(157) <= x"00000000";
		comRegMk(157) <= x"FFFFFFFF";
		ConfigCustomRegister(29) <= comRegOut(157);

		comRegRO(158) <= '0';
		comRegIn(158) <= comRegDataIn_MUX;
		comRegRD(158) <= x"00000000";
		comRegMk(158) <= x"FFFFFFFF";
		ConfigCustomRegister(30) <= comRegOut(158);

		comRegRO(159) <= '0';
		comRegIn(159) <= comRegDataIn_MUX;
		comRegRD(159) <= x"00000000";
		comRegMk(159) <= x"FFFFFFFF";
		ConfigCustomRegister(31) <= comRegOut(159);

		comRegRO(160) <= '0';
		comRegIn(160) <= comRegDataIn_MUX;
		comRegRD(160) <= x"00000000";
		comRegMk(160) <= x"FFFFFFFF";
		ConfigCustomRegister(32) <= comRegOut(160);

		comRegRO(161) <= '0';
		comRegIn(161) <= comRegDataIn_MUX;
		comRegRD(161) <= x"0FA00000";
		comRegMk(161) <= x"FFFFFFFF";
		ConfigCustomRegister(33) <= comRegOut(161);

		comRegRO(162) <= '0';
		comRegIn(162) <= comRegDataIn_MUX;
		comRegRD(162) <= x"23280000";
		comRegMk(162) <= x"FFFFFFFF";
		ConfigCustomRegister(34) <= comRegOut(162);

		comRegRO(163) <= '0';
		comRegIn(163) <= comRegDataIn_MUX;
		comRegRD(163) <= x"2EE00000";
		comRegMk(163) <= x"FFFFFFFF";
		ConfigCustomRegister(35) <= comRegOut(163);

		comRegRO(164) <= '0';
		comRegIn(164) <= comRegDataIn_MUX;
		comRegRD(164) <= x"00800000";
		comRegMk(164) <= x"FFFFFFFF";
		ConfigCustomRegister(36) <= comRegOut(164);

		comRegRO(165) <= '0';
		comRegIn(165) <= comRegDataIn_MUX;
		comRegRD(165) <= x"004CCCCC";
		comRegMk(165) <= x"FFFFFFFF";
		ConfigCustomRegister(37) <= comRegOut(165);

		comRegRO(166) <= '0';
		comRegIn(166) <= comRegDataIn_MUX;
		comRegRD(166) <= x"00266666";
		comRegMk(166) <= x"FFFFFFFF";
		ConfigCustomRegister(38) <= comRegOut(166);

		comRegRO(167) <= '0';
		comRegIn(167) <= comRegDataIn_MUX;
		comRegRD(167) <= x"00266666";
		comRegMk(167) <= x"FFFFFFFF";
		ConfigCustomRegister(39) <= comRegOut(167);

		comRegRO(168) <= '0';
		comRegIn(168) <= comRegDataIn_MUX;
		comRegRD(168) <= x"000000C8";
		comRegMk(168) <= x"FFFFFFFF";
		ConfigCustomRegister(40) <= comRegOut(168);

		comRegRO(169) <= '0';
		comRegIn(169) <= comRegDataIn_MUX;
		comRegRD(169) <= x"00000064";
		comRegMk(169) <= x"FFFFFFFF";
		ConfigCustomRegister(41) <= comRegOut(169);

		comRegRO(170) <= '0';
		comRegIn(170) <= comRegDataIn_MUX;
		comRegRD(170) <= x"00000032";
		comRegMk(170) <= x"FFFFFFFF";
		ConfigCustomRegister(42) <= comRegOut(170);

		comRegRO(171) <= '0';
		comRegIn(171) <= comRegDataIn_MUX;
		comRegRD(171) <= x"0000001E";
		comRegMk(171) <= x"FFFFFFFF";
		ConfigCustomRegister(43) <= comRegOut(171);

		comRegRO(172) <= '0';
		comRegIn(172) <= comRegDataIn_MUX;
		comRegRD(172) <= x"00266666";
		comRegMk(172) <= x"FFFFFFFF";
		ConfigCustomRegister(44) <= comRegOut(172);

		comRegRO(173) <= '0';
		comRegIn(173) <= comRegDataIn_MUX;
		comRegRD(173) <= x"00199999";
		comRegMk(173) <= x"FFFFFFFF";
		ConfigCustomRegister(45) <= comRegOut(173);

		comRegRO(174) <= '0';
		comRegIn(174) <= comRegDataIn_MUX;
		comRegRD(174) <= x"00199999";
		comRegMk(174) <= x"FFFFFFFF";
		ConfigCustomRegister(46) <= comRegOut(174);

		comRegRO(175) <= '0';
		comRegIn(175) <= comRegDataIn_MUX;
		comRegRD(175) <= x"00199999";
		comRegMk(175) <= x"FFFFFFFF";
		ConfigCustomRegister(47) <= comRegOut(175);

		comRegRO(176) <= '0';
		comRegIn(176) <= comRegDataIn_MUX;
		comRegRD(176) <= x"00000190";
		comRegMk(176) <= x"FFFFFFFF";
		ConfigCustomRegister(48) <= comRegOut(176);

		comRegRO(177) <= '0';
		comRegIn(177) <= comRegDataIn_MUX;
		comRegRD(177) <= x"0000012C";
		comRegMk(177) <= x"FFFFFFFF";
		ConfigCustomRegister(49) <= comRegOut(177);

		comRegRO(178) <= '0';
		comRegIn(178) <= comRegDataIn_MUX;
		comRegRD(178) <= x"00000064";
		comRegMk(178) <= x"FFFFFFFF";
		ConfigCustomRegister(50) <= comRegOut(178);

		comRegRO(179) <= '0';
		comRegIn(179) <= comRegDataIn_MUX;
		comRegRD(179) <= x"00000064";
		comRegMk(179) <= x"FFFFFFFF";
		ConfigCustomRegister(51) <= comRegOut(179);

		comRegRO(180) <= '0';
		comRegIn(180) <= comRegDataIn_MUX;
		comRegRD(180) <= x"00000000";
		comRegMk(180) <= x"FFFFFFFF";
		ConfigCustomRegister(52) <= comRegOut(180);

		comRegRO(181) <= '0';
		comRegIn(181) <= comRegDataIn_MUX;
		comRegRD(181) <= x"00000000";
		comRegMk(181) <= x"FFFFFFFF";
		ConfigCustomRegister(53) <= comRegOut(181);

		comRegRO(182) <= '0';
		comRegIn(182) <= comRegDataIn_MUX;
		comRegRD(182) <= x"00000000";
		comRegMk(182) <= x"FFFFFFFF";
		ConfigCustomRegister(54) <= comRegOut(182);

		comRegRO(183) <= '0';
		comRegIn(183) <= comRegDataIn_MUX;
		comRegRD(183) <= x"00000000";
		comRegMk(183) <= x"FFFFFFFF";
		ConfigCustomRegister(55) <= comRegOut(183);

		comRegRO(184) <= '0';
		comRegIn(184) <= comRegDataIn_MUX;
		comRegRD(184) <= x"00000000";
		comRegMk(184) <= x"FFFFFFFF";
		ConfigCustomRegister(56) <= comRegOut(184);

		comRegRO(185) <= '0';
		comRegIn(185) <= comRegDataIn_MUX;
		comRegRD(185) <= x"00000000";
		comRegMk(185) <= x"FFFFFFFF";
		ConfigCustomRegister(57) <= comRegOut(185);

		comRegRO(186) <= '0';
		comRegIn(186) <= comRegDataIn_MUX;
		comRegRD(186) <= x"00000000";
		comRegMk(186) <= x"FFFFFFFF";
		ConfigCustomRegister(58) <= comRegOut(186);

		comRegRO(187) <= '0';
		comRegIn(187) <= comRegDataIn_MUX;
		comRegRD(187) <= x"00000000";
		comRegMk(187) <= x"FFFFFFFF";
		ConfigCustomRegister(59) <= comRegOut(187);

		comRegRO(188) <= '0';
		comRegIn(188) <= comRegDataIn_MUX;
		comRegRD(188) <= x"00000000";
		comRegMk(188) <= x"FFFFFFFF";
		ConfigCustomRegister(60) <= comRegOut(188);

		comRegRO(189) <= '0';
		comRegIn(189) <= comRegDataIn_MUX;
		comRegRD(189) <= x"00000000";
		comRegMk(189) <= x"FFFFFFFF";
		ConfigCustomRegister(61) <= comRegOut(189);

		comRegRO(190) <= '0';
		comRegIn(190) <= comRegDataIn_MUX;
		comRegRD(190) <= x"00000000";
		comRegMk(190) <= x"FFFFFFFF";
		ConfigCustomRegister(62) <= comRegOut(190);

		comRegRO(191) <= '0';
		comRegIn(191) <= comRegDataIn_MUX;
		comRegRD(191) <= x"00000000";
		comRegMk(191) <= x"FFFFFFFF";
		ConfigCustomRegister(63) <= comRegOut(191);

		comRegRO(192) <= '0';
		comRegIn(192) <= comRegDataIn_MUX;
		comRegRD(192) <= x"00000000";
		comRegMk(192) <= x"FFFFFFFF";
		ConfigCustomRegister(64) <= comRegOut(192);

		comRegRO(193) <= '0';
		comRegIn(193) <= comRegDataIn_MUX;
		comRegRD(193) <= x"00000000";
		comRegMk(193) <= x"FFFFFFFF";
		ConfigCustomRegister(65) <= comRegOut(193);

		comRegRO(194) <= '0';
		comRegIn(194) <= comRegDataIn_MUX;
		comRegRD(194) <= x"00000000";
		comRegMk(194) <= x"FFFFFFFF";
		ConfigCustomRegister(66) <= comRegOut(194);

		comRegRO(195) <= '0';
		comRegIn(195) <= comRegDataIn_MUX;
		comRegRD(195) <= x"00000000";
		comRegMk(195) <= x"FFFFFFFF";
		ConfigCustomRegister(67) <= comRegOut(195);

		comRegRO(196) <= '0';
		comRegIn(196) <= comRegDataIn_MUX;
		comRegRD(196) <= x"00000000";
		comRegMk(196) <= x"FFFFFFFF";
		ConfigCustomRegister(68) <= comRegOut(196);

		comRegRO(197) <= '0';
		comRegIn(197) <= comRegDataIn_MUX;
		comRegRD(197) <= x"00000000";
		comRegMk(197) <= x"FFFFFFFF";
		ConfigCustomRegister(69) <= comRegOut(197);

		comRegRO(198) <= '0';
		comRegIn(198) <= comRegDataIn_MUX;
		comRegRD(198) <= x"00000000";
		comRegMk(198) <= x"FFFFFFFF";
		ConfigCustomRegister(70) <= comRegOut(198);

		comRegRO(199) <= '0';
		comRegIn(199) <= comRegDataIn_MUX;
		comRegRD(199) <= x"00000000";
		comRegMk(199) <= x"FFFFFFFF";
		ConfigCustomRegister(71) <= comRegOut(199);

		comRegRO(200) <= '0';
		comRegIn(200) <= comRegDataIn_MUX;
		comRegRD(200) <= x"00000000";
		comRegMk(200) <= x"FFFFFFFF";
		ConfigCustomRegister(72) <= comRegOut(200);

		comRegRO(201) <= '0';
		comRegIn(201) <= comRegDataIn_MUX;
		comRegRD(201) <= x"00000000";
		comRegMk(201) <= x"FFFFFFFF";
		ConfigCustomRegister(73) <= comRegOut(201);

		comRegRO(202) <= '0';
		comRegIn(202) <= comRegDataIn_MUX;
		comRegRD(202) <= x"00000000";
		comRegMk(202) <= x"FFFFFFFF";
		ConfigCustomRegister(74) <= comRegOut(202);

		comRegRO(203) <= '0';
		comRegIn(203) <= comRegDataIn_MUX;
		comRegRD(203) <= x"00000000";
		comRegMk(203) <= x"FFFFFFFF";
		ConfigCustomRegister(75) <= comRegOut(203);

		comRegRO(204) <= '0';
		comRegIn(204) <= comRegDataIn_MUX;
		comRegRD(204) <= x"00000000";
		comRegMk(204) <= x"FFFFFFFF";
		ConfigCustomRegister(76) <= comRegOut(204);

		comRegRO(205) <= '0';
		comRegIn(205) <= comRegDataIn_MUX;
		comRegRD(205) <= x"00000000";
		comRegMk(205) <= x"FFFFFFFF";
		ConfigCustomRegister(77) <= comRegOut(205);

		comRegRO(206) <= '0';
		comRegIn(206) <= comRegDataIn_MUX;
		comRegRD(206) <= x"00000000";
		comRegMk(206) <= x"FFFFFFFF";
		ConfigCustomRegister(78) <= comRegOut(206);

		comRegRO(207) <= '0';
		comRegIn(207) <= comRegDataIn_MUX;
		comRegRD(207) <= x"00000000";
		comRegMk(207) <= x"FFFFFFFF";
		ConfigCustomRegister(79) <= comRegOut(207);

		comRegRO(208) <= '0';
		comRegIn(208) <= comRegDataIn_MUX;
		comRegRD(208) <= x"00000000";
		comRegMk(208) <= x"FFFFFFFF";
		ConfigCustomRegister(80) <= comRegOut(208);

		comRegRO(209) <= '0';
		comRegIn(209) <= comRegDataIn_MUX;
		comRegRD(209) <= x"00000000";
		comRegMk(209) <= x"FFFFFFFF";
		ConfigCustomRegister(81) <= comRegOut(209);

		comRegRO(210) <= '0';
		comRegIn(210) <= comRegDataIn_MUX;
		comRegRD(210) <= x"00000000";
		comRegMk(210) <= x"FFFFFFFF";
		ConfigCustomRegister(82) <= comRegOut(210);

		comRegRO(211) <= '0';
		comRegIn(211) <= comRegDataIn_MUX;
		comRegRD(211) <= x"00000000";
		comRegMk(211) <= x"FFFFFFFF";
		ConfigCustomRegister(83) <= comRegOut(211);

		comRegRO(212) <= '0';
		comRegIn(212) <= comRegDataIn_MUX;
		comRegRD(212) <= x"00000000";
		comRegMk(212) <= x"FFFFFFFF";
		ConfigCustomRegister(84) <= comRegOut(212);

		comRegRO(213) <= '0';
		comRegIn(213) <= comRegDataIn_MUX;
		comRegRD(213) <= x"00000000";
		comRegMk(213) <= x"FFFFFFFF";
		ConfigCustomRegister(85) <= comRegOut(213);

		comRegRO(214) <= '0';
		comRegIn(214) <= comRegDataIn_MUX;
		comRegRD(214) <= x"00000000";
		comRegMk(214) <= x"FFFFFFFF";
		ConfigCustomRegister(86) <= comRegOut(214);

		comRegRO(215) <= '0';
		comRegIn(215) <= comRegDataIn_MUX;
		comRegRD(215) <= x"00000000";
		comRegMk(215) <= x"FFFFFFFF";
		ConfigCustomRegister(87) <= comRegOut(215);

		comRegRO(216) <= '0';
		comRegIn(216) <= comRegDataIn_MUX;
		comRegRD(216) <= x"00000000";
		comRegMk(216) <= x"FFFFFFFF";
		ConfigCustomRegister(88) <= comRegOut(216);

		comRegRO(217) <= '0';
		comRegIn(217) <= comRegDataIn_MUX;
		comRegRD(217) <= x"00000000";
		comRegMk(217) <= x"FFFFFFFF";
		ConfigCustomRegister(89) <= comRegOut(217);

		comRegRO(218) <= '0';
		comRegIn(218) <= comRegDataIn_MUX;
		comRegRD(218) <= x"00000000";
		comRegMk(218) <= x"FFFFFFFF";
		ConfigCustomRegister(90) <= comRegOut(218);

		comRegRO(219) <= '0';
		comRegIn(219) <= comRegDataIn_MUX;
		comRegRD(219) <= x"00000000";
		comRegMk(219) <= x"FFFFFFFF";
		ConfigCustomRegister(91) <= comRegOut(219);

		comRegRO(220) <= '0';
		comRegIn(220) <= comRegDataIn_MUX;
		comRegRD(220) <= x"00000000";
		comRegMk(220) <= x"FFFFFFFF";
		ConfigCustomRegister(92) <= comRegOut(220);

		comRegRO(221) <= '0';
		comRegIn(221) <= comRegDataIn_MUX;
		comRegRD(221) <= x"00000000";
		comRegMk(221) <= x"FFFFFFFF";
		ConfigCustomRegister(93) <= comRegOut(221);

		comRegRO(222) <= '0';
		comRegIn(222) <= comRegDataIn_MUX;
		comRegRD(222) <= x"00000000";
		comRegMk(222) <= x"FFFFFFFF";
		ConfigCustomRegister(94) <= comRegOut(222);

		comRegRO(223) <= '0';
		comRegIn(223) <= comRegDataIn_MUX;
		comRegRD(223) <= x"00000000";
		comRegMk(223) <= x"FFFFFFFF";
		ConfigCustomRegister(95) <= comRegOut(223);

		comRegRO(224) <= '0';
		comRegIn(224) <= comRegDataIn_MUX;
		comRegRD(224) <= x"00000000";
		comRegMk(224) <= x"FFFFFFFF";
		ConfigCustomRegister(96) <= comRegOut(224);

		comRegRO(225) <= '0';
		comRegIn(225) <= comRegDataIn_MUX;
		comRegRD(225) <= x"00000000";
		comRegMk(225) <= x"FFFFFFFF";
		ConfigCustomRegister(97) <= comRegOut(225);

		comRegRO(226) <= '0';
		comRegIn(226) <= comRegDataIn_MUX;
		comRegRD(226) <= x"00000000";
		comRegMk(226) <= x"FFFFFFFF";
		ConfigCustomRegister(98) <= comRegOut(226);

		comRegRO(227) <= '0';
		comRegIn(227) <= comRegDataIn_MUX;
		comRegRD(227) <= x"00000000";
		comRegMk(227) <= x"FFFFFFFF";
		ConfigCustomRegister(99) <= comRegOut(227);

		comRegRO(228) <= '0';
		comRegIn(228) <= comRegDataIn_MUX;
		comRegRD(228) <= x"00000000";
		comRegMk(228) <= x"FFFFFFFF";
		ConfigCustomRegister(100) <= comRegOut(228);

		comRegRO(229) <= '0';
		comRegIn(229) <= comRegDataIn_MUX;
		comRegRD(229) <= x"00000000";
		comRegMk(229) <= x"FFFFFFFF";
		ConfigCustomRegister(101) <= comRegOut(229);

		comRegRO(230) <= '0';
		comRegIn(230) <= comRegDataIn_MUX;
		comRegRD(230) <= x"00000000";
		comRegMk(230) <= x"FFFFFFFF";
		ConfigCustomRegister(102) <= comRegOut(230);

		comRegRO(231) <= '0';
		comRegIn(231) <= comRegDataIn_MUX;
		comRegRD(231) <= x"00000000";
		comRegMk(231) <= x"FFFFFFFF";
		ConfigCustomRegister(103) <= comRegOut(231);

		comRegRO(232) <= '0';
		comRegIn(232) <= comRegDataIn_MUX;
		comRegRD(232) <= x"00000000";
		comRegMk(232) <= x"FFFFFFFF";
		ConfigCustomRegister(104) <= comRegOut(232);

		comRegRO(233) <= '0';
		comRegIn(233) <= comRegDataIn_MUX;
		comRegRD(233) <= x"00000000";
		comRegMk(233) <= x"FFFFFFFF";
		ConfigCustomRegister(105) <= comRegOut(233);

		comRegRO(234) <= '0';
		comRegIn(234) <= comRegDataIn_MUX;
		comRegRD(234) <= x"00000000";
		comRegMk(234) <= x"FFFFFFFF";
		ConfigCustomRegister(106) <= comRegOut(234);

		comRegRO(235) <= '0';
		comRegIn(235) <= comRegDataIn_MUX;
		comRegRD(235) <= x"00000000";
		comRegMk(235) <= x"FFFFFFFF";
		ConfigCustomRegister(107) <= comRegOut(235);

		comRegRO(236) <= '0';
		comRegIn(236) <= comRegDataIn_MUX;
		comRegRD(236) <= x"00000000";
		comRegMk(236) <= x"FFFFFFFF";
		ConfigCustomRegister(108) <= comRegOut(236);

		comRegRO(237) <= '0';
		comRegIn(237) <= comRegDataIn_MUX;
		comRegRD(237) <= x"00000000";
		comRegMk(237) <= x"FFFFFFFF";
		ConfigCustomRegister(109) <= comRegOut(237);

		comRegRO(238) <= '0';
		comRegIn(238) <= comRegDataIn_MUX;
		comRegRD(238) <= x"00000000";
		comRegMk(238) <= x"FFFFFFFF";
		ConfigCustomRegister(110) <= comRegOut(238);

		comRegRO(239) <= '0';
		comRegIn(239) <= comRegDataIn_MUX;
		comRegRD(239) <= x"00000000";
		comRegMk(239) <= x"FFFFFFFF";
		ConfigCustomRegister(111) <= comRegOut(239);

		comRegRO(240) <= '0';
		comRegIn(240) <= comRegDataIn_MUX;
		comRegRD(240) <= x"00000000";
		comRegMk(240) <= x"FFFFFFFF";
		ConfigCustomRegister(112) <= comRegOut(240);

		comRegRO(241) <= '0';
		comRegIn(241) <= comRegDataIn_MUX;
		comRegRD(241) <= x"00000000";
		comRegMk(241) <= x"FFFFFFFF";
		ConfigCustomRegister(113) <= comRegOut(241);

		comRegRO(242) <= '0';
		comRegIn(242) <= comRegDataIn_MUX;
		comRegRD(242) <= x"00000000";
		comRegMk(242) <= x"FFFFFFFF";
		ConfigCustomRegister(114) <= comRegOut(242);

		comRegRO(243) <= '0';
		comRegIn(243) <= comRegDataIn_MUX;
		comRegRD(243) <= x"00000000";
		comRegMk(243) <= x"FFFFFFFF";
		ConfigCustomRegister(115) <= comRegOut(243);

		comRegRO(244) <= '0';
		comRegIn(244) <= comRegDataIn_MUX;
		comRegRD(244) <= x"00000000";
		comRegMk(244) <= x"FFFFFFFF";
		ConfigCustomRegister(116) <= comRegOut(244);

		comRegRO(245) <= '0';
		comRegIn(245) <= comRegDataIn_MUX;
		comRegRD(245) <= x"00000000";
		comRegMk(245) <= x"FFFFFFFF";
		ConfigCustomRegister(117) <= comRegOut(245);

		comRegRO(246) <= '0';
		comRegIn(246) <= comRegDataIn_MUX;
		comRegRD(246) <= x"00000000";
		comRegMk(246) <= x"FFFFFFFF";
		ConfigCustomRegister(118) <= comRegOut(246);

		comRegRO(247) <= '0';
		comRegIn(247) <= comRegDataIn_MUX;
		comRegRD(247) <= x"00000000";
		comRegMk(247) <= x"FFFFFFFF";
		ConfigCustomRegister(119) <= comRegOut(247);

		comRegRO(248) <= '0';
		comRegIn(248) <= comRegDataIn_MUX;
		comRegRD(248) <= x"00000000";
		comRegMk(248) <= x"FFFFFFFF";
		ConfigCustomRegister(120) <= comRegOut(248);

		comRegRO(249) <= '0';
		comRegIn(249) <= comRegDataIn_MUX;
		comRegRD(249) <= x"00000000";
		comRegMk(249) <= x"FFFFFFFF";
		ConfigCustomRegister(121) <= comRegOut(249);

		comRegRO(250) <= '0';
		comRegIn(250) <= comRegDataIn_MUX;
		comRegRD(250) <= x"00000000";
		comRegMk(250) <= x"FFFFFFFF";
		ConfigCustomRegister(122) <= comRegOut(250);

		comRegRO(251) <= '0';
		comRegIn(251) <= comRegDataIn_MUX;
		comRegRD(251) <= x"00000000";
		comRegMk(251) <= x"FFFFFFFF";
		ConfigCustomRegister(123) <= comRegOut(251);

		comRegRO(252) <= '0';
		comRegIn(252) <= comRegDataIn_MUX;
		comRegRD(252) <= x"00000000";
		comRegMk(252) <= x"FFFFFFFF";
		ConfigCustomRegister(124) <= comRegOut(252);

		comRegRO(253) <= '0';
		comRegIn(253) <= comRegDataIn_MUX;
		comRegRD(253) <= x"00000000";
		comRegMk(253) <= x"FFFFFFFF";
		ConfigCustomRegister(125) <= comRegOut(253);

		comRegRO(254) <= '0';
		comRegIn(254) <= comRegDataIn_MUX;
		comRegRD(254) <= x"00000000";
		comRegMk(254) <= x"FFFFFFFF";
		ConfigCustomRegister(126) <= comRegOut(254);

		comRegRO(255) <= '0';
		comRegIn(255) <= comRegDataIn_MUX;
		comRegRD(255) <= x"00000000";
		comRegMk(255) <= x"FFFFFFFF";
		ConfigCustomRegister(127) <= comRegOut(255);

	end procedure;

	procedure RegisterMapConfigBlock (
		signal ConfigCustomRegister   : in customRegArray;
		signal ChannelGenericA	:out regChannelsArray;
		signal ChannelGenericB	:out regChannelsArray;
		signal ChannelGenericC	:out regChannelsArray;
		signal ChannelGenericD	:out regChannelsArray;
		signal CurrLevelTable	:out regArrayCurDepSettings;
		signal DynThreshTableComp0	:out regArrayCurDepSettings;
		signal DynDiscrTableComp0	:out regArrayCurDepSettings;
		signal DynThreshTableComp1	:out regArrayCurDepSettings;
		signal DynDiscrTableComp1	:out regArrayCurDepSettings
	) is 
 	begin 
		ChannelGenericA(0) <= ConfigCustomRegister(0);
		ChannelGenericB(0) <= ConfigCustomRegister(1);
		ChannelGenericC(0) <= ConfigCustomRegister(2);
		ChannelGenericD(0) <= ConfigCustomRegister(3);
		ChannelGenericA(1) <= ConfigCustomRegister(4);
		ChannelGenericB(1) <= ConfigCustomRegister(5);
		ChannelGenericC(1) <= ConfigCustomRegister(6);
		ChannelGenericD(1) <= ConfigCustomRegister(7);
		ChannelGenericA(2) <= ConfigCustomRegister(8);
		ChannelGenericB(2) <= ConfigCustomRegister(9);
		ChannelGenericC(2) <= ConfigCustomRegister(10);
		ChannelGenericD(2) <= ConfigCustomRegister(11);
		ChannelGenericA(3) <= ConfigCustomRegister(12);
		ChannelGenericB(3) <= ConfigCustomRegister(13);
		ChannelGenericC(3) <= ConfigCustomRegister(14);
		ChannelGenericD(3) <= ConfigCustomRegister(15);
		ChannelGenericA(4) <= ConfigCustomRegister(16);
		ChannelGenericB(4) <= ConfigCustomRegister(17);
		ChannelGenericC(4) <= ConfigCustomRegister(18);
		ChannelGenericD(4) <= ConfigCustomRegister(19);
		CurrLevelTable(0) <= ConfigCustomRegister(32);
		CurrLevelTable(1) <= ConfigCustomRegister(33);
		CurrLevelTable(2) <= ConfigCustomRegister(34);
		CurrLevelTable(3) <= ConfigCustomRegister(35);
		DynThreshTableComp0(0) <= ConfigCustomRegister(36);
		DynThreshTableComp0(1) <= ConfigCustomRegister(37);
		DynThreshTableComp0(2) <= ConfigCustomRegister(38);
		DynThreshTableComp0(3) <= ConfigCustomRegister(39);
		DynDiscrTableComp0(0) <= ConfigCustomRegister(40);
		DynDiscrTableComp0(1) <= ConfigCustomRegister(41);
		DynDiscrTableComp0(2) <= ConfigCustomRegister(42);
		DynDiscrTableComp0(3) <= ConfigCustomRegister(43);
		DynThreshTableComp1(0) <= ConfigCustomRegister(44);
		DynThreshTableComp1(1) <= ConfigCustomRegister(45);
		DynThreshTableComp1(2) <= ConfigCustomRegister(46);
		DynThreshTableComp1(3) <= ConfigCustomRegister(47);
		DynDiscrTableComp1(0) <= ConfigCustomRegister(48);
		DynDiscrTableComp1(1) <= ConfigCustomRegister(49);
		DynDiscrTableComp1(2) <= ConfigCustomRegister(50);
		DynDiscrTableComp1(3) <= ConfigCustomRegister(51);
	end procedure;

end reg_map_pkg;