library ieee;
use ieee.std_logic_1164.all;
use std.textio.all;
use IEEE.numeric_std.all;

library UQDSlib;
use UQDSlib.UQDSlib.all;

entity data_gen is
  generic(	noch			: natural := 13;
			PACKET_SIZE 	: natural := 8 + noch * 4;
			chw				: natural := 32;
			nsch 			: natural := 5;		-- channel with sinus
			nr_cycle_trans	: natural := 100);	--sampling period
	port(	clk, nrst		: in std_logic;
			data			: out data8Array(PACKET_SIZE - 9 downto 0);
			en_wr, debug			: out std_logic;
			SPI_MOSI		: in std_logic;
			SPI_MISO		: out std_logic;
			SPI_CLOCK		: in std_logic;
			SPI_CS			: in std_logic);
end;

architecture rtl of data_gen is
constant len : natural := 8;
constant answer_length : natural := 5;
type DataArray8 is array (answer_length-1 downto 0) of std_logic_vector(len-1 downto 0);
--constant nr_cycle_trans : natural := 391;
constant spi_answer : DataArray8 := ((x"10"), (x"00"), (x"AF"), (x"FE"), (x"41"));
constant bits8 : outputChannelsArray := 
	((x"adf13465"), (x"cf045678"), (x"1234fedc"), (x"aaddbbbb"), (x"12340987"), 
	(x"ababcdcd"), (x"adf13465"), (x"cf045678"), (x"1234fedc"), (x"aaaabbbb"), 
	(x"a0aab1bb"), (x"1a340987"), (x"ababcdcd"), (x"fefe0101"), (x"1a340987"), 
	(x"ababcdcd"), (x"adf13465"), (x"cf045678"), (x"1234fedc"), (x"aaaabbbb"),
	(x"a0aab1bb"), (x"1a340987"), (x"ababcdcd"), (x"fefe0101"), (x"1a340987")
	);

constant spich : natural := 2;	-- channel with SPI data
signal trcnt : unsigned(2 downto 0);
	
signal scnt		: natural := 0;
signal sinsig : std_logic_vector(chw-1 downto 0);
signal rst, cs, spiclk, mosi, miso, rx_valid_pulse, tx_valid_pulse, stC, stTR : std_logic;
signal te : boolean;
signal for_transmit, received_dat : std_logic_vector(len-1 downto 0);
--signal trans_cnt : unsigned(2 downto 0);
type states is (idle, A, B, C, TR, TR_DATA);
signal st, nst : states;

component sine_iir is        
	generic(dlength : natural := 32);      
	port (clk, rst, sample : in std_logic; 
		sine_out : out std_logic_vector(dlength-1 downto 0));
end component;

component spi_slave is 				
generic(trans_length : natural := 8);
port (clk, nrst : in std_logic;
    --spi hw lines
    spi_csn			:in std_logic;
	spi_sclk		:in std_logic;
	spi_sdin		:in std_logic;
	spi_sdout		:out std_logic;
	debug			:out std_logic;
	--internal signals
	rdata			:out std_logic_vector(trans_length-1 downto 0);
	wdata			:in std_logic_vector(trans_length-1 downto 0);
	rx_valid_pulse	:out std_logic;
	tx_valid_pulse	:out std_logic
);
end component;

begin

sampling_out: process(clk)
begin
	if rising_edge(clk) then
		if nrst = '0' then
			en_wr <= '0';
			scnt <= 0;
		else
			scnt <= scnt + 1;
			if scnt = nr_cycle_trans-1 then
				en_wr <= '1';
				scnt <= 0;
			else
				en_wr <= '0';
				scnt <= scnt + 1;
			end if;
		end if;
	end if;
end process;

sin_wave_gen : sine_iir      
	generic map(dlength => chw)     
	port map(clk => clk, rst => nrst, sample => en_wr, sine_out => sinsig);

--========================================================================

fast_spi_slave : spi_slave
		generic map(trans_length => len)
		port map(clk => clk, nrst => nrst, spi_csn => SPI_CS, spi_sclk => SPI_CLOCK, spi_sdin => SPI_MOSI, spi_sdout => SPI_MISO, debug => debug, 
				rdata => received_dat, wdata => for_transmit, rx_valid_pulse => rx_valid_pulse, tx_valid_pulse => tx_valid_pulse);

state_reg_pattern24: process(clk, rst)
begin		
	if nrst = '0' then
		st <= idle;
	else
		if rising_edge(clk) then
			st <= nst;
		end if;
	end if;
end process;

next_st_log: process(all)
begin
		stC <= '0';
		stTR <= '0';
		case st is
			when idle => if rx_valid_pulse = '0' then
							nst <= idle;
						else
							if received_dat = x"04" then
								nst <= A;
							else
								nst <= idle;
							end if;
						end if;
			when A => if rx_valid_pulse = '0' then
							nst <= A;
						else
							if received_dat = x"06" then
								nst <= B;
							elsif received_dat = x"04" then
								nst <= A;
							else
								nst <= idle;
							end if;
						end if;
			when B => if rx_valid_pulse = '0' then
							nst <= B;
						else
							if received_dat = x"02" then
								nst <= C;
							else
								nst <= idle;
							end if;
						end if;
			when C =>	stC <= '1';
						if tx_valid_pulse = '1' then
							nst <= TR_DATA;
						else
							nst <= C;
						end if;
			when TR_DATA => nst <= TR;
			when TR =>	stTR <= '1';
						if tx_valid_pulse = '1' then
							nst <= idle when trcnt = answer_length else TR_DATA;
						else
							nst <= TR;
						end if;
		end case;
end process;

transmittion_counter: process(clk)
begin	
	if rising_edge(clk) then
		if nrst = '0' then
			trcnt <= (others => '0');
		else
			if st = TR_DATA then
				trcnt <= trcnt + 1;
			elsif st = idle then
				trcnt <= (others => '0');
			end if;
		end if;
	end if;
end process;

data_for_transmittion: process(clk)
begin	
	if rising_edge(clk) then
		if nrst = '0' then
			for_transmit <= (others => '0');
		else
			if st = TR_DATA then
				for_transmit <= spi_answer(answer_length-to_integer(trcnt)-1);
			elsif st = idle then
				for_transmit <= x"00";
			end if;
		end if;
	end if;
end process;		

--debug <= '0';
-- start_transmition_debug_led: process(clk)
-- begin	
	-- if rising_edge(clk) then
		-- if rst = '1' then
			-- debug <= '0';
		-- else
			-- if st = TR_DATA then
				-- debug <= '1';
			-- end if;
		-- end if;
	-- end if;
-- end process;
--SPI_MISO <= stTR;		--SPI_CS or 
--'1' when ((st = TR_DATA) or (st = A) or (tx_valid_pulse = '1') or (rx_valid_pulse = '1')) else '0';
--or tx_valid_pulse = '1' or rx_valid_pulse = '1'
--SPI_MISO <= received_dat(0);
--SPI_MISO <= rx_valid_pulse when received_dat = x"04" else '0';
--'1' when st = idle else '0';		
--========================================================================

data_assignments: process(clk)
begin
	if rising_edge(clk) then
		if en_wr = '1' then
			for i in 0 to noch-1 loop	
				if i = nsch or i = noch-1 then
					data(4*i) <= sinsig(31 downto 24); 
				    data(4*i+1) <= sinsig(23 downto 16);
				    data(4*i+2) <= sinsig(15 downto 8);
				    data(4*i+3) <= sinsig(7 downto 0);
				elsif i = spich then
					data(4*i) <= x"00"; 
				    data(4*i+1) <= x"00";
				    data(4*i+2) <= x"00";
				    data(4*i+3) <= received_dat;					
				else
					data(4*i) <= bits8(i)(31 downto 24); 
					data(4*i+1) <= bits8(i)(23 downto 16);
					data(4*i+2) <= bits8(i)(15 downto 8);
					data(4*i+3) <= bits8(i)(7 downto 0);
				end if;
			end loop;
		end if;
	end if;
end process;

end;
