library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity FIFOSRAM is

   port(WD    : in    std_logic_vector(7 downto 0);
        RD    : out   std_logic_vector(7 downto 0);
        WADDR : in    std_logic_vector(9 downto 0);
        RADDR : in    std_logic_vector(9 downto 0);
        WEN   : in    std_logic;
        REN   : in    std_logic;
        WCLK  : in    std_logic;
        RCLK  : in    std_logic
       );

end;

architecture RTL of FIFOSRAM is 
constant depth : natural := 2**WADDR'length;

type mem_type is array (depth-1 downto 0) of std_logic_vector(7 downto 0); 
signal mem : mem_type; 
attribute syn_ramstyle : string; 
attribute syn_ramstyle of mem : signal is "lsram"; 
--attribute syn_ramstyle of mem : signal is "rw_check"; 
	
begin
read_from_sram : process(rclk)
begin
	if falling_edge(rclk) then
	if ren = '1' then
		rd <= mem(conv_integer(raddr));
	end if;
	end if;
end process;

write_to_sram : process(wclk)
begin
	if falling_edge(wclk) then
	if wen = '1' then
		mem(conv_integer(waddr)) <= wd;
	end if;
	end if;
end process;

end architecture;
