library IEEE;
use ieee.numeric_std.all;
use IEEE.std_logic_1164.all;
use ieee.std_logic_signed.all;
use IEEE.MATH_REAL.ALL;
use ieee.fixed_pkg.all;

entity sine_iir is        
	generic(dlength : natural := 32);      
	port (clk, rst, sample : in std_logic; 
		sine_out : out std_logic_vector(dlength-1 downto 0));
end entity;

architecture rtl of sine_iir is
constant zero : sfixed(1 downto 2-dlength) := to_sfixed(0.0,1,2-dlength);
constant b1 : sfixed(1 downto 2-dlength) := to_sfixed(1.94754,1,2-dlength);
constant b2 : sfixed(1 downto 2-dlength) := to_sfixed(1.0,1,2-dlength);
constant y1 : sfixed(1 downto 2-dlength) := to_sfixed(0.1564,1,2-dlength);

type mem is array (1 downto 0) of sfixed(1 downto 2-dlength);
signal sinus : mem;
--signal temp : signed(2*dlength-1 downto 0);

begin

sin_gen : process (clk)
begin
	if rising_edge(clk) then
		if rst = '0' then
			sinus(0) <= zero;
			sinus(1) <= y1;
		else
			if sample = '1' then
				sinus(0) <= sinus(1);
				sinus(1) <= resize(b1*sinus(1) - b2*sinus(0),1,2-dlength);
			end if;
		end if;
	end if;
	
end process;

sine_out <= to_slv(sinus(1));

end architecture;
