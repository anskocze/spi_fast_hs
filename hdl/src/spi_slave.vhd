library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;
use IEEE.NUMERIC_STD.ALL;

entity spi_slave is
	generic(trans_length : natural := 8);
	port(clk, nrst		:in std_logic;
	    spi_csn			:in std_logic;
		spi_sclk		:in std_logic;
		spi_sdin		:in std_logic;
		spi_sdout		:out std_logic;
		debug			:out std_logic;
		rdata			:out std_logic_vector(trans_length-1 downto 0);
		wdata			:in std_logic_vector(trans_length-1 downto 0);
		rx_valid_pulse	:out std_logic;
		tx_valid_pulse	:out std_logic
	);
end entity;

architecture rtl of spi_slave is

signal ned_rx, tx,rxack, rxreq, txack, txreq : std_logic;
signal nedrx : std_logic_vector(7 downto 0);
signal nedtx : std_logic_vector(6 downto 0);
signal for_transmit, received_dat : std_logic_vector(trans_length-1 downto 0);

component spi_slave_fast is 				
generic(trans_length : natural := 8);
port (nrst : in std_logic;
    --spi hw lines
    spi_csn			:in std_logic;
	spi_sclk		:in std_logic;
	spi_sdin		:in std_logic;
	spi_sdout		:out std_logic;
	--internal signals
	rdata			:out std_logic_vector(trans_length-1 downto 0);
	wdata			:in std_logic_vector(trans_length-1 downto 0);
	rxreq			:out std_logic;
	rxack			:in std_logic;
	txreq			:in std_logic;
	txack			:out std_logic
);
end component;

begin
debug <= '0';
fast : spi_slave_fast
		generic map(trans_length => trans_length)
		port map(nrst => nrst, spi_csn => spi_csn, spi_sclk => spi_sclk, spi_sdin => spi_sdin, spi_sdout => spi_sdout, 
		rdata => received_dat, wdata => for_transmit, rxreq => rxreq, rxack => rxack, txreq => txreq, txack => txack);

synchro_rxreq: process(clk) 
begin
	if rising_edge(clk) then
		if nrst = '0' then
			nedrx <= (others => '0');
		else
			nedrx <= nedrx(6 downto 0) & rxreq;
		end if;
	end if;
end process;
ned_rx <= nedrx(1) and not nedrx(0);
rxack <= not nedrx(1) and nedrx(7);
rx_valid_pulse <= nedrx(3) and nedrx(4);
tx_valid_pulse <= nedrx(7) and not nedrx(6);

receive_register: process(clk)
begin
	if rising_edge(clk) then
		if nrst = '0' then
			rdata <= (others => '0');
		else
			if ned_rx = '1' then
				rdata <= received_dat;
			end if;
		end if;
	end if;
end process;

transmit_register: process(clk)
begin
	if rising_edge(clk) then
		if nrst = '0' then
			for_transmit <= (others => '0');
		else
			if txreq = '1' then
				for_transmit <= wdata;
			end if;
		end if;
	end if;
end process;

tx_ned: process(clk) begin
	if rising_edge(clk) then
		if nrst = '0' then
			nedtx <= (others => '0');
		else
			nedtx <= nedtx(5 downto 0) & spi_csn;
		end if;
	end if;
end process;
txreq <= not nedtx(0) and nedtx(2);
--spi_sdout <=  spi_csn;		--nedtx(0);

end architecture;
