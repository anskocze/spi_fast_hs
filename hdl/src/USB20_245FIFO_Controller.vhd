--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: USB20_245FIFO_Controller.vhd
-- File history:
--      <Revision number>: <Date>: <Designer>: <Comments>
--		?.?	:	?.?.?	:	(dbs)	:	creation
--		1.0	:	6.08.2019	:	(as)	:	ByteeCounter was a combinatorial process ! + Additional condition for byte_cnt increment to avoid range violation
--
-- Description: 
--
-- Sends information to USB 3.0 (FT601) through 245 FIFO (32 bits)
--
-- Targeted device: <Family::SmartFusion2> <Die::M2S150TS> <Package::1152 FC>
-- Author: Daniel Blasco Serrano
--
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library UQDSLib;
use UQDSLib.UQDSLib.all;

entity USB20_245FIFO_Controller is
	generic(
		PACKET_SIZE : natural := 11
	);
	port(
		-- USB 245FIFO lines
		--USBnRXF    : in  std_logic;     -- Data available to be received (not used)
		USBnTXE    : in  std_logic;     -- Data allowed to be sent
		USBCLK     : in  std_logic;
		USBDataBus : out std_logic_vector(7 downto 0);
		USBnRD     : out std_logic;     -- Start receiving data (not used)
		USBnWR     : out std_logic;     -- Start sending data
		USBnOE     : out std_logic;     -- 0 - Enable reading / 1 - Enable writing
		-- FIFO lines
		FIFOData   : in  data8Array(PACKET_SIZE - 9 downto 0); -- Data to be added to FIFO (header, counter and CRC not included)
		FIFOWR     : in  std_logic;     -- Enable writing
		FIFOFull   : out std_logic;
		clk        : in  std_logic;
		nRST       : in  std_logic
	);
end USB20_245FIFO_Controller;

architecture behav of USB20_245FIFO_Controller is

	type states is (IDLE, SAMPLEHOLD, WAITEMPTY, WRITEBYTES, DONE);
	type txBuffer is array (PACKET_SIZE - 1 downto 0) of std_logic_vector(7 downto 0);

	signal current_state : states;
	signal next_state    : states;

	signal data_buffer : txBuffer;      -- Whole packet to be written in FIFO
	signal byte_buffer : std_logic_vector(7 downto 0); -- Actual byte being written in FIFO

	signal byte_cnt           : integer range 0 to PACKET_SIZE;				 --		- 1
	signal start_byte_counter : std_logic;

	signal sample_cnt     : integer range 0 to integer'high;
	signal sample_cnt_reg : std_logic_vector(31 downto 0);

	signal write_byte : std_logic;

	signal FIFO_empty : std_logic;

	signal USBDataBusWire : std_logic_vector(7 downto 0);

	signal USBnTXEPrev : std_logic := '1';
	signal USBnTXEBuff : std_logic := '1';

	signal actual_counter, previous_counter : integer range 0 to integer'high;
	signal flag_missing_packets             : std_logic := '0';
	signal USBnWR_early		: std_logic;
BEGIN

	FSMClock : process(clk)
	begin
		if rising_edge(clk) then
			if nRST = '0' then
				current_state <= IDLE;
			else
				current_state <= next_state;
			end if;
		end if;
	end process;

	FSMMain : process(all)
	begin
		next_state <= IDLE;
		case current_state is

			when IDLE =>                -- Wait for incoming data (and FIFO is not full)
				if (FIFOFull = '0') and (FIFOWR = '1') then
					next_state <= SAMPLEHOLD;
				else
					next_state <= IDLE;
				end if;

			when SAMPLEHOLD =>          -- Adding header and counter to packet
				next_state <= WRITEBYTES;

			when WAITEMPTY =>           -- FIFO became full in middle of packet
				if FIFOFull = '1' then
					next_state <= WAITEMPTY;
				else
					next_state <= WRITEBYTES;
				end if;

			when WRITEBYTES =>          -- Currently writing bytes to FIFO
				if byte_cnt >= PACKET_SIZE- 1 then						--  
					next_state <= DONE;
				elsif FIFOFull = '1' then
					next_state <= WAITEMPTY;
				else
					next_state <= WRITEBYTES;
				end if;

			when DONE => null;
				

		end case;
	end process;

	FSMSignals : process(all)
	begin
	byte_buffer        <= x"00";
	start_byte_counter <= '0';
	write_byte         <= '0';
		case current_state is
			when SAMPLEHOLD =>
				start_byte_counter <= '1';
			when WAITEMPTY =>
				byte_buffer        <= data_buffer(byte_cnt);
			when WRITEBYTES =>
				byte_buffer        <= data_buffer(byte_cnt);
				write_byte         <= '1';
			when others => null;
		end case;
	end process;

	SampleHoldCurrentValues : process(clk)
	begin
		if rising_edge(clk) then
			if current_state = SAMPLEHOLD then
				--Add header and timesamp
				data_buffer(0) <= x"AA";
				data_buffer(1) <= x"BB";
				data_buffer(2) <= x"CC";
				data_buffer(3) <= sample_cnt_reg(31 downto 24);
				data_buffer(4) <= sample_cnt_reg(23 downto 16);
				data_buffer(5) <= sample_cnt_reg(15 downto 8);
				data_buffer(6) <= sample_cnt_reg(7 downto 0);

				-- Add the data
				mapFIFOInputToBuffer : for i in 7 to PACKET_SIZE - 2 loop
					data_buffer(i) <= FIFOData(i - 7);
				end loop;
				-- check sum (empty)
				data_buffer(PACKET_SIZE - 1) <= x"00";

			elsif current_state = WRITEBYTES and byte_cnt <= PACKET_SIZE - 1 then
				-- check sum generator
				data_buffer(PACKET_SIZE - 1) <= data_buffer(PACKET_SIZE - 1) xor data_buffer(byte_cnt);
			end if;

		end if;
	end process;

--timesamp generator
	sample_cnt_reg <= std_logic_vector(to_unsigned(sample_cnt, 32));
	SampleCounter : process(clk)
	begin
		if rising_edge(clk) then
			if nRST = '0' then
				sample_cnt <= 0;
			else
				if sample_cnt = integer'high then
					sample_cnt <= 0;
				else
					sample_cnt <= sample_cnt + 1;
				end if;
				
			end if;
		end if;
	end process;

-- index to packet bytes (for check sum generator)
	ByteCounter : process(clk)
	begin
		if rising_edge(clk) then
			if nRST = '0' then
				byte_cnt <= 0;
			else
				if start_byte_counter = '1' then
					byte_cnt <= 0;
				elsif (current_state = WRITEBYTES) then		--and (byte_cnt < PACKET_SIZE- 1) then				 --		
					byte_cnt <= byte_cnt + 1;
				end if;
			end if;
		end if;
	end process;

	USBFIFO : FIFOUSB
		port map(
			-- Inputs
			DATA   => byte_buffer,
			RCLOCK => USBCLK,
			RE     => not (USBnWR or USBnTXE),
			--not USBnWR and not USBnTXE,
			RESET  => not nRST,
			WCLOCK => clk,
			WE     => write_byte,
			-- Outputs
			EMPTY => FIFO_empty,
			FULL  => FIFOFull,
			AEMPTY  => open,
			AFULL   => open,
			Q      => USBDataBusWire
		);

	-- USBnWR <= USBnTXE or USBnTXEPrev or FIFO_empty;
	-- USBnTXEBuff <= USBnTXE;
	-- USBnTXEPrev <= USBnTXEBuff;
	writeusb : process(USBCLK, nRST)
	begin
		if rising_edge(USBCLK) then
			if nRST = '0' then
				USBnWR <= '1';
				USBnTXEBuff <= '1';
				USBnTXEPrev <= '1';
			else
				USBnWR <= USBnTXE or USBnTXEPrev or FIFO_empty;
				USBnTXEBuff <= USBnTXE;
				USBnTXEPrev <= USBnTXEBuff;
			end if;
		end if;
	end process;

	USBDataBus <= USBDataBusWire;       
	USBnOE <= '1';
	USBnRD <= '1';

end behav;
