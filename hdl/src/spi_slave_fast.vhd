LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.std_logic_unsigned.ALL;
use ieee.numeric_std.all;
use IEEE.NUMERIC_STD.ALL;

library UQDSlib;
use UQDSlib.util.all;
--------------------------------------------------------------------------------
-- Company: CERN
--
-- File: spi_slave.vhd
-- File history:
--      0.1: 21.07.2021: first attempt to SPI 20MHz
--      <Revision number>: <Date>: <Comments>
--
-- Description: 
--
-- implements an SPI slave
--
-- ToDo:
--		test !
--
-- Targeted device: igloo2
-- Author: Jens Steckert, Andrzej Skoczen
--
--------------------------------------------------------------------------------

entity spi_slave_fast is
generic(trans_length : natural := 8);
port (nrst : in std_logic;
    --spi hw lines
    spi_csn			:in std_logic;
	spi_sclk		:in std_logic;
	spi_sdin		:in std_logic;
	spi_sdout		:out std_logic;
	--internal signals
	rdata			:out std_logic_vector(trans_length-1 downto 0);
	wdata			:in std_logic_vector(trans_length-1 downto 0);
	rxreq			:out std_logic;
	rxack			:in std_logic;
	txreq			:in std_logic;
	txack			:out std_logic
);
end entity;

architecture spi_slave_arch of spi_slave_fast is
constant bcnt_length : natural := log2c(trans_length+4);
--constant first_bit : unsigned(bcnt_length-1 downto 0) := (bcnt_length-1 downto 1 => '0') & '1';

type spi_bus_state is (idle, send, done, synchro_rec, synchro_trn);
signal state, next_state : spi_bus_state;
signal clk 		: std_logic;
signal rst		: std_logic := '0';
signal tshreg, rshreg : std_logic_vector(trans_length-1 downto 0);
signal bitcnt : unsigned(bcnt_length-1 downto 0);
signal synctxreq : std_logic_vector(1 downto 0);

begin

clk <= spi_sclk;
rst <= not nrst or rxack;

rxreq <= '1' when state = done else '0';
rdata <= rshreg;
		
state_reg_fsm: process(clk)
begin
	if rst = '1' then
		state <= idle;
	else
		if rising_edge(clk) then
			state <= next_state;
		end if;
	end if;
end process;

nest_state_logic: process(all)
begin
		case state is
			when idle => if spi_csn = '0' then		
							next_state <=  synchro_trn;
						else
							next_state <=  idle;
						end if;
			when synchro_trn => if bitcnt = to_unsigned(1, bcnt_length) then
							next_state <=  send;
						else
							next_state <=  synchro_trn;
						end if;
			when send => if bitcnt = to_unsigned(trans_length, bcnt_length) then
							next_state <= done;
						else
							next_state <=  send;
						end if;
			when done => next_state <= synchro_rec;
			when synchro_rec => if bitcnt = to_unsigned(trans_length+2, bcnt_length) then
							next_state <= idle;
						else
							next_state <=  synchro_rec;
						end if;
		end case;
end process;

bit_counter: process(clk)
begin
	if rst = '1' then
		bitcnt <= (others => '0');
	else
		if rising_edge(clk) then
			if state = idle then
				bitcnt <= (others => '0');
			else
				bitcnt <= bitcnt + 1;
			end if;
		end if;
	end if;
end process;

shift_reg_rec: process(clk)
begin
	if rst = '1' then
		rshreg <= (others => '0');
	else 
		if rising_edge(clk) then
			if state = send or state = done then
				rshreg <= rshreg(trans_length-2 downto 0) & spi_sdin;
			end if;
		end if;
	end if;
end process;

synchro_txreq: process(clk)
begin
	if rst = '1' then
		synctxreq <= (others => '0');
	elsif rising_edge(clk) then
		if state = idle or state = synchro_trn then
			synctxreq <= synctxreq(0) & txreq;
		end if;
	end if;
end process;

shift_reg_tr: process(clk)
begin
	if rst = '1' then
		tshreg <= (others => '0');
	else 
		if falling_edge(clk) then									--- falling_edge !!!!!!!!!!!!!!
			if synctxreq(1) = '1' then 
				tshreg <= wdata;
			elsif state = send or state = done then
				tshreg <= tshreg(trans_length-2 downto 0) & '0';
			end if;
		end if;
	end if;
end process;
spi_sdout <= tshreg(trans_length-1);
--txreq;
--synctxreq(1) or 
end;
