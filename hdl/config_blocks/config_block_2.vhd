library ieee;
use ieee.std_logic_1164.all;
USE IEEE.numeric_std.ALL;
USE IEEE.fixed_pkg.all;
use IEEE.fixed_float_types.all;

LIBRARY UQDSLib;
use UQDSLib.UQDSLib.ALL;
use UQDSLib.reg_map_pkg.all;
use UQDSLib.config_block_lib.all;



---------------------------------------------------------------------------------------------------------
-- Company: CERN
--
-- File: config_block.vhd
-- File history:
--      0.1: 14.03.2019: first attempt
--      1.0: 18.06.2019: use for 11T protection in SM18
--  
--	<Revision number>: <Date>: <Comments>
--
-- Description: 
--
--!@brief Custom configuration block number 2 to work with UQDS for 11T protection in SM18 (July 2019)
--	
-- INPUT CHANNELS : 5
-- Ch0: U_AP2U
-- Ch1: U_AP2L
-- Ch2: U_AP1U
-- Ch3: U_AP1L
-- Ch4: CircuitCurrent
--
--
-- OUTPUT CHANNELS : 11
-- Ch0: U_AP2U
-- Ch1: U_AP2L
-- Ch2: U_AP1U
-- Ch3: U_AP1L
-- Ch4: CircuitCurrent
-- Ch5: Diff_AP2
-- Ch6: Diff_AP1
-- Ch7: Diff_AP12
-- Ch8: ActiveThreshold comp 0
-- Ch9: ActiveDiscriminator comp 0
-- Ch10: ActiveThreshold comp 1
-- Ch11: ActiveDiscriminator comp 1
-- Ch12: Logics

--
-- LOGICS
-- L1: U1-U2
-- L2: U_lead1
-- L3: U_lead2

-- Targeted device: M2GL150
-- Author: Daniel Blasco, Jens Steckert
--
----------------------------------------------------------------------------------------------------------
entity config_block is
	port(
		CLK                      : in  std_logic;
		rst                      : in  std_logic;
		--Data from ADC, scaled & dready pulses
		ChannelsScaledData       : in  channelsArray(CHANNELS_IN_MAX - 1 downto 0);
		ChannelsScaledDataRdy    : in  std_logic_vector(CHANNELS_IN_MAX - 1 downto 0);
		--Flags to show if ADC gives constant values
		ADC_Health			 	: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		ADC_Health_Mask		 	: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		--Flags gnerated by scaler to indicate that signal left linear range of UQDS channel
		ADC_overRange			: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		ADC_overRange_Mask		: in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		
		--Registers configuring this block (127 registers in total)		
		ConfigBlockReg           : in  customRegArray;
		
		--Signals from top level command interpreter to initiate auto offset correction
		AutozeroStart            : in  std_logic_vector(31 downto 0);
		
		--Configuration of Readout (mux settings)
		ReadoutConfig            : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		
		AdcStartConv             : in  std_logic;
		
		--Mask to select Post mortem trigger from local trigger sources
		PMBufferMask             : in  std_logic_vector(REGISTER_SIZE - 1 downto 0);
		
		--Sync signals from top level (actual action is defined on top)
		SyncSignals              : in  std_logic_vector(5 downto 0);
		
		--Data to PM buffer & associated dready signals
		BufferMUXData            : out outputChannelsArray;
		BufferMUXDataRdy         : out std_logic;
		
		--Mask to mark non-decimable PM buffer channels (non-analogue values)
		BufferNoDecimateChannels : out std_logic_vector(CHANNELS_OUT_MAX - 1 downto 0);
		
		--Data to normal register space
		RegMUXData               : out regChannelsArray;
		
		--Data to Fast Readout module & associated dready
		FReadoutMUXData          : out outputChannelsArray;
		FReadoutMUXDataRdy       : out std_logic;
		
		--Trigger lines generated locally, mapped on TOP to pysical lines
		TriggerLines             : out std_logic_vector(TRIGGER_LINES_MAX - 1 downto 0);
		
		--PM trigger signal generated locally
		TriggerPM                : out std_logic;
		
		--ID of this config block
		ConfigBlockNum           : out std_logic_vector(REGISTER_SIZE - 1 downto 0)
	);
end config_block;

architecture config_block_arch of config_block is

--type and function for initialkization of depth and middle parameters of filters
type integerArray is array (CHANNELS_GENERICS_NUM - 1 downto 0) of integer range 0 to 15;
function init_integerArray (init_val : in integer; size: in integer) return integerArray is
	variable initarr : integerArray;
	begin
		for i in size-1 downto 0 loop
			initarr(i) := init_val;
		end loop;
		return initarr;
end function;
	--========================= Signal declaration ============================

	-- Channel configuration signals
	type configArray is array (CHANNELS_GENERICS_NUM - 1 downto 0) of std_logic_vector(7 downto 0);
	signal channel_filter_cfg        : configArray;
	signal channel_daq_cfg           : configArray;
	--signal channel_mean_depth_int    : regDataArray(CHANNELS_GENERICS_NUM - 1 downto 0);
	signal channel_mean_depth_int    : integerArray := init_integerArray(2, CHANNELS_GENERICS_NUM);
	signal channel_median_depth_int  : integerArray := init_integerArray(2, CHANNELS_GENERICS_NUM);
	signal channel_median_middle_int : integerArray := init_integerArray(1, CHANNELS_GENERICS_NUM);
	signal channel_mean_inv_depth    : regDataArray(CHANNELS_GENERICS_NUM - 1 downto 0);
	type gainArray is array (CHANNELS_GENERICS_NUM - 1 downto 0) of std_logic_vector(15 downto 0);
	signal channel_corrector_gain    : gainArray;
	signal channel_corrector_offset  : channelsArray(CHANNELS_GENERICS_NUM - 1 downto 0);

	-- End of pipeline data
	signal filtered_data           : outputChannelsArray := (others=>(others=>'0'));
	signal filtered_data_secondary : outputChannelsArray := (others=>(others=>'0'));
	signal filtered_data_rdy       : std_logic_vector(CHANNELS_GENERICS_NUM - 1 downto 0);
	signal buf_data_rdy       	   : std_logic_vector(CHANNELS_GENERICS_NUM - 1 downto 0);
	signal scaled_data             : outputChannelsArray := (others=>(others=>'0'));

	-- Logic signals
	signal logic_status : std_logic_vector(31 downto 0);
	signal logic_trigger : std_logic;
	signal ADC_overrange_sum :std_logic;
	
	--Dynamic Threshold and discriminator (array with two elements for double comparator
	signal DynThres	: channelsArray(1 downto 0);
	signal DynDiscr	: channelsArray(1 downto 0);
	--signal DynDiscr	: channelsArray16B(1 downto 0);
	
	signal current_malfunction :std_logic;
	
	signal AP1L			: sf_voltage;
	signal AP1U			: sf_voltage;
	signal AP2L			: sf_voltage;
	signal AP2U			: sf_voltage;
	signal sum_AP1_UL 	: std_logic_vector(REGISTER_SIZE -1 downto 0);
	signal sum_AP2_UL 	: std_logic_vector(REGISTER_SIZE -1 downto 0);
	
	
	--=========================================================================
	constant generate_median : boolean := TRUE;


begin

	-- Config block identifier (each different config block for different FW should have a different identifier)
	ConfigBlockNum <= std_logic_vector(to_unsigned(2, 32));

	--========================== Register mapping =============================

	--	Assign Generic Config block registers to its signals
	RegisterMapConfigBlock(
			ConfigBlockReg,  	--IN
			ChannelGenericA, 	--OUT
			ChannelGenericB, 	--OUT
			ChannelGenericC, 	--OUT
			ChannelGenericD, 	--OUT
			CurrLevelTable,  	--OUT
			DynThreshTableComp0,  	--OUT
			DynDiscrTableComp0,    	--OUT
			DynThreshTableComp1,  	--OUT
			DynDiscrTableComp1
		);

	--=========================================================================

	--================= Channel generic for input channels ====================

	data_processing : for i in 0 to CHANNELS_IN - 1 generate
		channel_data_processing : channel_generic
			generic map(
				generate_median => generate_median,
				d_width        => 32,
				b_width        => 32,   -- Not used
				mavg_ram_depth => 1024, -- Max mavg depth
				cor_c_width    => 16
			)
				-- port map
			port map(
				-- Inputs
				clk             => CLK,
				rst             => rst,
				sampleclock     => ChannelsScaledDataRdy(i),
				Data_in         => ChannelsScaledData(i),
				autozero_start  => AutozeroStart(i),
				mux_data        => channel_filter_cfg(i), -- Config [autozero, manual, mean, median, decimation,decimation,decimation,inverter]
				mux_daq         => channel_daq_cfg(i)(1 downto 0), -- DAQ
				mavg_depth_bits => channel_mean_depth_int(i),
				real_div        => channel_mean_inv_depth(i),
				real_invdiv     => channel_mean_inv_depth(i),
				median_depth    => channel_median_depth_int(i),
				median_middle   => channel_median_middle_int(i),
				cor_gain        => channel_corrector_gain(i), -- Manual correction
				cor_offset      => channel_corrector_offset(i), -- Manual correction
				-- Outputs
				cor_offset_auto => open, -- Not used
				Dmux_data_out   => filtered_data(i)(31 downto 0), 				-- Output to regspace
				Bmux_data_out   => filtered_data_secondary(i)(31 downto 0), 	-- Output to buffers
				d_ready_out     => filtered_data_rdy(i), -- Output rdy
				buf_ready_out	=> buf_data_rdy(i),
				debug           => open
			);

	end generate;

	type_matching_scaled_data : for i in 0 to CHANNELS_IN - 1 generate
		scaled_data(i) <= ChannelsScaledData(i);
	end generate;

	data_processing_signals : for i in 0 to CHANNELS_GENERICS_NUM - 1 generate

		channel_filter_cfg(i)          <= ChannelGenericA(i)(7 downto 0); -- MUX control
		channel_daq_cfg(i)(7 downto 0) <= "000000" & ChannelGenericA(i)(9 downto 8); -- MUX control Buffer
		channel_median_depth_int(i)    <= to_integer(unsigned(ChannelGenericB(i)(7 downto 0))) when generate_median = TRUE else 2; -- Median filter depth
		channel_median_middle_int(i)   <= to_integer(unsigned('0' & ChannelGenericB(i)(7 downto 1))) when generate_median = TRUE else 1; -- Median filter middle (depth divided by 2)
		channel_mean_depth_int(i)      <= to_integer(unsigned(ChannelGenericA(i)(25 downto 16))); -- Moving average filter length
		--channel_mean_depth_int(i)      <= x"00000" & "00" & ChannelGenericA(i)(25 downto 16); -- Moving average filter length
		channel_corrector_gain(i)      <= ChannelGenericB(i)(31 downto 16); -- Corrector gain
		channel_corrector_offset(i)    <= ChannelGenericC(i); -- Corrector offset
		channel_mean_inv_depth(i)      <= ChannelGenericD(i); -- Inverted multiplication for mavg filter

	end generate;

	--=========================================================================

	--======================== Output multiplexers ============================
	
	-- We have thres main data paths:
	-- scaled_data: 			raw data from channels after scaler
	-- filtered_data:			from channel generic main data output (input to logic) & derived values
	-- filtered_data_secondary: from channel generic buffer data output & derived values
		
	--Multiplex data going to registers	
	generate_mux_data1 : for i in 0 to CHANNELS_OUT - 1 generate
		RegMUXData(i)      <= filtered_data_secondary(i) when (ReadoutConfig(1 downto 0) = "01")
			else scaled_data(i) when (ReadoutConfig(1 downto 0) = "10")
			else filtered_data(i);
	end generate;
	
	--set unnecessary registers to '0'
	--generate_mux_data2 :for i in CHANNELS_OUT to CHANNELS_OUT_MAX-1 generate
	--	RegMUXData(i) <= (others=>'0');
	--end generate;
		
	BufferMUXDataRdy   <= buf_data_rdy(0) when (ReadoutConfig(1 downto 0) = "01")
		else ChannelsScaledDataRdy(0) when (ReadoutConfig(1 downto 0) = "10")
		else filtered_data_rdy(0);
			
	--Multiplex data going to fast readout (on top level)		
			
	type_matching_freadout : for i in 0 to CHANNELS_OUT - 1 generate
		FReadoutMUXData(i) <= filtered_data_secondary(i) when (ReadoutConfig(9 downto 8) = "01")
			else scaled_data(i) when (ReadoutConfig(9 downto 8) = "10")
			else filtered_data(i);
	end generate;
	
	FReadoutMUXDataRdy <= buf_data_rdy(0) when (ReadoutConfig(9 downto 8) = "01")
		else ChannelsScaledDataRdy(0) when (ReadoutConfig(9 downto 8) = "10")
		else filtered_data_rdy(0);
 
	--Multiplex data going to buffers (on top level)
	BufferMUXData         <= filtered_data_secondary when (ReadoutConfig(17 downto 16) = "01")
		else scaled_data when (ReadoutConfig(17 downto 16) = "10")
		else filtered_data;

	--=========================================================================
	--Dynamic threshold
	--current is on channel 4
	--IF ADC frozen bit is high (adc interface detects no data from ADC)
	--dynthres selects lowest (last) threshold in dynamic threshold block
	
	

	dynthresh0 : dynamic_threshold
		generic map(d_width      => 32,
		            table_length => 4)
		port map(
			clk                  => CLK,
			rst                  => rst,
			threshold_tab        => DynThreshTableComp0,
			discriminator_tab    => DynDiscrTableComp0,
			current_level_tab    => CurrLevelTable,
			current              => filtered_data(4),
			force_last_threshold => ADC_Health(4),
			threshold            => DynThres(0),
			discriminator        => DynDiscr(0)
		);
	
	dynthresh1 : dynamic_threshold
		generic map(d_width      => 32,
		            table_length => 4)
		port map(
			clk                  => CLK,
			rst                  => rst,
			threshold_tab        => DynThreshTableComp1,
			discriminator_tab    => DynDiscrTableComp1,
			current_level_tab    => CurrLevelTable,
			current              => filtered_data(4),
			force_last_threshold => ADC_Health(4),
			threshold            => DynThres(1),
			discriminator        => DynDiscr(1)
		);
			
	--Propagate actual threshold and actual discriminator to data channels
	filtered_data(9) <= DynThres(0);
	filtered_data(10) <= DynDiscr(0);
	filtered_data(11) <= DynThres(1);
	filtered_data(12) <= DynDiscr(1);
	filtered_data_secondary(9) <= DynThres(0);
	filtered_data_secondary(10) <= DynDiscr(0);
	filtered_data_secondary(11) <= DynThres(1);
	filtered_data_secondary(12) <= DynDiscr(1);
	
	
	--============================ Logic blocks ===============================

	-- Logic Block 1 & 2 & 3
	--convert inputs to sfixed
	AP2U <= to_sfixed(filtered_data(0), AP2U);
	AP2L <= to_sfixed(filtered_data(1), AP2L);
	AP1U <= to_sfixed(filtered_data(2), AP1U);
	AP1L <= to_sfixed(filtered_data(3), AP1L);
	
	--summing of coil voltages to get aperture voltage for symmetric quench detection
	--adding implemented with fixed package and resize to handle overflow
	sum_AP1_UL <= to_slv(resize(arg => (AP1U + AP1L), size_res => AP1U, overflow_style => fixed_saturate, round_style => fixed_truncate));
	sum_AP2_UL <= to_slv(resize(arg => (AP2U + AP2L), size_res => AP2U, overflow_style => fixed_saturate, round_style => fixed_truncate));
	
	
	
	--ch0: A2U
	--ch1: A2L

	--Bridge 1, compares Aperture 2 upper and Lower
	bridge_AP2: qlogic_bridge_flex
		generic map(	d_width => data_path_width,
						subtract => TRUE,
						num_comps => 2
		) 
		port map( 	clk 		=> CLK,
					rst 		=> rst,
					qthreshold 	=> DynThres,
					time_disc 	=> DynDiscr,
					inA 		=> filtered_data(0),
					inB 		=> filtered_data(1),
					outAB		=> filtered_data(5),
					log_out 	=> logic_status(3 downto 0)
		);
	
	--Bridge 1, compares Aperture 1 upper and Lower
	bridge_AP1: qlogic_bridge_flex
		generic map(	d_width => data_path_width,
						subtract => TRUE,
						num_comps => 2
		) 
		port map( 	clk 		=> CLK,
					rst 		=> rst,
					qthreshold 	=> DynThres,
					time_disc 	=> DynDiscr,
					inA 		=> filtered_data(2),
					inB 		=> filtered_data(3),
					outAB		=> filtered_data(6),
					log_out 	=> logic_status(7 downto 4)
		);
		
		
		
	--Bridge 3, compares Aperture 2 with Aperture 1
	bridge_symmetric: qlogic_bridge_flex
		generic map(	d_width => data_path_width,
						subtract => TRUE,
						num_comps => 2
		) 
		port map( 	clk 		=> CLK,
					rst 		=> rst,
					qthreshold 	=> DynThres,
					time_disc 	=> DynDiscr,
					inA 		=> sum_AP1_UL,
					inB 		=> sum_AP2_UL,
					outAB		=> filtered_data(7),
					log_out 	=> logic_status(11 downto 8)
		);	
	

	--=========================================================================

	--============================ Logic match ================================
	--logic_status(11 downto 0): 3x 4 bits from QD logic
	--ADD adc status bits
	logic_status(16 downto 12)              <= ADC_Health(CHANNELS_IN - 1 downto 0);
	--add sync signals
	logic_status(22 downto 17)              <= SyncSignals;
	logic_status(23)						<= '0';
	logic_status(24)						<= current_malfunction;
	logic_status(25)						<= ADC_overrange_sum;
	logic_status(31 downto 26)              <= (others=>'0');	
	--Propagate logic vector for all three data sources
	scaled_data(8)(31 downto 0)             <= logic_status(31 downto 0);	
	filtered_data(8)(31 downto 0)           <= logic_status(31 downto 0);
	filtered_data_secondary(8)(31 downto 0) <= logic_status(31 downto 0);
	
	--Marks non-decimatable buffer channels (logic flags etc)  (in our case ch8 are logic signals)
	BufferNoDecimateChannels(31 downto 13) <= (others => '0');
	BufferNoDecimateChannels(12 downto 8) <= (others => '1');
	BufferNoDecimateChannels(7 downto 0)  <= (others => '0');
	
	--Evaluate the channel over range flags: (active if channel exceeds 22.5V, checked in scaler)
	ADC_overrange_sum <= 	(ADC_OverRange(4) AND ADC_overRange_Mask(4)) OR
							(ADC_OverRange(3) AND ADC_overRange_Mask(3)) OR
							(ADC_OverRange(2) AND ADC_overRange_Mask(2)) OR
							(ADC_OverRange(1) AND ADC_overRange_Mask(1)) OR
							(ADC_OverRange(0) AND ADC_overRange_Mask(0));
	
	--Control trigger HW lines
	--Triggers (17 downto 2): Active voltage output lines to trigger HDS etc
	--Triggers (1 downto 0):  Current loop ('0' = opens or '1' = closes)
	TriggerLines(17 downto 8) <= (others => '0');
	
	--no current loop interlock for SM18
	TriggerLines(1 downto 0) <= (others => '0');
	
	--Combine QD logic signals to trigger signals, we're high-active
	--								sym				asym ap1			asym ap2
	logic_trigger			<= 	logic_status(8)  OR logic_status(4) OR logic_status(0) OR --level 0 comparators
								logic_status(10) OR logic_status(6) OR logic_status(2) OR --level 1 comparators	
								ADC_overrange_sum ;	   									  --ADC over range	
	
	--Propagate logic trigger to hardware lines
	--In SM18 trigger is active low --> negate
	TriggerLines(7)<= not logic_trigger;
	TriggerLines(6)<= not logic_trigger;
	TriggerLines(5)<= not logic_trigger;
	TriggerLines(4)<= not logic_trigger;
	TriggerLines(3)<= not logic_trigger;
	TriggerLines(2)<= not logic_trigger;
	
	--Condition to trigger post mortem:
	TriggerPM                 <= 	(ADC_overrange_sum and PMBufferMask(12)) or
									(logic_status(11) and PMBufferMask(11)) or --quench sym level 1
									(logic_status(10) and PMBufferMask(10)) or --latch sym level 1
									(logic_status(7) and PMBufferMask(7)) or --quench asy2 level 1
									(logic_status(6) and PMBufferMask(6)) or --latch asy2 level 1
									(logic_status(3) and PMBufferMask(3)) or --quench asy1 level 1
									(logic_status(2) and PMBufferMask(2)) or --latch asy1  level 1
									(logic_status(9) and PMBufferMask(9)) or --quench sym level 0
									(logic_status(8) and PMBufferMask(8)) or --latch sym level 0
									(logic_status(5) and PMBufferMask(5)) or --quench asy2 level 0
									(logic_status(4) and PMBufferMask(4)) or --latch asy2 level 0
									(logic_status(1) and PMBufferMask(1)) or --quench asy1 level 0
									(logic_status(0) and PMBufferMask(0));   --latch asy1  level 0
	--=========================================================================

end;